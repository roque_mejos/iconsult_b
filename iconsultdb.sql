-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2017 at 08:54 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iconsultdb`
--
CREATE DATABASE IF NOT EXISTS `iconsultdb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `iconsultdb`;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `title`, `message`, `created`, `user_id`) VALUES
(1, 'School Meeting', 'School meeting tomorrow', '2017-02-21 08:58:21', 6);

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `student_id` int(11) NOT NULL,
  `school_date_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`id`, `status`, `student_id`, `school_date_id`) VALUES
(1, 1, 13, 1),
(2, 0, 12, 1),
(3, 0, 12, 2),
(4, 1, 13, 2),
(5, 0, 12, 3),
(6, 0, 13, 3);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `receiver` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `unread` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `message`, `receiver`, `sender`, `created_at`, `unread`) VALUES
(1, 'Hello, this is our first chat.', 14, 10, '2017-03-13 07:47:28', 1),
(2, 'test2', 10, 14, '2017-03-13 14:58:43', 1),
(3, 'hellow', 14, 10, '2017-03-14 08:09:58', 1),
(4, 'test2', 14, 10, '2017-03-14 08:14:17', 1),
(5, 'test2', 14, 10, '2017-03-14 08:15:08', 1),
(6, 'test', 14, 10, '2017-03-14 08:16:01', 1),
(7, 'hello again', 14, 10, '2017-03-14 08:26:27', 1),
(8, 'thanks', 14, 10, '2017-03-14 08:32:17', 1),
(9, 'sdasdasd wadasdasdad dasdasdasdasdsada dasdasdasdsadasdas dadasdsadadad adasdasd', 14, 10, '2017-03-14 08:36:57', 1),
(10, 's', 14, 10, '2017-03-14 08:38:14', 1),
(11, 'asa', 14, 10, '2017-03-14 08:38:17', 1),
(12, ':D', 14, 10, '2017-03-14 08:38:21', 1),
(13, 'test', 14, 10, '2017-03-14 08:47:30', 1),
(14, 'hi', 14, 10, '2017-03-14 09:28:27', 1),
(15, 'hi', 14, 10, '2017-03-14 09:28:33', 1),
(16, 'test', 14, 10, '2017-03-14 09:29:37', 1),
(17, 'sure', 14, 10, '2017-03-14 09:29:41', 1),
(18, 'kiss', 14, 10, '2017-03-14 09:30:01', 1),
(19, 'hello there', 14, 10, '2017-03-14 09:31:10', 1),
(20, 'whos in there?', 14, 10, '2017-03-14 09:31:19', 1),
(21, 'hi', 14, 10, '2017-03-14 09:31:24', 1),
(22, 'sad', 14, 10, '2017-03-14 09:31:26', 1),
(23, 's', 14, 10, '2017-03-14 09:31:27', 1),
(24, 'hi', 14, 10, '2017-03-14 09:32:40', 1),
(25, 'aa', 14, 10, '2017-03-14 09:36:38', 1),
(26, 's', 14, 10, '2017-03-14 09:36:39', 1),
(27, 's', 14, 10, '2017-03-14 09:36:40', 1),
(28, 's', 14, 10, '2017-03-14 09:36:40', 1),
(29, 's', 14, 10, '2017-03-14 09:36:41', 1),
(30, 's', 14, 10, '2017-03-14 09:36:42', 1),
(31, 's', 14, 10, '2017-03-14 09:36:43', 1),
(32, 's', 14, 10, '2017-03-14 09:37:10', 1),
(33, 's', 14, 10, '2017-03-14 09:37:11', 1),
(34, 's', 14, 10, '2017-03-14 09:37:12', 1),
(35, 's', 14, 10, '2017-03-14 09:37:12', 1),
(36, 's', 14, 10, '2017-03-14 09:37:13', 1),
(37, 's', 14, 10, '2017-03-14 09:37:13', 1),
(38, 'test', 6, 10, '2017-03-14 09:37:18', 1),
(39, 'test', 14, 10, '2017-03-14 09:49:12', 1),
(40, 's', 14, 10, '2017-03-14 10:00:39', 1),
(41, 'test', 14, 10, '2017-03-14 10:44:00', 1),
(42, 's', 14, 10, '2017-03-14 10:44:02', 1),
(43, 's', 14, 10, '2017-03-14 10:44:03', 1),
(44, 's', 14, 10, '2017-03-14 10:44:03', 1),
(45, 's', 14, 10, '2017-03-14 10:44:03', 1),
(46, 's', 14, 10, '2017-03-14 10:44:04', 1),
(47, 's', 14, 10, '2017-03-14 10:44:04', 1),
(48, 's', 14, 10, '2017-03-14 10:44:04', 1),
(49, 's', 14, 10, '2017-03-14 10:44:05', 1),
(50, 's', 14, 10, '2017-03-14 10:44:05', 1),
(51, 's', 14, 10, '2017-03-14 10:44:05', 1),
(52, 's', 14, 10, '2017-03-14 10:44:07', 1),
(53, 's', 14, 10, '2017-03-14 10:44:09', 1),
(54, 's', 14, 10, '2017-03-14 10:44:09', 1),
(55, 's', 14, 10, '2017-03-14 10:44:10', 1),
(56, 's', 14, 10, '2017-03-14 10:44:11', 1),
(57, 'sure', 10, 6, '2017-03-14 11:32:51', 1),
(58, 'test', 6, 10, '2017-03-14 12:57:14', 1),
(59, 'hoi', 14, 10, '2017-03-14 12:57:23', 1),
(60, 'hehe', 10, 14, '2017-03-14 12:57:50', 1),
(61, 's', 14, 10, '2017-03-14 13:22:26', 1),
(62, 's', 14, 10, '2017-03-14 13:22:36', 1),
(63, 'te', 10, 14, '2017-03-14 13:25:42', 1),
(64, 'te2', 10, 14, '2017-03-14 13:26:15', 1),
(65, 'te3', 10, 14, '2017-03-14 13:26:56', 1),
(66, 'test', 10, 14, '2017-03-14 13:35:28', 1),
(67, 'hehe', 14, 10, '2017-03-14 13:35:39', 1),
(68, 'test', 14, 10, '2017-03-14 13:37:10', 1),
(69, 'test', 10, 14, '2017-03-14 13:39:33', 1),
(70, 'lol', 10, 14, '2017-03-14 13:39:41', 1),
(71, 'test', 10, 14, '2017-03-14 13:39:57', 1),
(72, 'hello world', 14, 10, '2017-03-14 13:40:30', 1),
(73, 'sure', 10, 14, '2017-03-14 13:40:40', 1),
(74, 'hi po', 14, 10, '2017-03-14 13:40:58', 1),
(75, 'heheheheheheheh', 10, 14, '2017-03-14 13:41:10', 1),
(76, 'sadasd', 14, 10, '2017-03-14 13:48:14', 1),
(77, 'hoi', 6, 13, '2017-03-14 13:50:33', 1),
(78, 'ho', 13, 14, '2017-03-14 13:51:11', 1),
(79, 'huh?', 13, 14, '2017-03-14 13:51:44', 1),
(80, 'hi', 14, 13, '2017-03-14 14:11:56', 1),
(81, 'test', 6, 13, '2017-03-14 14:41:17', 1),
(82, 'test', 10, 14, '2017-03-14 14:42:36', 1),
(83, 'wee', 13, 14, '2017-03-14 14:42:57', 1),
(84, 's', 14, 13, '2017-03-14 14:43:04', 1),
(85, 's', 14, 13, '2017-03-14 14:43:05', 1),
(86, 's', 14, 13, '2017-03-14 14:43:06', 1),
(87, 's', 14, 13, '2017-03-14 14:43:09', 1),
(88, 's', 14, 13, '2017-03-14 14:43:09', 1),
(89, 's', 14, 13, '2017-03-14 14:43:10', 1),
(90, '1', 14, 13, '2017-03-14 14:43:11', 1),
(91, '2', 14, 13, '2017-03-14 14:43:12', 1),
(92, '3', 14, 13, '2017-03-14 14:43:12', 1),
(93, '4', 14, 13, '2017-03-14 14:43:13', 1),
(94, '5', 14, 13, '2017-03-14 14:43:14', 1),
(95, '6', 14, 13, '2017-03-14 14:43:15', 1),
(96, '7', 14, 13, '2017-03-14 14:43:16', 1),
(97, '8', 14, 13, '2017-03-14 14:43:18', 1),
(98, '9', 14, 13, '2017-03-14 14:43:19', 1),
(99, '10', 14, 13, '2017-03-14 14:43:20', 1),
(100, 'test', 10, 14, '2017-03-14 14:50:23', 1),
(101, 'test', 10, 14, '2017-03-14 14:50:28', 1),
(102, 'test1', 14, 13, '2017-03-14 14:50:33', 1),
(103, 'ha?', 13, 14, '2017-03-14 14:50:42', 1),
(104, 'test', 14, 13, '2017-03-14 15:28:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `date`, `created`, `status`, `title`) VALUES
(7, '2017-02-14', '2017-02-02 12:42:37', 1, 'Valentines days');

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id` int(11) NOT NULL,
  `name` enum('MALE','FEMALE') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `name`) VALUES
(1, 'MALE'),
(2, 'FEMALE');

-- --------------------------------------------------------

--
-- Table structure for table `grade_categories`
--

CREATE TABLE `grade_categories` (
  `id` int(11) NOT NULL,
  `written_work` float NOT NULL,
  `task_performance` float NOT NULL,
  `quarterly_assessment` float NOT NULL,
  `perfect_score` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_categories`
--

INSERT INTO `grade_categories` (`id`, `written_work`, `task_performance`, `quarterly_assessment`, `perfect_score`, `user_id`) VALUES
(1, 55, 75, 40, 40, 6);

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `grading_type` enum('1st','2nd','3rd','4th') NOT NULL,
  `student_record_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `grading_type`, `student_record_id`) VALUES
(195, '1st', 70),
(196, '2nd', 70),
(197, '3rd', 70),
(198, '4th', 70),
(199, '1st', 71),
(200, '2nd', 71),
(201, '3rd', 71),
(202, '4th', 71),
(203, '1st', 72),
(204, '2nd', 72),
(205, '3rd', 72),
(206, '4th', 72),
(207, '1st', 73),
(208, '2nd', 73),
(209, '3rd', 73),
(210, '4th', 73),
(211, '1st', 74),
(212, '2nd', 74),
(213, '3rd', 74),
(214, '4th', 74),
(215, '1st', 75),
(216, '2nd', 75),
(217, '3rd', 75),
(218, '4th', 75),
(219, '1st', 76),
(220, '2nd', 76),
(221, '3rd', 76),
(222, '4th', 76),
(223, '1st', 77),
(224, '2nd', 77),
(225, '3rd', 77),
(226, '4th', 77),
(227, '1st', 78),
(228, '2nd', 78),
(229, '3rd', 78),
(230, '4th', 78),
(231, '1st', 79),
(232, '2nd', 79),
(233, '3rd', 79),
(234, '4th', 79),
(235, '1st', 80),
(236, '2nd', 80),
(237, '3rd', 80),
(238, '4th', 80),
(239, '1st', 81),
(240, '2nd', 81),
(241, '3rd', 81),
(242, '4th', 81),
(243, '1st', 82),
(244, '2nd', 82),
(245, '3rd', 82),
(246, '4th', 82),
(247, '1st', 83),
(248, '2nd', 83),
(249, '3rd', 83),
(250, '4th', 83),
(251, '1st', 84),
(252, '2nd', 84),
(253, '3rd', 84),
(254, '4th', 84),
(255, '1st', 85),
(256, '2nd', 85),
(257, '3rd', 85),
(258, '4th', 85),
(259, '1st', 86),
(260, '2nd', 86),
(261, '3rd', 86),
(262, '4th', 86),
(263, '1st', 87),
(264, '2nd', 87),
(265, '3rd', 87),
(266, '4th', 87),
(267, '1st', 88),
(268, '2nd', 88),
(269, '3rd', 88),
(270, '4th', 88),
(271, '1st', 89),
(272, '2nd', 89),
(273, '3rd', 89),
(274, '4th', 89),
(275, '1st', 90),
(276, '2nd', 90),
(277, '3rd', 90),
(278, '4th', 90),
(279, '1st', 91),
(280, '2nd', 91),
(281, '3rd', 91),
(282, '4th', 91),
(283, '1st', 92),
(284, '2nd', 92),
(285, '3rd', 92),
(286, '4th', 92),
(287, '1st', 93),
(288, '2nd', 93),
(289, '3rd', 93),
(290, '4th', 93),
(291, '1st', 94),
(292, '2nd', 94),
(293, '3rd', 94),
(294, '4th', 94),
(295, '1st', 95),
(296, '2nd', 95),
(297, '3rd', 95),
(298, '4th', 95),
(299, '1st', 96),
(300, '2nd', 96),
(301, '3rd', 96),
(302, '4th', 96);

-- --------------------------------------------------------

--
-- Table structure for table `quarterly_assessments`
--

CREATE TABLE `quarterly_assessments` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `score` float NOT NULL,
  `perfect_score` float NOT NULL,
  `grade_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quarterly_assessments`
--

INSERT INTO `quarterly_assessments` (`id`, `title`, `score`, `perfect_score`, `grade_id`, `date`) VALUES
(5, '4', 4, 4, 195, '2017-02-01'),
(6, 'test', 4, 6, 196, '2017-02-01'),
(10, '22', 22, 22, 200, '2017-02-01'),
(12, '22', 22, 22, 201, '2017-02-01'),
(17, '2', 2, 2, 199, '2017-02-28'),
(18, '22', 22, 22, 202, '2017-02-02'),
(19, '24', 2, 2, 197, '2017-02-01'),
(20, '2', 2, 2, 198, '2017-02-24');

-- --------------------------------------------------------

--
-- Table structure for table `school_dates`
--

CREATE TABLE `school_dates` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_dates`
--

INSERT INTO `school_dates` (`id`, `date`) VALUES
(1, '2018-04-07'),
(2, '2018-03-08'),
(3, '2018-03-09');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `year_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `year_id`) VALUES
(1, 'Sampaguita', 1),
(2, 'Rose', 1),
(3, 'Orchid', 1),
(4, 'Gold', 2),
(5, 'Diamond', 2),
(6, 'Pearl', 2);

-- --------------------------------------------------------

--
-- Table structure for table `student_records`
--

CREATE TABLE `student_records` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_records`
--

INSERT INTO `student_records` (`id`, `student_id`, `subject_id`) VALUES
(70, 10, 5),
(71, 10, 2),
(72, 10, 1),
(73, 11, 2),
(74, 11, 5),
(75, 11, 1),
(76, 11, 4),
(77, 11, 16),
(78, 11, 3),
(79, 12, 2),
(80, 12, 5),
(81, 12, 1),
(82, 12, 4),
(83, 12, 16),
(84, 12, 3),
(85, 13, 5),
(86, 13, 1),
(87, 13, 4),
(88, 14, 2),
(89, 14, 5),
(90, 14, 1),
(91, 14, 4),
(92, 14, 16),
(93, 14, 3),
(94, 15, 2),
(95, 15, 5),
(96, 15, 4);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `mi` varchar(1) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `school_year` int(4) NOT NULL,
  `bdate` date NOT NULL,
  `year_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `ref_no` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `fname`, `lname`, `mi`, `mobile`, `address`, `school_year`, `bdate`, `year_id`, `parent`, `gender`, `teacher`, `section_id`, `ref_no`) VALUES
(10, 'Jeans', 'Dy', 'B', '09323453458', 'cebu city', 2016, '2001-02-01', 1, 10, 1, 6, 1, '1487570297'),
(11, 'roque', 'mejos', 'R', '09323453458', 'aaa', 2017, '2016-03-01', 1, 13, 1, 6, 2, '1488342581'),
(12, 'faye', 'ho', 'F', '09323453458', 'Cebu, Philippines', 2017, '2010-03-01', 2, 13, 1, 6, 6, '1488342760'),
(13, 'Bea', 'Yu', 'G', '09323453458', 'Cebu', 2017, '2015-12-27', 2, 13, 2, 6, 6, '1488343459'),
(14, 'Ben', 'Reyes', 'H', '09323453458', 'Lapu-Lapu City', 2017, '2010-02-02', 2, 10, 1, 14, 4, '1489022580'),
(15, 'ssss', 'sssss', 's', '09323453458', 'adsd', 2017, '2015-03-01', 1, 13, 1, 14, 3, '1489470559');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `status`) VALUES
(1, 'Math', 1),
(2, 'English', 1),
(3, 'Values Eduacation', 1),
(4, 'Science', 1),
(5, 'History', 1),
(16, 'Subject test', 1);

-- --------------------------------------------------------

--
-- Table structure for table `task_performances`
--

CREATE TABLE `task_performances` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `score` float NOT NULL,
  `perfect_score` float NOT NULL,
  `grade_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task_performances`
--

INSERT INTO `task_performances` (`id`, `title`, `score`, `perfect_score`, `grade_id`, `date`) VALUES
(4, '4', 4, 4, 195, '2017-02-01'),
(5, 'adsd', 8, 8, 196, '2017-02-01'),
(12, '22', 33, 33, 199, '2017-02-28'),
(13, '22', 33, 33, 200, '2017-02-28'),
(14, '22', 22, 22, 201, '2017-02-01'),
(15, '22', 22, 22, 202, '2017-02-01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `usertype` int(1) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `mi` varchar(1) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `gender` int(1) NOT NULL,
  `bdate` date NOT NULL,
  `address` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `usertype`, `fname`, `lname`, `mi`, `mobile`, `image`, `gender`, `bdate`, `address`, `created_at`) VALUES
(1, 'roque.mejos@gmail.com', '96e79218965eb72c92a549dd5a330112', 1, 'Roque', 'Mejos', 'R', '09122323432', 'b1316c06c5409994db5874930f4765f2.jpg', 1, '1970-01-01', 'Philippiness', '2017-01-30 14:26:30'),
(6, 'james@gmail.com', '96e79218965eb72c92a549dd5a330112', 2, 'James', 'Sy', 'T', '09323453454', '9a9b1ee854b0fbcf1beb9114b2d9b3f4.jpg', 1, '1991-01-03', 'Zamboanga Del Sur', '2017-01-31 09:30:33'),
(10, 'jean@gmail.com', '96e79218965eb72c92a549dd5a330112', 3, 'Jean', 'Tan', 'E', '09323453458', '', 2, '2009-03-06', 'Cebu City', '2017-02-04 14:50:20'),
(13, 'roque.mejos@gmail.coms', '96e79218965eb72c92a549dd5a330112', 3, 'Roque', 'Mejos', 'R', '09323453458', '', 1, '2010-02-15', 'Cebu City', '2017-02-22 13:29:09'),
(14, 'kylie@gmail.com', '96e79218965eb72c92a549dd5a330112', 2, 'Kylie', 'Yai', 'G', '09323453458', '', 1, '2009-02-03', 'Cebu City', '2017-03-09 09:20:48'),
(15, 'jash@gmail.com', '96e79218965eb72c92a549dd5a330112', 2, 'Jash', 'Tan', 'H', '09323453458', '', 1, '2010-02-02', 'Mandaue City', '2017-03-09 09:21:34');

-- --------------------------------------------------------

--
-- Table structure for table `usertypes`
--

CREATE TABLE `usertypes` (
  `id` int(11) NOT NULL,
  `name` enum('ADMIN','TEACHER','PARENTS') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usertypes`
--

INSERT INTO `usertypes` (`id`, `name`) VALUES
(1, 'ADMIN'),
(2, 'TEACHER'),
(3, 'PARENTS');

-- --------------------------------------------------------

--
-- Table structure for table `written_works`
--

CREATE TABLE `written_works` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `score` float NOT NULL,
  `perfect_score` float NOT NULL,
  `grade_id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `written_works`
--

INSERT INTO `written_works` (`id`, `title`, `score`, `perfect_score`, `grade_id`, `date`) VALUES
(20, 'dd', 22, 22, 199, '2017-02-28'),
(21, '33', 33, 33, 200, '2017-02-28'),
(22, '22', 22, 22, 201, '2017-02-01'),
(23, '22', 22, 22, 202, '2017-02-01');

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

CREATE TABLE `years` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `years`
--

INSERT INTO `years` (`id`, `name`) VALUES
(1, 'Grade 1'),
(2, 'Grade 2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade_categories`
--
ALTER TABLE `grade_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quarterly_assessments`
--
ALTER TABLE `quarterly_assessments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_dates`
--
ALTER TABLE `school_dates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_records`
--
ALTER TABLE `student_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_performances`
--
ALTER TABLE `task_performances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`username`);

--
-- Indexes for table `usertypes`
--
ALTER TABLE `usertypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `written_works`
--
ALTER TABLE `written_works`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `years`
--
ALTER TABLE `years`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `grade_categories`
--
ALTER TABLE `grade_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;
--
-- AUTO_INCREMENT for table `quarterly_assessments`
--
ALTER TABLE `quarterly_assessments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `school_dates`
--
ALTER TABLE `school_dates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `student_records`
--
ALTER TABLE `student_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `task_performances`
--
ALTER TABLE `task_performances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `usertypes`
--
ALTER TABLE `usertypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `written_works`
--
ALTER TABLE `written_works`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `years`
--
ALTER TABLE `years`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
