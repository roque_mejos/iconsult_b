<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Year_lib {

	private static $CI;

	public function __construct() {
		self::$CI =& get_instance();
	}

	public function read_data_sql() {
		$sql = "
			SELECT
				`". YEARS ."`.*
			FROM
				`". YEARS ."`
			ORDER BY
				`". YEARS ."`.`id`
			ASC
		";
		return $sql;
	}
}