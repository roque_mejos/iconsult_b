<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Event_lib {

	private static $CI;

	public function __construct() {
		self::$CI =& get_instance();
	}

	public function insert_data_sql($data) {
		$sql = "
			INSERT INTO
				`". EVENTS ."`
				(
					`". EVENTS ."`.`title`,
					`". EVENTS ."`.`date`
				)
			VALUES
				(
					". self::$CI->db->escape($data->title) .",
					". self::$CI->db->escape($data->date) ."
				)
		";
		return $sql;
	}

	public function count_data_by_title_and_date_sql($title, $date) {
		$sql = "
			SELECT
				COUNT(*) as `count_data`
			FROM
				`". EVENTS ."`
			WHERE
				`". EVENTS ."`.`title` = ". self::$CI->db->escape($title) ."
			AND
				`". EVENTS ."`.`date` = ". self::$CI->db->escape($date) ."
		";
		return $sql;
	}

	public function read_data_sql() {
		$sql = "
			SELECT
				`". EVENTS ."`.`id`,
				`". EVENTS ."`.`title`,
				`". EVENTS ."`.`date`
			FROM
				`". EVENTS ."`
			ORDER BY
				DATE(`". EVENTS ."`.`date`)
			DESC
		";
		return $sql;
	}

	public function delete_data_by_event_id_sql($event_id) {
		$sql = "
			DELETE FROM
				`". EVENTS ."`
			WHERE
				`". EVENTS ."`.`id` = ". self::$CI->db->escape($event_id) ."
		";
		return $sql;
	}

	public function read_data_by_event_id_sql($event_id) {
		$sql = "
			SELECT
				`". EVENTS ."`.`id`,
				`". EVENTS ."`.`title`,
				`". EVENTS ."`.`date`
			FROM
				`". EVENTS ."`
			WHERE
				`". EVENTS ."`.`id` = ". self::$CI->db->escape($event_id) ."
		";
		return $sql;
	}

	public function count_data_by_event_id_sql($event_id) {
		$sql = "
			SELECT
				COUNT(*) as `count_data`
			FROM
				`". EVENTS ."`
			WHERE
				`". EVENTS ."`.`id` = ". self::$CI->db->escape($event_id) ."
		";
		return $sql;
	}

	public function update_data_by_event_id_sql($data, $event_id) {
		$sql = "
			UPDATE
				`". EVENTS ."`
			SET
				`". EVENTS ."`.`title` = ". self::$CI->db->escape($data->title) .",
				`". EVENTS ."`.`date` = ". self::$CI->db->escape($data->date) ."
			WHERE
				`". EVENTS ."`.`id` = ". self::$CI->db->escape($event_id) ."
		";
		return $sql;
	}
}