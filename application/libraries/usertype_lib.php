<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Usertype_lib {

	public function read_data_sql() {
		$sql = "SELECT 
					`". USERTYPES ."`.*
				FROM
					`". USERTYPES ."`
				ORDER BY
					`". USERTYPES ."`.`id`
				ASC
		";
		return $sql;
	}
}