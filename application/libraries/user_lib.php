<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_lib {

	private static $CI;

	public function __construct() {
		self::$CI =& get_instance();
	}

	public function check_user_sql($data) {
		$sql = "
			SELECT
				`". USERS ."`.`id`,
				`". USERS ."`.`fname`,
				`". USERS ."`.`lname`,
				`". USERS ."`.`usertype`,
				`". USERS ."`.`gender`,
				`". USERS ."`.`image`,
				`". USERS ."`.`username`,
				`". USERTYPES ."`.`name`
			FROM
				`". USERS ."`
			LEFT JOIN
				`". USERTYPES ."`
			ON
				`". USERS ."`.`usertype` = `". USERTYPES ."`.`id`
			WHERE
				`". USERS ."`.`username` = ". self::$CI->db->escape($data->username) ."
			AND
				`". USERS ."`.`password` = ". self::$CI->db->escape(md5($data->password)) ."
		";
		return $sql;
	}

	public function count_user_sql($data) {
		$sql = "
			SELECT
				COUNT(*) AS `count_data`
			FROM
				`". USERS ."`
			LEFT JOIN
				`". USERTYPES ."`
			ON
				`". USERS ."`.`usertype` = `". USERTYPES ."`.`id`
			WHERE
				`". USERS ."`.`username` = ". self::$CI->db->escape($data->username) ."
			AND
				`". USERS ."`.`password` = ". self::$CI->db->escape(md5($data->password)) ."
		";
		return $sql;
	}

	public function read_data_not_by_users_and_admin_sql($user_id) {
		$sql = "
			SELECT
				`". USERS ."`.`id`,
				`". USERS ."`.`fname`,
				`". USERS ."`.`lname`,
				`". USERS ."`.`mi`,
				`". USERS ."`.`mobile`,
				`". USERS ."`.`bdate`,
				`". USERS ."`.`username`,
				`". USERTYPES ."`.`name` AS usertype_name,
				`". GENDERS ."`.`name` AS gender_name
			FROM
				`". USERS ."`
			LEFT JOIN
				`". USERTYPES ."`
			ON
				`". USERS ."`.`usertype` = `". USERTYPES ."`.`id`
			LEFT JOIN
				`". GENDERS ."`
			ON
				`". USERS ."`.`gender` = `". GENDERS ."`.`id`
			WHERE
				`". USERS ."`.`id` != ". self::$CI->db->escape($user_id) ."
			AND
				`". USERS ."`.`usertype` != ". self::$CI->db->escape(ADMIN) ."
			ORDER BY
				`". USERS ."`.`fname`
		";
		return $sql;
	}

	public function delete_data_by_user_id_sql($user_id) {
		$sql = "
			DELETE FROM
				`". USERS ."`
			WHERE
				`". USERS ."`.`id` = ". self::$CI->db->escape($user_id) ."
		";
		return $sql;
	}

	public function read_data_by_user_id_sql($user_id) {
		$sql = "
			SELECT
				`". USERS ."`.`id`,
				`". USERS ."`.`fname`,
				`". USERS ."`.`lname`,
				`". USERS ."`.`mi`,
				`". USERS ."`.`username`,
				`". USERS ."`.`bdate`,
				`". USERS ."`.`gender`,
				`". USERS ."`.`mobile`,
				`". USERS ."`.`address`,
				`". USERTYPES ."`.`name` AS usertype_name
			FROM
				`". USERS ."`
			LEFT JOIN
				`". USERTYPES ."`
			ON
				`". USERS ."`.`usertype` = `". USERTYPES ."`.`id`
			WHERE
				`". USERS ."`.`id` = ". self::$CI->db->escape($user_id) ."
		";
		return $sql;
	}

	public function count_data_by_user_id_sql($user_id) {
		$sql = "
			SELECT
				COUNT(*) AS `count_data`
			FROM
				`". USERS ."`
			WHERE
				`". USERS ."`.`id` = ". self::$CI->db->escape($user_id) ."
		";
		return $sql;
	}

	public function count_data_by_username_and_not_user_id_sql($username, $user_id) {
		$sql = "
			SELECT 
				COUNT(*) AS `count_data`
			FROM
				`". USERS ."`
			WHERE
				`". USERS ."`.`username` = ". self::$CI->db->escape($username) ."
			AND
				`". USERS ."`.`id` != ". self::$CI->db->escape($user_id) ."
		";
		return $sql;
	}

	public function update_data_by_user_id_sql($user_id, $data) {
		$set_data = "";
		$set = array();
		$set[] = "`". USERS ."`.`fname` = ". self::$CI->db->escape($data->fname) ."";
		$set[] = "`". USERS ."`.`lname` = ". self::$CI->db->escape($data->lname) ."";
		$set[] = "`". USERS ."`.`mi` = ". self::$CI->db->escape($data->mi) ."";
		$set[] = "`". USERS ."`.`bdate` = ". self::$CI->db->escape($data->bdate) ."";
		$set[] = "`". USERS ."`.`gender` = ". self::$CI->db->escape($data->gender) ."";
		$set[] = "`". USERS ."`.`mobile` = ". self::$CI->db->escape($data->mobile) ."";
		$set[] = "`". USERS ."`.`address` = ". self::$CI->db->escape($data->address) ."";
		if(isset($data->username)) {
			$set[] = "`". USERS ."`.`username` = ". self::$CI->db->escape($data->username) ."";
		}
		if(isset($data->image)) {
			if(strlen($data->image) > 0) {
				$set[] = "`". USERS ."`.`image` = ". self::$CI->db->escape($data->image) ."";
			}
		}
		$set_data = join(',', $set);
		$sql = "
			UPDATE
				`". USERS ."`
			SET
				". $set_data ."
			WHERE
				`". USERS ."`.`id` = ". self::$CI->db->escape($user_id) ."
		";
		return $sql;
	}

	public function insert_data_sql($data) {
		$sql = "
			INSERT INTO
				`". USERS ."`(
					`". USERS ."`.`fname`,
					`". USERS ."`.`lname`,
					`". USERS ."`.`mi`,
					`". USERS ."`.`username`,
					`". USERS ."`.`password`,
					`". USERS ."`.`usertype`,
					`". USERS ."`.`bdate`,
					`". USERS ."`.`gender`,
					`". USERS ."`.`mobile`,
					`". USERS ."`.`address`
				)
			VALUES(
				". self::$CI->db->escape($data->fname) .",
				". self::$CI->db->escape($data->lname) .",
				". self::$CI->db->escape($data->mi) .",
				". self::$CI->db->escape($data->username) .",
				". self::$CI->db->escape($data->password) .",
				". self::$CI->db->escape($data->usertype) .",
				". self::$CI->db->escape($data->bdate) .",
				". self::$CI->db->escape($data->gender) .",
				". self::$CI->db->escape($data->mobile) .",
				". self::$CI->db->escape($data->address) ."
			)
		";
		return $sql;
	}

	public function read_password_by_user_id_sql($user_id) {
		$sql = "
			SELECT
				`". USERS ."`.`password`
			FROM
				`". USERS ."`
			WHERE
				`". USERS ."`.`id` = ". self::$CI->db->escape($user_id) ."
		";
		return $sql;
	}

	public function update_password_by_user_id_sql($password, $user_id) {
		$sql = "
			UPDATE
				`". USERS ."`
			SET
				`". USERS ."`.`password` = ". self::$CI->db->escape($password) ."
			WHERE
				`". USERS ."`.`id` = ". self::$CI->db->escape($user_id) ."
		";
		return $sql;
	}

	public function read_name_by_parent_usertype_sql() {
		$sql = "
			SELECT
				`". USERS ."`.`id`,
				CONCAT(`". USERS ."`.fname, ' ', `". USERS ."`.`mi`, '. ', `". USERS ."`.`lname`) AS `name`
			FROM
				`". USERS ."`
			WHERE
				`". USERS ."`.`usertype` = ". PARENTS ."
			ORDER BY
				`". USERS ."`.`fname`
		";
		return $sql;
	}
}