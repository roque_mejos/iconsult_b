<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Subject_lib {

	private static $CI;

	public function __construct() {
		self::$CI =& get_instance();
	}

	public function read_data_sql() {
		$sql = "
			SELECT
				`". SUBJECTS ."`.*
			FROM
				`". SUBJECTS ."`
			ORDER BY
				`". SUBJECTS ."`.`name`
			ASC
		";
		return $sql;
	}

	public function read_data_by_active_sql() {
		$sql = "
			SELECT
				`". SUBJECTS ."`.*
			FROM
				`". SUBJECTS ."`
			WHERE
				`". SUBJECTS ."`.`status` = true
			ORDER BY
				`". SUBJECTS ."`.`name`
			ASC
		";
		return $sql;
	}

	public function deactivate_status_by_subject_id_sql($subject_id) {
		$sql = "
			UPDATE
				`". SUBJECTS ."`
			SET
				`". SUBJECTS ."`.`status` = false
			WHERE
				`". SUBJECTS ."`.`id` = ". self::$CI->db->escape($subject_id) ."
		";
		return $sql;
	}

	public function activate_status_by_subject_id_sql($subject_id) {
		$sql = "
			UPDATE
				`". SUBJECTS ."`
			SET
				`". SUBJECTS ."`.`status` = true
			WHERE
				`". SUBJECTS ."`.`id` = ". self::$CI->db->escape($subject_id) ."
		";
		return $sql;
	}

	public function insert_data_sql($subject) {
		$sql = "
			INSERT INTO
				`". SUBJECTS ."`(`". SUBJECTS ."`.`name`)
			VALUES(". self::$CI->db->escape($subject) .")
		";
		return $sql;
	}
}