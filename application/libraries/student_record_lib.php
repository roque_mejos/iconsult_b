<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Student_record_lib {

	private static $CI;

	public function __construct() {
		self::$CI =& get_instance();
	}

	public function insert_data_sql($subject_id, $student_id) {
		$sql = "
			INSERT INTO
				`". STUDENT_RECORDS ."`(
					`". STUDENT_RECORDS ."`.`subject_id`,
					`". STUDENT_RECORDS ."`.`student_id`
				)
			VALUES
				(
					". self::$CI->db->escape($subject_id) .",
					". self::$CI->db->escape($student_id) ."
				)

		";
		return $sql;
	}

	public function read_data_by_student_id_sql($student_id) {
		$sql = "
			SELECT
				`". SUBJECTS ."`.`name` AS `subject`,
				`". STUDENT_RECORDS ."`.`id`,
				`". STUDENT_RECORDS ."`.`student_id`,
				`". STUDENT_RECORDS ."`.`subject_id`
			FROM
				`". STUDENT_RECORDS ."`
			LEFT JOIN
				`". SUBJECTS ."`
			ON
				`". STUDENT_RECORDS ."`.`subject_id` = `". SUBJECTS ."`.`id`
			WHERE
				`". STUDENT_RECORDS ."`.`student_id` = ". self::$CI->db->escape($student_id) ."
			AND
				`". SUBJECTS ."`.`status` = true
			ORDER BY
				`". SUBJECTS ."`.`name`
		";
		return $sql;
	}

	public function count_data_by_student_id_sql($student_id) {
		$sql = "
			SELECT
				COUNT(*) as `count_data`
			FROM
				`". STUDENT_RECORDS ."`
			WHERE
				`". STUDENT_RECORDS ."`.`student_id` = ". self::$CI->db->escape($student_id) ."
		";
		return $sql;
	}

	public function update_grade_by_student_record_id_sql($data) {
		$sql = "
			UPDATE
				`". STUDENT_RECORDS ."`
			SET
				`". STUDENT_RECORDS ."`.`grade` = ". self::$CI->db->escape($data->grade) ."
			WHERE
				`". STUDENT_RECORDS ."`.`id` = ". self::$CI->db->escape($data->student_record_id) ."
		";
		return $sql;
	}

	public function delete_data_by_student_id_sql($student_id) {
		$sql = "
			DELETE FROM
				`". STUDENT_RECORDS ."`
			WHERE
				`". STUDENT_RECORDS ."`.`student_id` = ". self::$CI->db->escape($student_id) ."
		";
		return $sql;
	}

	public function delete_data_not_by_subject($subject, $student_id) {
		$subject = join(', ', $subject);
		$sql = "
			DELETE FROM
				`". STUDENT_RECORDS ."`
			WHERE
				`". STUDENT_RECORDS ."`.`subject_id` NOT IN(". $subject .")
			AND
				`". STUDENT_RECORDS .".`student_id` = ". self::$CI->db->escape($student_id) ."
		";
		return $sql;
	}
}