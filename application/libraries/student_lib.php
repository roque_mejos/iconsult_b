<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Student_lib {

	private static $CI;

	public function __construct() {
		self::$CI =& get_instance();
	}

	public function insert_data_sql($data) {
		$sql = "
			INSERT INTO
				`". STUDENTS ."`(
					`". STUDENTS ."`.`fname`,
					`". STUDENTS ."`.`lname`,
					`". STUDENTS ."`.`mi`,
					`". STUDENTS ."`.`mobile`,
					`". STUDENTS ."`.`address`,
					`". STUDENTS ."`.`bdate`,
					`". STUDENTS ."`.`year_id`,
					`". STUDENTS ."`.`parent`,
					`". STUDENTS ."`.`gender`,
					`". STUDENTS ."`.`section_id`,
					`". STUDENTS ."`.`teacher`,
					`". STUDENTS ."`.`ref_no`,
					`". STUDENTS ."`.`school_year`
				)
			VALUES(
				". self::$CI->db->escape($data->fname) .",
				". self::$CI->db->escape($data->lname) .",
				". self::$CI->db->escape($data->mi) .",
				". self::$CI->db->escape($data->mobile) .",
				". self::$CI->db->escape($data->address) .",
				". self::$CI->db->escape($data->bdate) .",
				". self::$CI->db->escape($data->year_id) .",
				". self::$CI->db->escape($data->user_id) .",
				". self::$CI->db->escape($data->gender) .",
				". self::$CI->db->escape($data->section_id) .",
				". self::$CI->db->escape(self::$CI->session->id) .",
				". self::$CI->db->escape($data->ref_no) .",
				". self::$CI->db->escape(date('Y')) ."
			)

		";
		return $sql;
	}

	public function read_data_by_teacher_sql($teacher) {
		$sql = "
			SELECT
				`". STUDENTS ."`.*,
				`". YEARS ."`.`name` AS `year`,
				`sections`.`name` AS `section`
			FROM
				`". STUDENTS ."`
			LEFT JOIN
				`". YEARS ."`
			ON
				`". STUDENTS ."`.`year_id` = `". YEARS ."`.`id`
			LEFT JOIN
				`sections`
			ON
				`sections`.`id` = `". STUDENTS ."`.`section_id`
			WHERE
				`". STUDENTS ."`.`teacher` = ". self::$CI->db->escape($teacher) ."
			ORDER BY
				`". STUDENTS ."`.`fname`
			ASC
		";
		return $sql;
	}

	public function read_data_by_parent_sql($parent) {
		$sql = "
			SELECT
				`". STUDENTS ."`.*,
				`". YEARS ."`.`name` AS `year`,
				`sections`.`name` AS `section`
			FROM
				`". STUDENTS ."`
			LEFT JOIN
				`". YEARS ."`
			ON
				`". STUDENTS ."`.`year_id` = `". YEARS ."`.`id`
			LEFT JOIN
				`sections`
			ON
				`sections`.`id` = `". STUDENTS ."`.`section_id`
			WHERE
				`". STUDENTS ."`.`parent` = ". self::$CI->db->escape($parent) ."
			ORDER BY
				`". STUDENTS ."`.`fname`
			ASC
		";
		return $sql;
	}

	public function read_data_by_student_id_sql($student_id) {
		$sql = "
			SELECT
				CONCAT(`". STUDENTS ."`.`fname`, ' ', `". STUDENTS ."`.`mi`, '. ', `". STUDENTS ."`.`lname`) AS `name`,
				`". STUDENTS ."`.`id`,
				`". STUDENTS ."`.`fname`,
				`". STUDENTS ."`.`lname`,
				`". STUDENTS ."`.`mi`,
				`". STUDENTS ."`.`bdate`,
				`". STUDENTS ."`.`address`,
				`". STUDENTS ."`.`mobile`,
				`". STUDENTS ."`.`gender`,
				`". STUDENTS ."`.`parent`,
				`". STUDENTS ."`.`teacher`,
				`". STUDENTS ."`.`ref_no`,
				`". STUDENTS ."`.`section_id` AS section,
				`". YEARS ."`.`id` AS `year_id`,
				`". YEARS ."`.`name` AS `year`,
				`sections`.name AS `sections`
			FROM
				`". STUDENTS ."`
			LEFT JOIN
				`". YEARS ."`
			ON
				`". STUDENTS ."`.`year_id` = `". YEARS ."`.`id`
			LEFT JOIN
				`sections`
			ON
				`". STUDENTS ."`.`section_id` = `sections`.`id`
			WHERE
				`". STUDENTS ."`.`id` = ". self::$CI->db->escape($student_id) ."
		";
		return $sql;
	}

	public function delete_data_by_student_id_sql($student_id) {
		$sql = "
			DELETE FROM
				`". STUDENTS ."`
			WHERE
				`". STUDENTS ."`.id = ". self::$CI->db->escape($student_id) ."
		";
		return $sql;
	}

	public function count_data_by_student_id_sql($student_id) {
		$sql = "
			SELECT
				COUNT(*) AS `count_data`
			FROM
				`". STUDENTS ."`
			WHERE
				`". STUDENTS ."`.id = ". self::$CI->db->escape($student_id) ."
		";
		return $sql;
	}

	public function update_data_by_student_id_sql($data, $student_id) {
		$sql = "
			UPDATE
				`". STUDENTS ."`
			SET
				`". STUDENTS ."`.`fname` = ". self::$CI->db->escape($data->fname) .",
				`". STUDENTS ."`.`lname` = ". self::$CI->db->escape($data->lname) .",
				`". STUDENTS ."`.`mi` = ". self::$CI->db->escape($data->mi) .",
				`". STUDENTS ."`.`bdate` = ". self::$CI->db->escape($data->bdate) .",
				`". STUDENTS ."`.`mobile` = ". self::$CI->db->escape($data->mobile) .",
				`". STUDENTS ."`.`address` = ". self::$CI->db->escape($data->address) .",
				`". STUDENTS ."`.`year_id` = ". self::$CI->db->escape($data->year_id) .",
				`". STUDENTS ."`.`parent` = ". self::$CI->db->escape($data->user_id) .",
				`". STUDENTS ."`.`gender` = ". self::$CI->db->escape($data->gender) ."
			WHERE
				`". STUDENTS ."`.id = ". self::$CI->db->escape($student_id) ."
		";
		return $sql;
	}
}