<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gender_lib {

	public function read_data_sql() {
		$sql = "
			SELECT
				`". GENDERS ."`.*
			FROM
				`". GENDERS ."`
			ORDER BY
				`". GENDERS ."`.`id`
			ASC
		";
		return $sql;
	}
}