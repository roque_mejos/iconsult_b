<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement_lib {

	private static $CI;

	public function __construct() {
		self::$CI =& get_instance();
	}

	public function read_data_sql() {
		$sql = "
			SELECT
				`". ANNOUNCEMENT ."`.`id`,
				`". ANNOUNCEMENT ."`.`title`,
				`". ANNOUNCEMENT ."`.`message`,
				`". ANNOUNCEMENT ."`.`created`,
				CONCAT(`". USERS ."`.`fname`, ' ', `". USERS ."`.`lname`) as `name`
			FROM
				`". ANNOUNCEMENT ."`
			LEFT JOIN
				`". USERS ."`
			ON
				`". ANNOUNCEMENT ."`.`user_id` = `". USERS ."`.`id`
			ORDER BY
				DATE(`". ANNOUNCEMENT ."`.`created`)
			DESC
		";
		return $sql;
	}

	public function insert_data_sql($data) {
		$sql = "
			INSERT INTO
				`". ANNOUNCEMENT ."`(`". ANNOUNCEMENT ."`.`title`, `". ANNOUNCEMENT ."`.`message`, `". ANNOUNCEMENT ."`.`user_id`)
			VALUES
				(
					". self::$CI->db->escape($data->title) .",
					". self::$CI->db->escape($data->message) .",
					". self::$CI->db->escape(self::$CI->session->id) ."
				)
		";
		return $sql;
	}

	public function delete_data_by_announcement_id_sql($announcement_id) {
		$sql = "
			DELETE FROM
				`". ANNOUNCEMENT ."`
			WHERE
				`". ANNOUNCEMENT ."`.`id` = ". self::$CI->db->escape($announcement_id) ."
		";
		return $sql;
	}

	public function read_data_by_announcement_id_sql($announcement_id) {
		$sql = "
			SELECT 
				`". ANNOUNCEMENT ."`.*
			FROM
				`". ANNOUNCEMENT ."`
			WHERE
				`". ANNOUNCEMENT ."`.`id` = ". self::$CI->db->escape($announcement_id) ."
		";
		return $sql;
	}

	public function count_data_by_announcement_id_sql($announcement_id) {
		$sql = "
			SELECT 
				COUNT(*) AS `count_data`
			FROM
				`". ANNOUNCEMENT ."`
			WHERE
				`". ANNOUNCEMENT ."`.`id` = ". self::$CI->db->escape($announcement_id) ."
		";
		return $sql;
	}

	public function update_data_by_announcement_id_sql($data, $announcement_id) {
		$sql = "
			UPDATE
				`". ANNOUNCEMENT ."`
			SET
				`". ANNOUNCEMENT ."`.`title` = ". self::$CI->db->escape($data->title) .",
				`". ANNOUNCEMENT ."`.`message` = ". self::$CI->db->escape($data->message) ."
			WHERE
				`". ANNOUNCEMENT ."`.`id` = ". self::$CI->db->escape($announcement_id) ."
		";
		return $sql;
	}
}