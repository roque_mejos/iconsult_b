<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">Grade Levels</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="grade-table">
					<thead>
						<tr>
							<td>Grade Level</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach($year as $row): ?>
							<tr>
								<td><?php echo $row->name?></td>
								<td><a href="<?php echo base_url('year/detail/'.$row->id) ?>">Edit</a> | <a href="<?php echo base_url('year/delete/' . $row->id) ?>" class="delete" data-id="<?php echo $row->id ?>">Delete</a></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript">
	$(function() {
		$('#grade-table').dataTable();
		$('.delete').click(function() {
			if(!confirm('Are you sure?')) {
				return false;
			}
		});
	});
</script>