<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">Add Year</div>
		<div class="panel-body">
			<?php echo form_open('', array('role' => 'form', 'id' => 'form')); ?>
				<input type="hidden" name="year_id"  value="<?php echo $year_id?>">
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group">
							<label>Grade Level</label>
							<input type="text" name="year" class="form-control" placeholder="Grade Level" value="<?php echo $year ?>"> 
						</div>
					</div>
				</div>
				<?php if(count($section) == 0):?>
					<div class="row section-form">
						<div class="col-lg-5">
							<div class="form-group">
								<label>Section</label>
								<input type="text" name="section[]" class="form-control" placeholder="Section" value="">
								<button type="button" class="addme btn btn-success" style="margin-top: 5px;">+</button>
								<button type="button" class="btn btn-danger" style="margin-top: 5px;">-</button>
							</div>
						</div>
					</div>
				<?php endif?>
				<?php 
				$flag = false;
				foreach($section as $row):
					if($flag != true) {
						$class="";
						$flag = true;
					} else {
						$class="removeme ";
					}
				?>
					<div class="row section-form">
						<div class="col-lg-5">
							<div class="form-group">
								<label>Section</label>
								<input type="text" name="section[]" class="form-control" placeholder="Section" value="<?php echo $row->name; ?>">
								<button type="button" class="addme btn btn-success" style="margin-top: 5px;">+</button>
								<button type="button" class="<?php echo $class?>btn btn-danger" style="margin-top: 5px;">-</button>
							</div>
						</div>
					</div>
				<?php endforeach?>
				<button type="submit" class="btn btn-primary">Update</button>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('body').on('click', '.addme', function() {
			$(this).parent().parent().parent().after('<div class="row section-form"><div class="col-lg-5"><div class="form-group"><label>Section</label><input type="text" name="section[]" class="form-control" placeholder="Section" value=""><button type="button" class="addme btn btn-success" style="margin-top: 5px;">+</button> <button type="button" class="removeme btn btn-danger" style="margin-top: 5px;">-</button></div></div></div>');
		});
		$('body').on('click', '.removeme', function() {
			$(this).parent().parent().parent().detach();
		});
		$('#form').submit(function(e) {
			e.preventDefault();
			$.ajax({
				type: 'post',
				url: '<?php echo base_url('year/update'); ?>',
				data: $(this).serialize(),
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				dataType: 'json',
				success: function(data) {
					var success = data.success;
					if(!data.session) {
						window.location = '<?php echo base_url(); ?>';
					} else {
						bootbox.alert({
							title: 'Message',
							message: data.message,
							callback: function() {
								if(success) {
									window.location = '<?php echo base_url('year/detail/' . $year_id); ?>';
								}
							}
						});
					}
				}
			});
		});
	});
</script>