<div class="col-lg-12">
	<div class="row">
		<div class="col-lg-5">
			<?php echo form_open('grade/search', array('role' => 'form')); ?>
				<div class="form-group">
					<label>Enter Reference Number:</label>
					<input type="" class="form-control" name="ref_no" value="" placeholder="Reference Number">
				</div>
				<button class="btn btn-primary" type="submit">Search</button>
			</form>
		</div>
	</div><br>
	<?php if($count_student): ?>
		<div class="panel panel-default">
			<div class="panel-heading"><strong></strong></div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="student-table">
						<thead>
							<tr>
								<th>Reference No.</th>
								<th>Firstname</th>
								<th>Lastname</th>
								<th>MI</th>
								<th>Age</th>
								<th>Mobile</th>
								<th>Address</th>
								<th>Year Level</th>
								<th>Section</th>
								<th>Report Card</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $student->ref_no; ?></td>
								<td><?php echo $student->fname; ?></td>
								<td><?php echo $student->lname; ?></td>
								<td><?php echo $student->mi; ?></td>
								<td><?php echo age(date('m/d/Y', strtotime($student->bdate))); ?></td>
								<td><?php echo $student->mobile; ?></td>
								<td><?php echo $student->address; ?></td>
								<td><?php echo $student->year; ?></td>
								<td><?php echo $student->sections; ?></td>
								<td><a href="<?php echo base_url('student/reports/' . $student->id); ?>">Download</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php echo $contents; ?>
</div>