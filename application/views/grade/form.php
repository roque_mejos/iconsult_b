<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Grading</div>
			<div class="panel-body">
				<?php if(count($student_record) > 0): ?>
					<div class="table-responsive">
						<table class="table table-hover" id="student-table">
							<thead>
								<tr>
									<th>Subject</th>
									<th colspan="2">Written Works(40%)</th>
									<th colspan="2">Task Performance(40%)</th>
									<th colspan="2">Quarterly assessment(20%)</th>
									<th>1st</th>
									<th>2nd</th>
									<th>3rd</th>
									<th>4th</th>
									<th>Final Ratings</th>
								</tr>
							</thead>
							
							
						</table>
					</div>
				<?php else: ?>
					<p>No Subject found</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
