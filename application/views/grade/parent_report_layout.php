<br>
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-info">
			<i class="fa fa-folder-open"></i>&nbsp;Student name: <b><?php echo $name; ?> </b>
		</div>
	</div>
</div>
<?php
foreach(GRADING_TYPE as $type): ?>

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading"><?php echo $type; ?> Grading</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover" id="student-table">
							<thead>
								<tr>
									<th>Subject</th>
									<th>Written Works(40%)</th>
									<th>Task Performance(40%)</th>
									<th>Quarterly Assessment(20%)</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($grade[$type] as $row): ?>
									<tr>
										<td><?php echo $row['subject'] ?></td>
										<td><?php echo $row['written_work'] ?></td>
										<td><?php echo $row['task_performance'] ?></td>
										<td><?php echo $row['quarterly_assessment'] ?></td>
										<td class="total_subject"><?php echo $row['total_per_subject']; ?>%</td>
									</tr>
								<?php endforeach ?>
							</tbody>
							<thead>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th><?php echo $grading_total[$type]; ?>%</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php endforeach; ?>
<strong class="pull-right" style="margin-right: 20px; font-size: 14px">General Average: <?php echo number_format($gen_ave,2); ?></strong>
<hr>
<div style="clear: both"></div>
<a href="<?php echo base_url('grade/report/' . $student_id); ?>" class="btn btn-danger btn-block">Download PDF</a>
<br>