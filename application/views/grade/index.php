<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>List of Students</strong></div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="student-table">
					<thead>
						<tr>
							<th>Reference No.</th>
							<th>Firstname</th>
							<th>Lastname</th>
							<th>MI</th>
							<th>Attendance</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($student as $row) { ?>
							<tr>
								<td><?php echo $row->ref_no; ?></td>
								<td><?php echo $row->fname; ?></td>
								<td><?php echo $row->lname; ?></td>
								<td><?php echo $row->mi; ?></td>
								<td><a href="<?php echo base_url('grade/detail/' . $row->id) ?>">Grade</a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript">
	$(function() {
		var student_table = $('table#student-table');
		student_table.dataTable();
	});
</script>