<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-info">
			<i class="fa fa-folder-open"></i>&nbsp;Student name: <b><?php echo $name; ?> </b>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-heading">Grades</div>
			<div class="panel-body">
				<?php if(count($student_record) > 0): ?>
					<div class="table-responsive">
						<table class="table table-hover" id="student-table">
							<thead>
								<tr>
									<th>Subject</th>
									<th>Written Works(40%)</th>
									<th>Task Performance(40%)</th>
									<th>Quarterly assessment(20%)</th>
									<th>1st</th>
									<th>2nd</th>
									<th>3rd</th>
									<th>4th</th>
									<th>Final Ratings</th>
								</tr>
							</thead>
							
							<tbody>
								<?php 
									$i = 0;
									$sub_id = 0;
								 foreach($subject as $row): 
									$sub_id = $subject_id[$i];
									++$i;?>
									<tr>
										<td><?php echo $row ?></td>

										<td><a href="<?php echo base_url('written_work?student_id='. $student_id .'&subject_id=' . $sub_id); ?>" title="Written Works (<?php echo $row ?>)"><img src="<?php echo base_url('assets/img/edit.png'); ?>" style="max-width: 22px;"></a></td>
										<td><a href="<?php echo base_url('task_performance?student_id='. $student_id .'&subject_id=' . $sub_id); ?>" title="Task Performance (<?php echo $row ?>)"><img src="<?php echo base_url('assets/img/edit.png'); ?>" style="max-width: 22px;"></a></td>
										<td><a href="<?php echo base_url('quarterly_assessment?student_id='. $student_id .'&subject_id=' . $sub_id); ?>" title="Quarterly assessment (<?php echo $row ?>)"><img src="<?php echo base_url('assets/img/edit.png'); ?>" style="max-width: 22px;"></a></td>

										<td><?php echo $first_grading_total[$row]; ?></td>
										<td><?php echo $second_grading_total[$row]; ?></td>
										<td><?php echo $third_grading_total[$row]; ?></td>
										<td><?php echo $fourth_grading_total[$row]; ?></td>
										<td><strong><?php echo $first_ave[$row]; ?></strong></td>
									</tr>
								<?php endforeach; ?>
							</tbody>

						</table>
					</div>
				<?php else: ?>
					<p>No Subject found</p>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<a href="<?php echo base_url('grade'); ?>" class="btn btn-info">Back</a>
<strong class="pull-right" style="margin-right: 20px; font-size: 14px">General Average: <?php echo number_format($ave,2); ?></strong>
<hr>
<div style="clear: both"></div>
<!-- <a href="<?php //echo base_url('grade/report/' . $student_id); ?>" class="btn btn-danger btn-block">Download PDF</a> -->
<br>