<div class="col-lg-12">
	<div class="row">
		<div class="col-lg-4">
			<div class="panel panel-default">
				<div class="panel-heading"><strong><?php echo $subject; ?> (<span class="grading_type_span">1st</span> Grading)</strong></div>
				<div class="panel-body">
					<?php echo form_open('', array('role' => 'form', 'id' => 'form')); ?>
						<input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
						<input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>">
						<input type="hidden" name="id" value="" class="cleartxt">
						<input type="hidden" name="grade_id" value="<?php echo $grade_id; ?>">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Grading type</label>
									<select name="grading_type" class="form-control">
										<?php foreach(GRADING_TYPE as $grading_type): ?>
											<option value="<?php echo $grading_type; ?>"><?php echo $grading_type; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Title</label>
									<input type="text" name="title" class="form-control cleartxt" placeholder="Title">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Score</label>
									<input type="text" name="score" class="form-control cleartxt" placeholder="Score">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Perfect Score</label>
									<input type="text" name="perfect_score" class="form-control cleartxt" placeholder="Perfect Score">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Date</label>
									<input type="text" name="date" class="form-control cleartxt" placeholder="Date">
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-primary">Save</button>
						<a href="<?php echo base_url('grade/detail/' . $student_id); ?>" class="btn btn-info">Back</a>
					<?php echo form_close() ?>
				</div>
			</div>
		</div>
		<div class="col-lg-8">
			<div class="panel panel-default">
				<div class="panel-heading"><strong>Quarterly Assessment scores</strong></div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="written-table">
							<thead>
								<tr>
									<th>Title</th>
									<th>Score</th>
									<th>Perfect Score</th>
									<th>Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody id="tbody">
								<?php foreach($activity as $row): ?>
									<tr>
										<td><?php echo $row->title?></td>
										<td><?php echo $row->score?></td>
										<td><?php echo $row->perfect_score?></td>
										<td><?php echo date('m/d/Y', strtotime($row->date))?></td>
										<td><a href="javascript:void(0);" class="edit" data-id="<?php echo $row->id?>">Edit</a> | <a href="javascript:void(0);" class="delete" data-id="<?php echo $row->id ?>">Delete</a></td>
									</tr>
								<?php endforeach?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript">
	$(function() {
		var written_table = $('table#written-table');
		// written_table.dataTable();
		$('input[name="date"]').datepicker();

		$('body').on('submit', '#form', function(e) {
			e.preventDefault();
			$('#tbody').html('');
			$.ajax({
				type: 'post',
				data: $(this).serialize(),
				dataType: 'json',
				url: '<?php echo base_url('quarterly_assessment/insert'); ?>',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					bootbox.alert({
						title: 'Message',
						message: result.message,
						callback: function() {
							if(!result.error) {
								reload_data(result);
							}
						}
					});
				}
			});
		});

		$('body').on('click', '.edit', function() {
			$.ajax({
				type: 'post',
				dataType: 'json',
				data: { id: $(this).data('id') },
				url: '<?php echo base_url('quarterly_assessment/detail'); ?>',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					if(!result.session) {
						window.location = '<?php base_url(); ?>';
					} else {
						$('input[name="title"]').val(result.title);
						$('input[name="score"]').val(result.score);
						$('input[name="perfect_score"]').val(result.perfect_score);
						$('input[name="date"]').val(result.date);
						$('input[name="id"]').val(result.id);
						$('form').attr('id', 'form_update');
						$('button[type="submit"]').text('Update');
					}
				}
			});
		});

		$('body').on('submit', '#form_update', function(e) {
			e.preventDefault();
			$('#tbody').html('');
			$.ajax({
				type: 'post',
				data: $(this).serialize(),
				dataType: 'json',
				url: '<?php echo base_url('quarterly_assessment/update'); ?>',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					bootbox.alert({
						title: 'Message',
						message: result.message,
						callback: function() {
							if(!result.error) {
								reload_data(result);
							}
						}
					});
				}
			});
		});

		$('body').on('click', '.delete', function() {
			var id = $(this).data('id');
			bootbox.confirm({
				title: 'Message',
				message: 'Are you sure?',
				callback: function(result) {
					if(result) {
						$('#tbody').html('');
						delete_data(id);
					}
				}
			});
		});

		function delete_data(id) {
			$.ajax({
				type: 'post',
				data: {id: id, subject_id: $('input[name="subject_id"]').val(), student_id: $('input[name="student_id"]').val(), grading_type: $('select[name="grading_type"]').val()},
				dataType: 'json',
				url: '<?php echo base_url('quarterly_assessment/delete'); ?>',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					bootbox.alert({
						title: 'Message',
						message: result.message,
						callback: function() {
							if(!result.error) {
								reload_data(result);
							}
						}
					});
				}
			});
		}

		$('select[name="grading_type"]').change(function() {
			var grading_type = $(this).val();
			$('span.grading_type_span').text(grading_type);
			search_table(grading_type);
		});

		function search_table(grading_type) {
			$('#tbody').html('');
			$.ajax({
				type: 'post',
				url: '<?php echo base_url('quarterly_assessment/search_grading_type'); ?>',
				data: { grading_type: grading_type, subject_id: $('input[name="subject_id"]').val(), student_id: $('input[name="student_id"]').val() },
				dataType: 'json',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					if(result.session != true) {
						window.location = '<?php echo base_url(); ?>';
						return false;
					}
					reload_data(result);
				}
			});
		}

		function reload_data(result) {
			for(var i in result.data) {
				$('<tr/>', {
					'html': [
						$('<td/>', { 'text': result.data[i].title }),
						$('<td/>', { 'text': result.data[i].score }),
						$('<td/>', { 'text': result.data[i].perfect_score }),
						$('<td/>', { 'text': result.data[i].date }),
						$('<td/>', { 'html': '<a href="#" class="edit" data-id="'+ result.data[i].id +'">Edit</a> | <a href="#" class="delete" data-id="'+ result.data[i].id +'">Delete</a>' })
					]
				}).appendTo('#tbody');
			}

			$('input[name="grade_id"]').val(result.grade_id);
			$('.cleartxt').val('');
		}

	});
</script>