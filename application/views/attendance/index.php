<div class="col-lg-5">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>Student Class</strong></div>
		<div class="panel-body">
			<?php echo form_open('attendance/filter', array('role' => 'form')); ?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group<?php echo $year_id_has_error; ?>">
							<label>Year Level</label>
							<select name="year_id" class="form-control">
								<?php foreach($year as $row): ?>
									<option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo $year_id_error; ?>
						</div>
						<div class="form-group">
							<label>Section</label>
							<select name="section_id" class="form-control<?php echo $section_id_has_error; ?>">
								<?php foreach($section as $row): ?>
									<option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
								<?php endforeach; ?>
							</select>
							<?php echo $section_id_error; ?>
						</div>
						<div class="form-group">
							<label>School Year</label>
							<select name="school_year" class="form-control<?php echo $school_year_has_error; ?>">
								<?php for($year = date('Y'); $year >= date('Y', strtotime('-10 year')); --$year): ?>
									<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
								<?php endfor?>
							</select>
							<?php echo $school_year_error; ?>
						</div>
						<button type="submit" class="btn btn-success">Filter</button>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('select[name="year_id"]').change(function() {
			$('select[name="section_id"]').html('');
			$.ajax({
				type: 'post',
				url: '<?php echo base_url('attendance/get_section'); ?>',
				data: { year_id: $(this).val() },
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				dataType: 'json',
				success: function(result) {
					for(var x in result.section) {
						$('<option/>', {'value': result.section[x].id, 'text': result.section[x].name}).appendTo('select[name="section_id"]');
					}
				}
			});
		});
	});
</script>