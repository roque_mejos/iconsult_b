<div class="col-lg-12">
	<div class="row">
		<div class="col-lg-12">
			<p>Year Level: <?php echo $year_level; ?></p>
			<p>Section: <?php echo $section; ?></p>
			<p>School Year: <?php echo $school_year; ?></p>
			<p>Teacher: <?php echo $this->session->fname . ' ' . $this->session->lname; ?></p>
		</div>
	</div>

	<div style="margin-top: 30px;">
		<div class="row">
			<div class="col-lg-2">
				<a href="<?php echo base_url('student'); ?>" class="btn btn-success">Add Student</a>
			</div>

			<div class="col-lg-3">
				<div class="form-group form-date">
					<input type="text" name="date" class="form-control" placeholder="Date" value="<?php echo date('m/d/Y'); ?>">
				</div>
			</div>
			<div class="col-lg-3 col-lg-offset-4">
				<div class="form-group">
					<input type="text" name="search" class="form-control" placeholder="Search" value="">
				</div>
			</div>
		</div>
	</div>

	<div style="margin-bottom: 5px; margin-top: 20px;">
		<div class="row">
			<div class="col-lg-1 col-lg-offset-5">
				<a href="<?php echo base_url('student/list'); ?>" class="btn btn-success">View Students</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<strong>Students</strong>
			</div>
			<div class="panel-body">
				<table class="table table-striped table-bordered table-hover" id="student-table">
					<thead>
						<tr>
							<th>No</th>
							<th>Reference No.</th>
							<th>Firstname</th>
							<th>Lastname</th>
							<th>MI</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbody">
						<?php
							$counter = 0; 
							foreach($student as $row): 
							++$counter;
						?>
							<tr>
								<td><?php echo $counter; ?></td>
								<td><?php echo $row->ref_no; ?></td>
								<td><?php echo $row->fname; ?></td>
								<td><?php echo $row->lname; ?></td>
								<td><?php echo $row->mi; ?>.</td>
								<td>
									<?php  
										if($status[$row->id]) {
											$present = 'checked';
											$absent = '';
										} else {
											$present = '';
											$absent = 'checked';
										}
									?>
									<form>Present <input type="radio" name="status" <?php echo $present; ?> data-id="<?php echo $row->id; ?>" value="<?php echo true; ?>"> | Absent <input type="radio" name="status" <?php echo $absent; ?> value="<?php echo false; ?>" data-id="<?php echo $row->id; ?>"></form>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
					<tfoot>
						<tr>
							<th>Total: <span id="total"><?php echo count($student); ?></span></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
		<a href="<?php echo base_url('attendance'); ?>" class="btn btn-info">Back</a>
		<?php if(count($student) == 0): ?>
			<button type="button" class="btn btn-primary save_button action_button" disabled="disabled" data-type="save">Save</button>
			<button type="button" class="btn btn-primary update_button action_button" disabled="disabled" data-type="update">Update</button>
		<?php else: ?>
			<?php if($count_school_date_id): ?>
				<button type="button" class="btn btn-primary save_button action_button" disabled="disabled" data-type="save">Save</button>
				<button type="button" class="btn btn-primary update_button action_button" data-type="update">Update</button>
			<?php else: ?>
				<button type="button" class="btn btn-primary save_button action_button" data-type="save">Save</button>
				<button type="button" class="btn btn-primary update_button action_button" disabled="disabled" data-type="update">Update</button>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('input[name="date"]').datepicker();
		$('input[name="date"]').change(function() {
			$('#tbody').html('');
			var counter = 0;
			var date = $(this).val();
			var year_id = '<?php echo $year_id; ?>';
			var school_year = '<?php echo $school_year; ?>';
			var section_id = '<?php echo $section_id; ?>';
			var search = $('input[name="search"]').val();
			var data = {date, year_id, school_year, section_id, search};
			$.ajax({
				type: 'post',
				url: '<?php echo base_url('attendance/check_attendance_date'); ?>',
				data: data,
				dataType: 'json',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					// button save/update disabled
					if(result.student.length == 0) {
						$('button.save_button, button.update_button').attr('disabled', 'disabled');
					} else {
						if(result.count_school_date_id) {
							$('button.update_button').removeAttr('disabled');
							$('button.save_button').attr('disabled', 'disabled');
						} else {
							$('button.save_button').removeAttr('disabled');
							$('button.update_button').attr('disabled', 'disabled');
						}
					}
					for(var x in result.student) {
						++counter;
						if(result.status[result.student[x].id] == 1) {
							var present = 'checked';
							var absent = '';
						} else {
							var present = '';
							var absent = 'checked';
						}
						$('<tr/>', {
							'html': [
								$('<td/>', {'text': counter}),
								$('<td/>', {'text': result.student[x].ref_no}),
								$('<td/>', {'text': result.student[x].fname}),
								$('<td/>', {'text': result.student[x].lname}),
								$('<td/>', {'text': result.student[x].mi}),
								$('<td/>', {'html': '<form>Present <input type="radio" name="status" '+ present +' data-id="'+ result.student[x].id +'" value="<?php echo true ?>"> | Absent <input type="radio" name="status" '+ absent +' data-id="'+ result.student[x].id +'" value="<?php echo false ?>"></form>'}),
							]
						}).appendTo('#tbody');
					}
					$('#total').text(counter);
				}
			});
		});

		$('.action_button').click(function() {
			var button_type = $(this).data('type');
			switch(button_type) {
				case 'save':
					save_attendance();
				break;
				case 'update':
					update_attendance();
				break;
			}
		});

		function save_attendance() {
			$('div.form-group.form-date').removeClass('has-error');
			var status_check = $('input[name="status"]:checked');
			
			$.ajax({
				type: 'post',
				url: '<?php echo base_url('attendance/save'); ?>',
				data: {date: $('input[name="date"]').val()},
				dataType: 'json',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					if(result.error) {
						bootbox.alert({
							title: 'Message',
							message: result.message,
							callback: function() {
								$('div.form-group.form-date').addClass('has-error');
							}
						});
					} else {
						if(result.school_date) {
							status_check.each(function() {
								insert_data(result.school_date, $(this).data('id'), $(this).val());
							});
							bootbox.alert({
								title: 'Message',
								message: 'Attendance Successfully Saved!',
								callback: function() {
									$('button.update_button').removeAttr('disabled');
									$('button.save_button').attr('disabled', 'disabled');
								}
							});
						} else {
							bootbox.alert({
								title: 'Message',
								message: 'Failed to save the attendance'
							});
						}
					}
				}
			});
		}

		function insert_data(school_date, student_id, status) {
			var data = {school_date, student_id, status};
			$.ajax({
				type: 'post',
				data: data,
				url: '<?php echo base_url('attendance/insert_attendance'); ?>',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});
		}

		function update_attendance() {
			var date = $('input[name="date"]').val();
			var status_check = $('input[name="status"]:checked');
			var data = {date};
			$.ajax({
				type: 'POST',
				method: 'POST',
				dataType: 'json',
				data: data,
				url: '<?php echo base_url('attendance/get_school_date_id'); ?>',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					var school_date_id = result.school_date_id;
					if(!result.session) {
						window.location = '<?php echo base_url(); ?>';
						return false;
					}

					status_check.each(function() {
						$.ajax({
							type: 'POST',
							method: 'POST',
							dataType: 'json',
							data: { school_date: school_date_id, student_id: $(this).data('id'), status: $(this).val() },
							url: '<?php echo base_url('attendance/update_attendance'); ?>',
							error: function(xhr, status, error) {
								console.log(xhr.responseText);
							}
						});
					});

					bootbox.alert({
						title: 'Message',
						message: 'Attendance Successfully Updated!'
					});

				}
			});
		}

		$('input[name="search"]').keyup(function() {
			var date = $('input[name="date"]').val();
			var search = $(this).val();
			var year_id = '<?php echo $year_id; ?>';
			var school_year = '<?php echo $school_year; ?>';
			var section_id = '<?php echo $section_id; ?>';
			var data = {date, year_id, school_year, section_id, search};
			var counter = 0;
			$('#tbody').html('');
			$.ajax({
				type: 'POST',
				method: 'POST',
				dataType: 'json',
				url: '<?php echo base_url('attendance/check_attendance_date') ?>',
				data: data,
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					// button save/update disabled
					if(result.student.length == 0) {
						$('button.save_button, button.update_button').attr('disabled', 'disabled');
					} else {
						if(result.count_school_date_id) {
							$('button.update_button').removeAttr('disabled');
							$('button.save_button').attr('disabled', 'disabled');
						} else {
							$('button.save_button').removeAttr('disabled');
							$('button.update_button').attr('disabled', 'disabled');
						}
					}
					for(var x in result.student) {
						++counter;
						if(result.status[result.student[x].id] == 1) {
							var present = 'checked';
							var absent = '';
						} else {
							var present = '';
							var absent = 'checked';
						}
						$('<tr/>', {
							'html': [
								$('<td/>', {'text': counter}),
								$('<td/>', {'text': result.student[x].ref_no}),
								$('<td/>', {'text': result.student[x].fname}),
								$('<td/>', {'text': result.student[x].lname}),
								$('<td/>', {'text': result.student[x].mi}),
								$('<td/>', {'html': '<form>Present <input type="radio" name="status" '+ present +' data-id="'+ result.student[x].id +'" value="<?php echo true ?>"> | Absent <input type="radio" name="status" '+ absent +' data-id="'+ result.student[x].id +'" value="<?php echo false ?>"></form>'}),
							]
						}).appendTo('#tbody');
					}
					$('#total').text(counter);
				}
			});
		});

	});
</script>