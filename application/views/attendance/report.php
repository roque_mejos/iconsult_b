<!DOCTYPE html>
<html>
<head>
	<title>Attendance Report</title>
</head>
<body>
	<table cellpadding="0" cellspacing="0" border="1" width="100%">
		<thead>
			<tr>
				<th>Date</th>
				<th>Day</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($attendance as $row): ?>
				<tr>
					<td><?php echo date('m/d/Y', strtotime($row->date)); ?></td>
					<td><?php echo date('l', strtotime($row->date)); ?></td>
					<td><?php echo ucfirst($row->status); ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</body>
</html>