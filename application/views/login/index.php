<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->file('calendar-event/functions.php') ?>
<div class="container">
	<div class="row" style="margin-top: 100px;">
		<div class="col-lg-4">
			<div id="calendar_div">
				<?php echo getCalender(); ?>
			</div>
		</div>
		
		<div class="col-lg-4 col-lg-offset-4">
			<?php echo $invalid; ?>
			<div class="login-panel panel panel-default">                  
				<div class="panel-heading">
				<h3 class="panel-title">Please Sign In</h3>
				</div>
				<div class="panel-body">
					<?php echo form_open('verify', array('role' => 'form')); ?>
						<fieldset>
							<div class="form-group<?php echo $email_has_error; ?>">
								<input class="form-control" placeholder="E-mail" name="username" type="text" value="<?php echo set_value('username') ?>" autofocus>
								<?php echo form_error('username', '<span class="text-danger">', '</span>'); ?>
							</div>
							<div class="form-group<?php echo $password_has_error; ?>">
								<input class="form-control" placeholder="Password" name="password" type="password" value="">
								<?php echo form_error('password', '<span class="text-danger">', '</span>'); ?>
							</div>
							<button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
							<br>
							<a href="<?php echo base_url('register/index'); ?>" class="hide">Create Account</a>
						</fieldset>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>