<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong>List of Subjects</strong>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="subject-table">
					<thead>
						<tr>
							<th>Subject</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($subject as $row): ?>
							<tr>
								<td><?php echo $row->name; ?></td>
								<td>
									<?php 
									if($row->status) {
										$label = 'Active';
										$class = 'success';
										$status = 'active';
									} else {
										$label = 'Inactive';
										$class = 'danger';
										$status = 'inactive';
									}
									?>
									<button type="button" class="btn btn-<?php echo $class; ?> status-button" data-id="<?php echo $row->id; ?>" data-status="<?php echo $status; ?>" data-status-update="<?php echo $status; ?>"><?php echo $label; ?></button>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript">
	$(function() {
		var subject_table_el = $('table#subject-table'), status_button = $('button.status-button');
		subject_table_el.dataTable();
		status_button.hover(function() {
			var _this = $(this);
			change_button(_this);
		}, function() {
			var _this = $(this);
			change_button(_this);
		});

		$('body').on('click', 'button.status-button', function() {
			var subject_id = $(this).data('id');
			var _this = $(this);
			confirmation(_this, subject_id);
		});

		function change_button(button) {
			var status = button.data('status');
			if(status != 'active') {
				button.removeClass('btn-danger').addClass('btn-success').text('Active').data('status', 'active');
			} else {
				button.addClass('btn-danger').removeClass('btn-success').text('Inactive').data('status', 'inactive');
			}
		}

		function confirmation(button, subject_id) {
			var status = button.data('status-update');
			if(status != 'inactive') {
				bootbox.confirm({
					size: "medium",
					title: "Message",
					message: "Are you sure to deactivate the subject? This can affect to grade computation.",
					buttons: {
						confirm: {
							label: 'Yes'
						},
						cancel: {
							label: 'No'
						}
					},
					callback: function (result) {
						if(result) {
							deactivate_subject(subject_id, button);
						}
					}
				});
			} else {
				bootbox.confirm({
					size: "medium",
					title: "Message",
					message: "Are you sure to activate the subject? This can affect to grade computation",
					buttons: {
						confirm: {
							label: 'Yes'
						},
						cancel: {
							label: 'No'
						}
					},
					callback: function (result) {
						if(result) {
							activate_subject(subject_id, button);
						}
					}
				});
			}
		}

		function deactivate_subject(subject_id, _this) {
			var data = {subject_id};
			$.ajax({
				method: 'post',
				type: 'post',
				url: '<?php echo base_url('subject/deactivate_subject') ?>',
				data: data,
				dataType: 'json',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					var success = result.success;
					if(result.session != true) {
						window.location = '<?php echo base_url(); ?>';
					} else {
						bootbox.alert({
							title: 'Message',
							message: result.message,
							size: 'medium',
							callback: function() {
								if(success) {
									_this.text('Inactive').addClass('btn-danger').removeClass('btn-success').data('status-update', 'inactive').data('status', 'status');
								}
							}
						});
					}
				}
			});
		}

		function activate_subject(subject_id, _this) {
			var data = {subject_id};
			$.ajax({
				method: 'post',
				type: 'post',
				url: '<?php echo base_url('subject/activate_subject') ?>',
				data: data,
				dataType: 'json',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					var success = result.success;
					if(result.session != true) {
						window.location = '<?php echo base_url(); ?>';
					} else {
						bootbox.alert({
							title: 'Message',
							message: result.message,
							size: 'medium',
							callback: function() {
								if(success) {
									_this.text('Active').addClass('btn-success').removeClass('btn-danger').data('status-update', 'active').data('status', 'active');
								}
							}
						});
					}
				}
			});
		}

	});
</script>