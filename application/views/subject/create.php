<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<?php echo $message; ?>
	<div class="panel panel-default">
		<div class="panel-heading"><strong>Create Subject</strong></div>
		<div class="panel-body">
			<?php echo form_open('subject/insert', array('role' => 'form')); ?>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $subject_has_error; ?>">
							<label>Subject</label>
							<input type="text" name="subject" class="form-control" placeholder="Subject" value="<?php echo set_value('subject'); ?>">
							<?php echo $subject_error; ?>
						</div>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>