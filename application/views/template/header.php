<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- load CSS -->
	<link href="<?php echo base_url('assets/plugins/bootstrap/bootstrap.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/plugins/pace/pace-theme-big-counter.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/datepicker.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/main-style.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/plugins/dataTables/dataTables.bootstrap.css'); ?>" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('calendar-event/style.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap-select/dist/css/bootstrap-select.min.css'); ?>">
	<!-- load jquery -->
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-1.10.2.js') ?>"></script>
	<!-- javascript -->
	<?php load_js(); ?>
	<!-- title -->
	<title><?php echo $title; ?></title>
</head>
<body>
	<?php load_nav(); ?>