<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div id="wrapper">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo base_url('dashboard'); ?>">
				<img src="<?php echo base_url('assets/img/iconsult-logo wh5te.png'); ?>" alt="Image" width="228">
			</a>
		</div>
		<ul class="nav navbar-top-links navbar-right">
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">
					<i class="fa fa-user fa-3x"></i>
				</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="<?php echo base_url('profile'); ?>"><i class="fa fa-user fa-fw"></i>User Profile</a></li></li>
					<li><a href="<?php echo base_url('user/change-password'); ?>"><i class="fa fa-user fa-lock"></i> Change Password</a></li></li>
					<li class="divider"></li>
					<li><a href="<?php echo base_url('login/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
				</ul>
			</li>
		</ul>
	</nav>
	<?php $this->load->view('template/sidebar'); ?>