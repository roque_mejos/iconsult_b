<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="sidebar-collapse">
			<ul class="nav" id="side-menu">
				<li>
					<div class="user-section">
						<!-- user image -->
						<div class="user-section-inner">
							<img src="<?php echo profile_image(); ?>" alt="">
						</div>
						<!-- user info -->
						<div class="user-info">
							<div><?php echo $this->session->fname; ?> <strong><?php echo $this->session->lname; ?></strong></div>
							<!-- usertype -->
							<div class="user-text-online"><font style="font-size:16px"><?php echo $this->session->usertype_name; ?></font></div>
						</div>
					</div>
				</li>
				<?php $this->load->view($side_nav); ?>
			</ul>
		</div>
	</nav>