<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<?php echo $success; ?>
	<?php echo $fail; ?>
	<div class="panel panel-default">
		<div class="panel-heading"><strong style="color: #03ab15;">Update Event</strong></div>
			<div class="panel-body">
				<?php echo form_open('event/update', array('role' => 'form')); ?>
					<input type="hidden" name="event_id" value="<?php echo $event->id ?>">
					<div class="row">
						<div class="col-lg-5">
							<div class="form-group<?php echo $title_has_error; ?>">
								<label>Title</label>
								<input type="text" name="title" class="form-control" placeholder="Title" value="<?php echo $title_set_value; ?>">
								<?php echo $title_error; ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5">
							<div class="form-group<?php echo $date_has_error; ?>">
								<label>Event Date</label>
								<input type="text" name="date" class="form-control" placeholder="Event Date" value="<?php echo $date_set_value; ?>">
								<?php echo $date_error; ?>
							</div>
							<button type="submit" class="btn btn-primary" name="update">Update</button>
						</div>
					</div>
				<?php echo form_close(); ?>
			</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('input[name="date"]').datepicker();
	});
</script>