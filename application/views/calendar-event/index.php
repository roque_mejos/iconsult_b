<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->file('calendar-event/functions.php') ?>
<div class="col-lg-10 col-lg-offset-1">
	<div id="calendar_div">
		<?php echo getCalender(); ?>
	</div>
</div>