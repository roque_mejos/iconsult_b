<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			List of Events
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="event-table">
					<thead>
						<tr>
							<th>Title</th>
							<th>Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($event as $row) { ?>
							<tr>
								<td><?php echo $row->title; ?></td>
								<td><?php echo date('F d, Y', strtotime($row->date)); ?></td>
								<td><a href="<?php echo base_url('event/detail/' . $row->id); ?>">Edit</a> | <a href="javascript:void(0);" class="event_id" data-id="<?php echo $row->id; ?>">Delete</a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript">
	$(function() {
		var event_table_el = $('table#event-table'), event_id_el = $('a.event_id'), event_id;
		event_table_el.dataTable();

		function delete_event(event_id) {
			$.ajax({
				type: 'post',
				method: 'post',
				url: '<?php echo base_url('event/delete'); ?>',
				data: {event_id: event_id},
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			}).done(function(response) {
				var data = $.parseJSON(response);
				bootbox.alert({
						size: 'medium',
						title: 'Message',
						message: data.message,
						callback: function() {
							window.location = '<?php echo base_url('event/action'); ?>';
						}
				});
			});
		}

		$.fn.delete_event = function() {
			return this.click(function() {
				var _this = $(this);
				bootbox.confirm({
					title: "Message",
					message: "Are you sure to delete this event?",
					size: 'medium',
					buttons: {
						cancel: {
							label: 'No'
						},
						confirm: {
							label: 'Yes'
						}
					},
					callback: function(result) {
						if(result) {
							event_id = _this.data('id');
							delete_event(event_id);
						}
					}
				});
			});
		}
		event_id_el.delete_event();
	});
</script>