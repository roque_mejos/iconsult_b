<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Report Card</title>
</head>
<body>
	<p><strong>Periodic Table</strong></p>
	<table border="1" cellspacing="0" cellspacing="0" width="60%" style="margin-bottom: 30px;">
		<thead>
			<tr>
				<th>LEARNING AREAS</th>
				<th>1<sup>st</sup></th>
				<th>2<sup>nd</sup></th>
				<th>3<sup>rd</sup></th>
				<th>4<sup>th</sup></th>
				<th>Final Ratings</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($subject as $row): ?>
				<tr>
					<td><?php echo $row; ?></td>
					<td><?php echo $first_grading_total[$row]; ?></td>
					<td><?php echo $second_grading_total[$row]; ?></td>
					<td><?php echo $third_grading_total[$row]; ?></td>
					<td><?php echo $fourth_grading_total[$row]; ?></td>
					<td><?php echo $first_ave[$row]; ?></td>
				</tr>
			<?php endforeach; ?>
			<tr>
				<td>GENERAL AVERAGE</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td><?php echo number_format($tot_ave / count($subject), 2); ?></td>
			</tr>
		</tbody>
	</table>

	<p><strong>LEGEND</strong></p>

	<table border="0" style="margin-bottom: 30px;">
		<tbody>
			<tr>
				<td>&nbsp;&nbsp;Advanced (A)&nbsp;&nbsp;</td><td>90% and above</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;Proficient (P)&nbsp;&nbsp;</td><td>85% - 89%</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;Approaching Proficiency (AP)&nbsp;&nbsp;</td><td>80% - 84%</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;Developing (D)&nbsp;&nbsp;</td><td>75% - 79%</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;Beginning (B)&nbsp;&nbsp;</td><td>74% and below</td>
			</tr>
		</tbody>
	</table>

	<table border="1" cellspacing="0" cellspacing="0" width="60%">
		<thead>
			<tr>
				<td></td>
				<td>Jun</td>
				<td>Jul</td>
				<td>Aug</td>
				<td>Sep</td>
				<td>Oct</td>
				<td>Nov</td>
				<td>Dec</td>
				<td>Jan</td>
				<td>Feb</td>
				<td>Mar</td>
				<td>Apr</td>
				<td>May</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>No. of School Days</td>
				<td><?php echo $num_of_days[5]; ?></td>
				<td><?php echo $num_of_days[6]; ?></td>
				<td><?php echo $num_of_days[7]; ?></td>
				<td><?php echo $num_of_days[8]; ?></td>
				<td><?php echo $num_of_days[9]; ?></td>
				<td><?php echo $num_of_days[10]; ?></td>
				<td><?php echo $num_of_days[11]; ?></td>
				<td><?php echo $num_of_days[0]; ?></td>
				<td><?php echo $num_of_days[1]; ?></td>
				<td><?php echo $num_of_days[2]; ?></td>
				<td><?php echo $num_of_days[3]; ?></td>
				<td><?php echo $num_of_days[4]; ?></td>
			</tr>
			<tr>
				<td>No. of School Days Present</td>
				<td><?php echo $present_days[5]; ?></td>
				<td><?php echo $present_days[6]; ?></td>
				<td><?php echo $present_days[7]; ?></td>
				<td><?php echo $present_days[8]; ?></td>
				<td><?php echo $present_days[9]; ?></td>
				<td><?php echo $present_days[10]; ?></td>
				<td><?php echo $present_days[11]; ?></td>
				<td><?php echo $present_days[0]; ?></td>
				<td><?php echo $present_days[1]; ?></td>
				<td><?php echo $present_days[2]; ?></td>
				<td><?php echo $present_days[3]; ?></td>
				<td><?php echo $present_days[4]; ?></td>
			</tr>
			<tr>
				<td>No. of Tardy</td>
				<td><?php echo $absent_days[5]; ?></td>
				<td><?php echo $absent_days[6]; ?></td>
				<td><?php echo $absent_days[7]; ?></td>
				<td><?php echo $absent_days[8]; ?></td>
				<td><?php echo $absent_days[9]; ?></td>
				<td><?php echo $absent_days[10]; ?></td>
				<td><?php echo $absent_days[11]; ?></td>
				<td><?php echo $absent_days[0]; ?></td>
				<td><?php echo $absent_days[1]; ?></td>
				<td><?php echo $absent_days[2]; ?></td>
				<td><?php echo $absent_days[3]; ?></td>
				<td><?php echo $absent_days[4]; ?></td>
			</tr>
		</tbody>
	</table>
</body>
</html>