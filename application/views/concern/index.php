<style type="text/css">
	.chat
	{
	    list-style: none;
	    margin: 0;
	    padding: 0;
	}

	.chat li
	{
	    margin-bottom: 10px;
	    padding-bottom: 5px;
	    border-bottom: 1px dotted #B3A9A9;
	}

	.chat li.left .chat-body
	{
	    margin-left: 60px;
	}

	.chat li.right .chat-body
	{
	    margin-right: 60px;
	}


	.chat li .chat-body p
	{
	    margin: 0;
	    color: #777777;
	}

	.panel .slidedown .glyphicon, .chat .glyphicon
	{
	    margin-right: 5px;
	}

	.panel-body
	{
	    overflow-y: scroll;
	    height: 250px;
	}

	::-webkit-scrollbar-track
	{
	    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	    background-color: #F5F5F5;
	}

	::-webkit-scrollbar
	{
	    width: 12px;
	    background-color: #F5F5F5;
	}

	::-webkit-scrollbar-thumb
	{
	    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	    background-color: #555;
	}
</style>
<div class="col-lg-12">
	<div class="container">
	    <div class="row">
	    	<div class="col-md-3">
			  <h3>Teacher's List</h3>
			  <ul class="list-group">
			  	<?php foreach($teacher as $row): ?>
			    	<li class="list-group-item"><a href="javascript:void(0);" data-id="<?php echo $row->id; ?>" class="fullname"><?php echo $row->fullname; ?></a></li>
			    <?php endforeach; ?>
			  </ul>
	    	</div>
	        <div class="col-md-5">
	            <div class="panel panel-primary">
	                <div class="panel-heading">
	                    <span class="glyphicon glyphicon-comment"></span> Chat
	                </div>
	                <div class="panel-body"><ul class="chat"></ul></div>
	                <div class="panel-footer">
	                	<?php echo form_open('', array('id' => 'form', 'autocomplete' => 'off')); ?>
	                		<input type="hidden" name="receiver" value="">
		                    <div class="input-group">
		                        <input id="btn-input" type="text" class="form-control input-sm text-form-message" name="message" placeholder="Type your message here..." disabled="disabled" />
		                        <span class="input-group-btn">
		                            <button class="btn btn-warning btn-sm" id="btn-chat" type="submit">
		                                Send</button>
		                        </span>
		                    </div>
		                  <?php echo form_close(); ?>
	                </div>

	            </div>
	        </div>
	    </div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/scripts/scroll_top.js'); ?>"></script>
<script type="text/javascript">
	$(function() {

		$('.fullname').click(function(e) {
			e.preventDefault();
			$(".panel-body").animate({ scrollTop: SCROLL_TOP }, "slow");
			var user_id = $(this).data('id');
			get_message(user_id);
			$('input.text-form-message').removeAttr('disabled');
		});

		function get_message(user_id) {
			$('ul.chat').html('');
			$.ajax({
				type: 'post',
				method: 'post',
				url: '<?php echo base_url('concern/get_message'); ?>',
				dataType: 'json',
				data: { user_id },
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					if(!result.session) {
						window.location = '<?php echo base_url(); ?>';
					} else {
						$('input[name="receiver"]').val(result.user_id);
						_get(result);
					}
				}
			});
		}

		$('#form').submit(function(e) {
			e.preventDefault();
			$(".panel-body").animate({ scrollTop: $('ul.chat').height() }, "slow");
			$.ajax({
				type: 'post',
				method: 'post',
				url: '<?php echo base_url('concern/save'); ?>',
				data: $(this).serialize(),
				dataType: 'json',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					if(!result.session) {
						window.location = '<?php echo base_url(); ?>';
					} else {
						if(result.error) {
							bootbox.alert({
								title: 'Message',
								message: result.message
							});
						} else {
							$('ul.chat').html('');
							$('input[name="message"]').val('');
							_get(result);
						}
					}
				}
			});
		});

		function message_num() {
			return $('ul.chat li').length;
		}

		setInterval(function() { 
			$.ajax({
				type: 'post',
				method: 'post',
				data: { receiver: $('input[name="receiver"]').val() },
				url: '<?php echo base_url('concern/check_message'); ?>',
				dataType: 'json',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				success: function(result) {
					if(!result.session) {
						window.location = '<?php echo base_url(); ?>';
					} else {
						if(result.count != message_num()) {
							$(".panel-body").animate({ scrollTop: SCROLL_TOP }, "slow");
							$('ul.chat').html('');
							_get(result);
						} else {
							return false;
						}
					}
				}
			});
		}, 1000);

		function _get(result) {
			for(var x in result.chat) {
				var list_class = 'right', span_float = 'pull-right', avatar = 'me-chat.png', chat_name = result.chat[x].receiver_name, time_class = 'pull-right';
				if(result.chat[x].sender != result.current_user) {
					list_class = 'left';
					span_float = 'pull-left';
					avatar = 'u-chat.png';
					chat_name = result.chat[x].receiver_name;
					time_class = '';
				}

				$('<li/>', {
					'class': list_class + ' clearfix',
					'html': [
						$('<span/>', {'class': 'chat-img ' + span_float, 'html': '<img src="<?php echo base_url('assets/img/'); ?>'+ avatar +'" alt="User Avatar" class="img-circle" />'}),
						$('<div/>', {'class': 'chat-body clearfix', 'html': [
							$('<div/>', {'class': 'header', 'html': [
								$('<strong/>', {'text': chat_name}),
								$('<small/>', {'class': time_class + ' text-muted', 'html': ' <span class="glyphicon glyphicon-time"></span>' + result.time_history[x]})
							]}),
							$('<p/>', {'text': result.chat[x].message})
						]})

					]
				}).appendTo('ul.chat');
			}
		}
	});
</script>