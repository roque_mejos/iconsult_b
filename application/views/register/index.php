<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Create account</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/bootstrap/bootstrap.css') ?>">
	<link href="<?php echo base_url('assets/css/datepicker.css'); ?>" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-1.10.2.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/scripts/bootstrap-datepicker.js'); ?>"></script>
	<style type="text/css">
		body {
		    padding-top: 30px;
		}
		.panel-login {
			border-color: #ccc;
			-webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
			-moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
			box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
		}
		.panel-login>.panel-heading {
			color: #00415d;
			background-color: #fff;
			border-color: #fff;
			text-align:center;
		}
		.panel-login>.panel-heading a{
			text-decoration: none;
			color: #666;
			font-weight: bold;
			font-size: 15px;
			-webkit-transition: all 0.1s linear;
			-moz-transition: all 0.1s linear;
			transition: all 0.1s linear;
		}
		.panel-login>.panel-heading a.active{
			color: #029f5b;
			font-size: 18px;
		}
		.panel-login>.panel-heading hr{
			margin-top: 10px;
			margin-bottom: 0px;
			clear: both;
			border: 0;
			height: 1px;
			background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
			background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
			background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
			background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
		}
		.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
			height: 45px;
			border: 1px solid #ddd;
			font-size: 16px;
			-webkit-transition: all 0.1s linear;
			-moz-transition: all 0.1s linear;
			transition: all 0.1s linear;
		}
		.panel-login input:hover,
		.panel-login input:focus {
			outline:none;
			-webkit-box-shadow: none;
			-moz-box-shadow: none;
			box-shadow: none;
			border-color: #ccc;
		}
		.btn-login {
			background-color: #59B2E0;
			outline: none;
			color: #fff;
			font-size: 14px;
			height: auto;
			font-weight: normal;
			padding: 14px 0;
			text-transform: uppercase;
			border-color: #59B2E6;
		}
		.btn-login:hover,
		.btn-login:focus {
			color: #fff;
			background-color: #53A3CD;
			border-color: #53A3CD;
		}
		.forgot-password {
			text-decoration: underline;
			color: #888;
		}
		.forgot-password:hover,
		.forgot-password:focus {
			text-decoration: underline;
			color: #666;
		}

		.btn-register {
			background-color: #1CB94E;
			outline: none;
			color: #fff;
			font-size: 14px;
			height: auto;
			font-weight: normal;
			padding: 14px 0;
			text-transform: uppercase;
			border-color: #1CB94A;
		}
		.btn-register:hover,
		.btn-register:focus {
			color: #fff;
			background-color: #1CA347;
			border-color: #1CA347;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<?php echo $message; ?>
			</div>
		</div>
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6 col-xs-offset-3">
								<a href="#" id="register-form-link active">Registration form</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="register-form" action="<?php echo base_url('register/insert'); ?>" method="post" role="form">
									<div class="form-group">
										<select class="form-control" name="usertype">
											<option value="">Usertype</option>
											<?php foreach($usertype as $row): ?>
												<?php if($row->id == set_value('usertype')): 
														$selected = " selected";
													else:
														$selected = '';
													endif;
												?>
												<option value="<?php echo $row->id; ?>"<?php echo $selected; ?>><?php echo $row->name ?></option>
											<?php endforeach; ?>
										</select>
										<?php echo $usertype_error ?>
									</div>
									<div class="form-group">
										<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Email Address" value="<?php echo set_value('username'); ?>">
										<?php echo $username_error; ?>
									</div>
									<div class="form-group">
										<input type="text" name="fname" id="fname" tabindex="1" class="form-control" placeholder="Firstname" value="<?php echo set_value('fname'); ?>">
										<?php echo $fname_error; ?>
									</div>
									<div class="form-group">
										<input type="text" name="lname" id="lname" tabindex="1" class="form-control" placeholder="Lastname" value="<?php echo set_value('lname'); ?>">
										<?php echo $lname_error; ?>
									</div>
									<div class="form-group">
										<input type="text" name="mi" id="mi" tabindex="1" class="form-control" placeholder="M.I." value="<?php echo set_value('mi'); ?>">
										<?php echo $mi_error; ?>
									</div>
									<div class="form-group">
										<input type="text" name="bdate" id="bdate" tabindex="1" class="form-control" placeholder="Birth Date" value="<?php echo set_value('bdate'); ?>">
										<?php echo $bdate_error; ?>
									</div>
									<div class="form-group">
										<select name="gender" id="gender" tabindex="1" class="form-control">
											<option value="">Gender</option>
											<?php foreach($gender as $row): 
												if(set_value('gender') == $row->id) {
													$selected = 'selected';
												} else {
													$selected = '';
												}
											?>
												<option value="<?php echo $row->id; ?>" <?php echo $selected; ?>><?php echo $row->name; ?></option>
											<?php endforeach; ?>
										</select>
										<?php echo $gender_error; ?>
									</div>
									<div class="form-group">
										<textarea name="address" id="address" tabindex="1" class="form-control" placeholder="Address"><?php echo set_value('address'); ?></textarea>
										<?php echo $address_error; ?>
									</div>
									<div class="form-group">
										<input type="text" name="mobile" id="mobile" tabindex="1" class="form-control" placeholder="Mobile Number" value="<?php echo set_value('mobile'); ?>">
										<?php echo $mobile_error; ?>
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
										<?php echo $password_error; ?>
									</div>
									<div class="form-group">
										<input type="password" name="confirm_password" id="confirm_password" tabindex="2" class="form-control" placeholder="Confirm Password">
										<?php echo $confirm_password_error; ?>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
											</div>
										</div>
										<a href="<?php echo base_url(); ?>">Back to Login</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$('input[name="bdate"]').datepicker();
		});
	</script>
</body>
</html>