<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<?php echo $message; ?>
	<div class="panel panel-default">
		<div class="panel-heading"><strong>Create</strong></div>
		<div class="panel-body">
			<?php echo form_open('announcement/update', array('role' => 'form')); ?>
				<input type="hidden" name="announcement_id" value="<?php echo $announcement_id; ?>">
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $title_has_error; ?>">
							<label>Title</label>
							<input type="text" name="title" class="form-control" placeholder="Title" value="<?php echo $title_set_value; ?>">
							<?php echo $title_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $message_has_error; ?>">
							<label>Message</label>
							<textarea row class="form-control" name="message" placeholder="Message" style="width: 502px; height: 101px;"><?php echo $message_set_value; ?></textarea>
							<?php echo $message_error; ?>
						</div>
						<button type="submit" class="btn btn-primary">Update</button>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>