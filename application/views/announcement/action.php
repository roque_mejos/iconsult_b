<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<strong>List of Annoucements</strong>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="annoucements-table">
					<thead>
						<tr>
							<th>Title</th>
							<th>Announcement</th>
							<th>Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($announcement as $row) { ?>
							<tr>
								<td><?php echo $row->title; ?></td>
								<td><?php echo $row->message; ?></td>
								<td><?php echo date('F d, Y h:i:s A', strtotime($row->created)); ?></td>
								<td><a href="<?php echo base_url('announcement/detail/' . $row->id); ?>">Edit</a> | <a href="javascript:void(0);" class="announcement_id" data-id="<?php echo $row->id; ?>">Delete</a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript">
	$(function() {
		var announcement_table_el = $('table#annoucements-table'), announcement_id_el = $('a.announcement_id'), announcement_id;
		announcement_table_el.dataTable();
		function delete_announcement(announcement_id) {
			$.ajax({
				type: 'post',
				method: 'post',
				url: '<?php echo base_url('announcement/delete'); ?>',
				data: {announcement_id: announcement_id},
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			}).done(function(response) {
				var data = $.parseJSON(response);
				bootbox.alert({
						size: 'medium',
						title: 'Message',
						message: data.message,
						callback: function() {
							window.location = '<?php echo base_url('announcement/action'); ?>';
						}
				});
			});
		}
		$.fn.delete_announcement = function() {
			return this.click(function() {
				var _this = $(this);
				bootbox.confirm({
					title: "Message",
					message: "Are you sure to delete this announcement?",
					size: 'medium',
					buttons: {
						cancel: {
							label: 'No'
						},
						confirm: {
							label: 'Yes'
						}
					},
					callback: function(result) {
						if(result) {
							announcement_id = _this.data('id');
							delete_announcement(announcement_id);
						}
					}
				});
			});
		}
		announcement_id_el.delete_announcement();
	});
</script>