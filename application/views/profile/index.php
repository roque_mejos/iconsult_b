<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<?php echo $message; ?>
	<div class="panel panel-default">
		<div class="panel-heading"><strong style="color: #03ab15;">Profile</strong></div>
		<div class="panel-body">
			<?php echo form_open_multipart('profile/update', array('role' => 'form')); ?>
				<input type="hidden" name="user_id" value="<?php echo $this->session->id; ?>">
				<div class="col-lg-8">
					<div class="row">
						<div class="col-lg-5">
							<div class="form-group<?php echo $fname_has_error; ?>">
								<label>Firstname</label>
								<input type="text" name="fname" class="form-control" placeholder="Firstname" value="<?php echo $fname_set_value; ?>">
								<?php echo $fname_error; ?>
							</div>
						</div>
						<div class="col-lg-5">
							<div class="form-group<?php echo $lname_has_error; ?>">
								<label>Lastname</label>
								<input type="text" name="lname" class="form-control" placeholder="Lastname" value="<?php echo $lname_set_value; ?>">
								<?php echo $lname_error; ?>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group<?php echo $mi_has_error; ?>">
								<label>M.I.</label>
								<input type="text" name="mi" class="form-control" placeholder="M.I." value="<?php echo $mi_set_value; ?>" maxlength="1" required="required">
								<?php echo $mi_error; ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group<?php echo $bdate_has_error; ?>">
								<label>Birthdate</label>
								<input type="text" name="bdate" class="form-control" placeholder="Birthdate" value="<?php echo $bdate_set_value; ?>">
								<?php echo $bdate_error; ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group<?php echo $gender_has_error; ?>">
								<label>Gender</label>
								<select name="gender" class="form-control">
									<option value="">Gender</option>
									<?php foreach($gender as $row) { ?>
										<option value="<?php echo $row->id; ?>"<?php echo $gender_selected[$row->id]; ?>><?php echo $row->name; ?></option>
									<?php } ?>
								</select>
								<?php echo $gender_error; ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group<?php echo $mobile_has_error; ?>">
								<label>Mobile Number</label>
								<input type="text" name="mobile" class="form-control" placeholder="Mobile Number" value="<?php echo $mobile_set_value; ?>">
								<?php echo $mobile_error; ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group<?php echo $address_has_error; ?>">
								<label>Address</label>
								<textarea name="address" class="form-control" placeholder="Address"><?php echo $address_set_value; ?></textarea>
								<?php echo $address_error; ?>
							</div>
							<button type="submit" class="btn btn-primary" value="update">Update</button>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4" align="center">
					<div class="form-group">
						<img src="<?php echo profile_image() ?>" alt="Profile image" style="width: 200px; height: 200px;" class="img-thumbnail" id="profile-image">
					</div>
					<button type="button" class="btn btn-success" id="choose-image">Choose image</button>
					<input type="file" name="userfile" class="hide" value="">
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		var choose_image_el = $('button#choose-image'), userfile = $('input[name="userfile"]'), bdate = $('input[name="bdate"]');
		bdate.datepicker();
		function readURL(input) {
			file = $('input[name="imagefile"]');
			fileTypes = ['jpg', 'jpeg', 'png', 'bmp'];

			if (input.files && input.files[0]) {

				var extension = input.files[0].name.split('.').pop().toLowerCase(),
				isSuccess = fileTypes.indexOf(extension) > -1;

				if (isSuccess) { 
					var reader = new FileReader();

					reader.onload = function (e) {
						$('img#profile-image').attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				} else {
					bootbox.alert({ 
						size: "small",
						title: "Message",
						message: 'Invalid File Type'
					});
					file.replaceWith(file = file.clone(true));
					return false;
				}
			}
		}
		$.fn.choose_image = function() {
			return this.click(function() {
				userfile.trigger('click');
			});
		}
		$.fn.imageUpload = function() {
			return this.change(function() {
				readURL(this);
			});
		}
		choose_image_el.choose_image();
		userfile.imageUpload();
	});
</script>