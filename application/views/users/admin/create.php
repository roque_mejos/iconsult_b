<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<?php echo $success; ?>
	<?php echo $fail; ?>
	<div class="panel panel-default">
		<div class="panel-heading"><strong style="color: #03ab15;">Create User</strong></div>
		<div class="panel-body">
			<?php echo form_open('user/insert', array('role' => 'form')); ?>
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group<?php echo $fname_has_error; ?>">
							<label>Firstname</label>
							<input type="text" name="fname" class="form-control" placeholder="Firstname" value="<?php echo set_value('fname'); ?>">
							<?php echo $fname_error; ?>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group<?php echo $lname_has_error; ?>">
							<label>Lastname</label>
							<input type="text" name="lname" class="form-control" placeholder="Lastname" value="<?php echo set_value('lname'); ?>">
							<?php echo $lname_error; ?>
						</div>
					</div>
					<div class="col-lg-1">
						<div class="form-group<?php echo $mi_has_error; ?>">
							<label>M.I.</label>
							<input type="text" name="mi" class="form-control" placeholder="M.I." value="<?php echo set_value('mi'); ?>" maxlength="1" required="required">
							<?php echo $mi_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $username_has_error; ?>">
							<label>Username</label>
							<input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo set_value('username'); ?>">
							<?php echo $username_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $usertype_has_error; ?>">
							<label>Usertype</label>
							<select type="text" name="usertype" class="form-control">
								<option value="">Usertype</option>
								<?php foreach($usertype as $row) { ?>
									<option value="<?php echo $row->id; ?>"<?php echo $usertype_selected[$row->id]; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $usertype_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $bdate_has_error; ?>">
							<label>Birth date</label>
							<div class="input-append date" id="dp3" data-date="12-02-2012" data-date-format="mm-dd-yyyy">
								<input type="text" name="bdate" class="form-control" placeholder="Birth date" value="<?php echo set_value('bdate'); ?>">
								<?php echo $bdate_error; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $gender_has_error; ?>">
							<label>Gender</label>
							<select name="gender" class="form-control">
								<option value="">Gender</option>
								<?php foreach($gender as $row) { ?>
									<option value="<?php echo $row->id; ?>"<?php echo $gender_selected[$row->id]; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $gender_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $mobile_has_error; ?>">
							<label>Mobile Number</label>
							<input type="text" name="mobile" class="form-control" placeholder="Mobile" value="<?php echo set_value('mobile'); ?>">
							<?php echo $mobile_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $address_has_error; ?>">
							<label>Address</label>
							<textarea name="address" class="form-control" placeholder="Address"><?php echo set_value('address'); ?></textarea>
							<?php echo $address_error; ?>
						</div>
						<button type="submit" class="btn btn-primary" name="save">Save</button>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('input[name="bdate"]').datepicker();
	});
</script>