<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<li<?php echo $selected['dashboard']; ?>><a href="<?php echo base_url('dashboard'); ?>" style="color:white;"><img src="<?php echo base_url('assets/img/dashboard.png'); ?>" width="30"> &nbsp; &nbsp; Dashboard</a>
</li>

<li<?php echo $selected['manage']; ?>><a href="javascript:void(0);" style="color:white;"><img src="<?php echo base_url('assets/img/grades-01.png'); ?>" width="30"> &nbsp; &nbsp; Manage Users<span class="fa arrow"></span></a>
	<ul class="nav nav-second-level collapse">
		<li>
			<a href="<?php echo base_url('user/create'); ?>" style="color:white;">Add User</a>
		</li>
	</ul>
</li>
