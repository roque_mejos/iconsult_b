<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<?php echo $success; ?>
	<?php echo $fail; ?>
	<div class="panel panel-default">
		<div class="panel-heading"><strong style="color: #03ab15;"><?php echo $user->usertype_name; ?></strong></div>
		<div class="panel-body">
			<?php echo form_open('user/update', array('role' => 'form')); ?>
				<input type="hidden" name="user_id" value="<?php echo $user->id; ?>">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group<?php echo $fname_has_error; ?>">
							<label>Firstname</label>
							<input type="text" name="fname" class="form-control" placeholder="Firstname" value="<?php echo $fname_set_value; ?>">
							<?php echo $fname_error; ?>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group<?php echo $lname_has_error; ?>">
							<label>Lastname</label>
							<input type="text" name="lname" class="form-control" placeholder="Lastname" value="<?php echo $lname_set_value; ?>">
							<?php echo $lname_error; ?>
						</div>
					</div>
					<div class="col-lg-1">
						<div class="form-group<?php echo $mi_has_error; ?>">
							<label>M.I.</label>
							<input type="text" name="mi" class="form-control" placeholder="M.I." value="<?php echo $mi_set_value; ?>" maxlength="1" required="required">
							<?php echo $mi_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $username_has_error; ?>">
							<label>Username</label>
							<input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo $username_set_value; ?>">
							<?php echo $username_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $bdate_has_error; ?>">
							<label>Birth date</label>
							<div class="input-append date" id="dp3" data-date="12-02-2012" data-date-format="mm-dd-yyyy">
								<input type="text" name="bdate" class="form-control" placeholder="Birth date" value="<?php echo $bdate_set_value; ?>">
								<?php echo $bdate_error; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $gender_has_error; ?>">
							<label>Gender</label>
							<select name="gender" class="form-control">
								<option value="">Gender</option>
								<?php foreach($gender as $row) { ?>
									<option value="<?php echo $row->id; ?>"<?php echo $gender_selected[$row->id]; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $gender_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $mobile_has_error; ?>">
							<label>Mobile Number</label>
							<input type="text" name="mobile" class="form-control" placeholder="Mobile" value="<?php echo $mobile_set_value; ?>">
							<?php echo $mobile_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group<?php echo $address_has_error; ?>">
							<label>Address</label>
							<textarea name="address" class="form-control" placeholder="Address"><?php echo $address_set_value; ?></textarea>
							<?php echo $address_error; ?>
						</div>
						<button type="submit" class="btn btn-primary" name="update">Update</button>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('input[name="bdate"]').datepicker();
	});
</script>