<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			Total Count of User(s): <strong><?php echo count($users); ?></strong>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="user-table">
					<thead>
						<tr>
							<th>Firstname</th>
							<th>Lastname</th>
							<th>M.I.</th>
							<th>Gender</th>
							<th>Age</th>
							<th>Mobile Number</th>
							<th>Username</th>
							<th>Usertype</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($users as $row) { ?>
							<tr>
								<td><?php echo $row->fname; ?></td>
								<td><?php echo $row->lname; ?></td>
								<td><?php echo $row->mi; ?></td>
								<td><?php echo $row->gender_name; ?></td>
								<td><?php echo age(date('m/d/Y', strtotime($row->bdate))); ?></td>
								<td><?php echo $row->mobile; ?></td>
								<td><?php echo $row->username; ?></td>
								<td><?php echo $row->usertype_name; ?></td>
								<td><a href="<?php echo base_url('user/detail/' . $row->id); ?>">Edit</a> | <a href="javascript:void(0)" data-id="<?php echo $row->id ?>" class="user_id">Delete</a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript">
	$(function() {
		var user_table = $('#user-table'), user_id_el = $('a.user_id');
		var user_id;
		user_table.dataTable();

		function delete_user(user_id) {
			$.ajax({
				type: 'post',
				method: 'post',
				url: '<?php echo base_url('user/delete_user'); ?>',
				data: {user_id: user_id},
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			}).done(function(response) {
				var data = $.parseJSON(response);
				bootbox.alert({
						size: 'medium',
						title: 'Message',
						message: data.message,
						callback: function() {
							window.location = '<?php echo base_url(); ?>';
						}
				});
			});
		}

		$.fn.delete_user = function() {
			return this.click(function() {
				var _this = $(this);
				bootbox.confirm({
					title: "Message",
					message: "Are you sure to delete this user?",
					size: 'medium',
					buttons: {
						cancel: {
							label: 'No'
						},
						confirm: {
							label: 'Yes'
						}
					},
					callback: function(result) {
						if(result) {
							user_id = _this.data('id');
							delete_user(user_id);
						}
					}
				});
			});
		}
		user_id_el.delete_user();
	});
</script>