<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<?php echo $success; ?>
	<?php echo $fail; ?>
	<div class="panel panel-default">
		<div class="panel-heading"><strong style="color: #03ab15;">Change Password</strong></div>
		<div class="panel-body">
			<?php echo form_open('user/change-password/update', array('role' => 'form')); ?>
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group<?php echo $current_password_has_error; ?>">
							<label>Current Password</label>
							<input type="password" name="current_password" class="form-control" placeholder="Current Password" value="">
							<?php echo $current_password_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group<?php echo $password_has_error ?>">
							<label>New Password</label>
							<input type="password" name="password" class="form-control" placeholder="New Password" value="">
							<?php echo $password_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<label>Confirm Password</label>
							<input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password" value="">
							<?php echo $confirm_password_error; ?>
						</div>
						<button type="submit" class="btn btn-primary" name="change_password">Change</button>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>