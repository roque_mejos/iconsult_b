<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<li<?php echo $selected['dashboard']; ?>><a href="<?php echo base_url('dashboard'); ?>" style="color:white;"><img src="<?php echo base_url('assets/img/dashboard.png'); ?>" width="30"> &nbsp; &nbsp; Dashboard</a>
</li>
<li<?php echo $selected['announcement']; ?>><a href="javascript:void(0);" style="color:white;"><img src="<?php echo base_url('assets/img/announcement-512.png'); ?>" width="30"> &nbsp; &nbsp; Announcement<span class="fa arrow"></span></a>
	<ul class="nav nav-second-level collapse">
		<li>
			<a href="<?php echo base_url('announcement/create'); ?>" style="color:white;">Create</a>
			<a href="<?php echo base_url('announcement/action'); ?>" style="color:white;">Action</a>
		</li>
	</ul>
</li>

<li<?php echo $selected['calendar_event']; ?>><a href="javascript:void(0);" style="color:white;"><img src="<?php echo base_url('assets/img/1286-weekly-calendar-day-vector.png'); ?>" width="30"> &nbsp; &nbsp; School Activities<span class="fa arrow"></span></a>
	<ul class="nav nav-second-level collapse">
		<li>
			<a href="<?php echo base_url('event'); ?>" style="color:white;">Events</a>
			<a href="<?php echo base_url('event/create'); ?>" style="color:white;">Create</a>
			<a href="<?php echo base_url('event/action'); ?>" style="color:white;">Action</a>
		</li>
	</ul>
</li>

<li<?php echo $selected['concern']; ?>><a href="<?php echo base_url('concern'); ?>" style="color:white;"><img src="<?php echo base_url('assets/img/grades-01.png'); ?>" width="30"> &nbsp; &nbsp; Concern</a>
</li>

<li<?php echo $selected['grade']; ?>><a href="<?php echo base_url('grade'); ?>" style="color:white;"><img src="<?php echo base_url('assets/img/grades-01.png'); ?>" width="30"> &nbsp; &nbsp; Grade</a>
</li>

<li<?php echo $selected['attendance']; ?>><a href="<?php echo base_url('attendance'); ?>" style="color:white;"><img src="<?php echo base_url('assets/img/attendance.png'); ?>" width="30"> &nbsp; &nbsp; Attendance</a>
</li>

<li<?php echo $selected['report']; ?>><a href="<?php echo base_url('student/report'); ?>" style="color:white;"><img src="<?php echo base_url('assets/img/report.png'); ?>" width="30"> &nbsp; &nbsp; Report</a>
</li>

<li<?php echo $selected['subject']; ?>><a href="javascript:void(0);" style="color:white;"><img src="<?php echo base_url('assets/img/subject.png'); ?>" width="30"> &nbsp; &nbsp; Subject<span class="fa arrow"></span></a>
	<ul class="nav nav-second-level collapse">
		<li>
			<a href="<?php echo base_url('subject/create'); ?>" style="color:white;">Add</a>
			<a href="<?php echo base_url('subject/view'); ?>" style="color:white;">Action</a>
		</li>
	</ul>
</li>

<li<?php echo $selected['attendance']; ?> class="hide"><a href="<?php echo base_url('attendance'); ?>" style="color:white;"><img src="<?php echo base_url('assets/img/attendance.png'); ?>" width="30"> &nbsp; &nbsp; Setup Attendance</a>
</li>



<!-- hide -->
<li class="hide"><a href="javascript:void(0);" style="color:white;"><img src="<?php echo base_url('assets/img/suicide004-512.png'); ?>" width="30"> &nbsp; &nbsp; Student<span class="fa arrow"></span></a>
	<ul class="nav nav-second-level collapse">
		<li>
			<a href="<?php echo base_url('student'); ?>" style="color:white;">Add Student</a>
			<a href="<?php echo base_url('student/action'); ?>" style="color:white;">Action</a>
		</li>
	</ul>
</li>
<li class="hide"><a href="javascript:void(0);" style="color:white;"><img src="<?php echo base_url('assets/img/grades-01.png'); ?>" width="30"> &nbsp; &nbsp; Year Level / Section<span class="fa arrow"></span></a>
	<ul class="nav nav-second-level collapse">
		<li>
			<a href="<?php echo base_url('year/index'); ?>" style="color:white;">Add Year</a>
			<a href="<?php echo base_url('year/action'); ?>" style="color:white;">Action</a>
		</li>
	</ul>
</li>