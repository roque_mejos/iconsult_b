<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header"><?php echo $title; ?></h3>
			</div>
		</div>
		<div class="row"><?php $this->load->view($page); ?></div>
	</div>
</div>