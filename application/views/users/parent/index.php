<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>Announcement</strong></div>
		<div class="panel-body">
			<?php $count = 1; ?>
			<?php foreach($notification as $row) { ?>
				<?php 
					 $current = strtotime(date("Y-m-d"));
					 $date = strtotime(date('Y-m-d', strtotime($row->created)));
					 $datediff = $date - $current;
					 $difference = floor($datediff/(60*60*24));
					 if($difference == 0) {
					 	$label = ' <span class="label label-danger">Today</span>';
					 } else if($difference == -1) {
					 	$label = ' <span class="label label-warning">Yesterday</span>';
					 } else {
					 	$label = '';
					 }
				?>
				<?php $rand = rand(0, 3); ?>
				<?php if($count == 1): ?>
					<div class="row">
				<?php endif;?>
					<div class="col-lg-4">
						<div class="panel panel-<?php echo $panel_class[$rand]; ?>">
							<div class="panel-heading">
								<strong><?php echo ucfirst($row->title); ?></strong> - <?php echo date('F d, Y h:i:s A', strtotime($row->created)) . $label; ?>
							</div>
							<div class="panel-body">
								<p><?php echo $row->message; ?></p>
							</div>
								<div class="panel-footer">
									<strong>Posted By:</strong> <?php echo $row->name; ?>
								</div>
						</div>
					</div>
				<?php
					if(count($notification) >= 3) {
						if($count == 3) {
							echo '</div>';
							$count = 0;
						}
					} else {
						if($count == count($notification)) {
							echo '</div>';
							$count = 0;
						}
					}
					$count++; 
				?>
			<?php } ?>
		</div>
	</div>
</div>