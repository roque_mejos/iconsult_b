<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading"><strong>List of Students</strong></div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="student-table">
					<thead>
						<tr>
							<th>Reference No.</th>
							<th>Firstname</th>
							<th>Lastname</th>
							<th>MI</th>
							<th>Age</th>
							<th>Mobile</th>
							<th>Address</th>
							<th>Year Level</th>
							<th>Section</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($student as $row) { ?>
							<tr>
								<td><?php echo $row->ref_no; ?></td>
								<td><?php echo $row->fname; ?></td>
								<td><?php echo $row->lname; ?></td>
								<td><?php echo $row->mi; ?></td>
								<td><?php echo age(date('m/d/Y', strtotime($row->bdate))); ?></td>
								<td><?php echo $row->mobile; ?></td>
								<td><?php echo $row->address; ?></td>
								<td><?php echo $row->year; ?></td>
								<td><?php echo $row->section; ?></td>
								<td><a href="<?php echo base_url('student/detail/' . $row->id) ?>">Edit</a> | <a href="javascript:void(0);" class="delete-student" data-id="<?php echo $row->id; ?>">Delete</a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/dataTables/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript">
	$(function() {
		var student_table = $('table#student-table'), delete_student_el = $('a.delete-student'), student_id;
		student_table.dataTable();
		$.fn.delete_user = function() {
			return this.click(function() {
				var _this = $(this);
				bootbox.confirm({
					title: "Message",
					message: "Are you sure to delete this student? This will also delete all the student records.",
					size: 'medium',
					buttons: {
						cancel: {
							label: 'No'
						},
						confirm: {
							label: 'Yes'
						}
					},
					callback: function(result) {
						if(result) {
							student_id = _this.data('id');
							delete_student(student_id);
						}
					}
				});
			});
		}

		function delete_student(student_id) {
			var data = {student_id};
			$.ajax({
				type: 'post',
				method: 'post',
				url: '<?php echo base_url('student/delete'); ?>',
				data: data,
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			}).done(function(response) {
				var data = $.parseJSON(response);
				bootbox.alert({
						size: 'medium',
						title: 'Message',
						message: data.message,
						callback: function() {
							window.location = '<?php echo base_url('student/action'); ?>';
						}
				});
			});
		}
		delete_student_el.delete_user();
	});
</script>