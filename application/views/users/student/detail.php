<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<?php echo $message; ?>
	<div class="panel panel-default">
		<div class="panel-heading"><strong>Details</strong></div>
		<div class="panel-body">
			<?php echo form_open('student/update', array('role' => 'form')) ?>
				<input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group<?php echo $fname_has_error ?>">
							<label>Firstname</label>
							<input type="text" name="fname" class="form-control" placeholder="Firstname" value="<?php echo $fname_set_value; ?>">
							<?php echo $fname_error; ?>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group<?php echo $lname_has_error ?>">
							<label>Lastname</label>
							<input type="text" name="lname" class="form-control" placeholder="Lastname" value="<?php echo $lname_set_value; ?>">
							<?php echo $lname_error; ?>
						</div>
					</div>
					<div class="col-lg-1">
						<div class="form-group<?php echo $mi_has_error ?>">
							<label>M.I.</label>
							<input type="text" name="mi" class="form-control" placeholder="M.I." value="<?php echo $mi_set_value; ?>" maxlength="1" required="required">
							<?php echo $mi_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $bdate_has_error ?>">
							<label>Birthdate</label>
							<input type="text" name="bdate" class="form-control" placeholder="Birthdate" value="<?php echo $bdate_set_value; ?>">
							<?php echo $bdate_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $gender_has_error ?>">
							<label>Gender</label>
							<select name="gender" class="form-control">
								<option value="">Gender</option>
								<?php foreach($gender as $row) { 
									$selected = '';
									if($gender_set_value == $row->id) {
										$selected = ' selected="selected"';
									}
								?>
									<option value="<?php echo $row->id; ?>"<?php echo $selected; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $gender_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $mobile_has_error ?>">
							<label>Mobile Number</label>
							<input type="text" name="mobile" class="form-control" placeholder="Mobile Number" value="<?php echo $mobile_set_value; ?>">
							<?php echo $mobile_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $address_has_error ?>">
							<label>Address</label>
							<textarea name="address" class="form-control" placeholder="Address"><?php echo $address_set_value; ?></textarea>
							<?php echo $address_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $year_id_has_error ?>">
							<label>Year Level</label>
							<select name="year_id" class="form-control">
								<option value="">Year Level</option>
								<?php foreach($year as $row) { 
									$selected = '';
									if($year_id_set_value == $row->id) {
										$selected = ' selected="selected"';
									}
								?>
									<option value="<?php echo $row->id; ?>"<?php echo $selected; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $year_id_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group">
							<label>Section</label>
							<select class="form-control" name="section_id">
								<option value="">Section</option>
								<?php foreach($section as $row): 
									if($section_set_value == $row->id) {
										$selected = 'selected';
									} else {
										$selected = '';
									}
								?>
									<option value="<?php echo $row->id ?>"<?php echo $selected ?>><?php echo $row->name ?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $user_id_has_error ?>">
							<label>Choose Parent</label>
							<select name="user_id" class="form-control">
								<option value="">Parent</option>
								<?php foreach($parent as $row) { 
									$selected = '';
									if($user_id_set_value == $row->id) {
										$selected = ' selected="selected"';
									}
								?>
									<option value="<?php echo $row->id; ?>"<?php echo $selected; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $user_id_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $subject_has_error ?>">
							<label>Choose Subject(s)</label>
							<select name="subject[]" class="form-control selectpicker" multiple="multiple" title="Subject">
								<?php foreach($subject as $row) { ?>
									<?php $selected = '';  
										foreach($subject_set_value as $subject_selected) {
											if($row->id == $subject_selected) {
												$selected = ' selected="selected"';
											}
										} 
								?>
									<option value="<?php echo $row->id; ?>"<?php echo $selected; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $subject_error ?>
						</div>
						<button type="submit" name="save" class="btn btn-primary">Update</button>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		var bdate_el = $('input[name="bdate"]');
		bdate_el.datepicker();
	});
</script>