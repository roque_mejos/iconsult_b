<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-lg-12">
	<?php echo $message; ?>
	<div class="panel panel-default">
		<div class="panel-heading"><strong>Create Student</strong></div>
		<div class="panel-body">
			<?php echo form_open('student/insert', array('role' => 'form')) ?>
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group<?php echo $fname_has_error ?>">
							<label>Firstname</label>
							<input type="text" name="fname" class="form-control" placeholder="Firstname" value="<?php echo set_value('fname'); ?>">
							<?php echo $fname_error; ?>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group<?php echo $lname_has_error ?>">
							<label>Lastname</label>
							<input type="text" name="lname" class="form-control" placeholder="Lastname" value="<?php echo set_value('lname'); ?>">
							<?php echo $lname_error; ?>
						</div>
					</div>
					<div class="col-lg-1">
						<div class="form-group<?php echo $mi_has_error ?>">
							<label>M.I.</label>
							<input type="text" name="mi" class="form-control" placeholder="M.I." value="<?php echo set_value('mi'); ?>" maxlength="1" required="required">
							<?php echo $mi_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $bdate_has_error ?>">
							<label>Birthdate</label>
							<input type="text" name="bdate" class="form-control" placeholder="Birthdate" value="<?php echo set_value('bdate'); ?>">
							<?php echo $bdate_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $gender_has_error ?>">
							<label>Gender</label>
							<select name="gender" class="form-control">
								<option value="">Gender</option>
								<?php foreach($gender as $row) { 
									$selected = '';
									if(set_value('gender') == $row->id) {
										$selected = ' selected="selected"';
									}
								?>
									<option value="<?php echo $row->id; ?>"<?php echo $selected; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $gender_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $mobile_has_error ?>">
							<label>Mobile Number</label>
							<input type="text" name="mobile" class="form-control" placeholder="Mobile Number" value="<?php echo set_value('mobile'); ?>">
							<?php echo $mobile_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $address_has_error ?>">
							<label>Address</label>
							<textarea name="address" class="form-control" placeholder="Address"><?php echo set_value('address'); ?></textarea>
							<?php echo $address_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $year_id_has_error ?>">
							<label>Year Level</label>
							<select name="year_id" class="form-control">
								<option value="">Year Level</option>
								<?php foreach($year as $row) { 
									$selected = '';
									if(set_value('year_id') == $row->id) {
										$selected = ' selected="selected"';
									}
								?>
									<option value="<?php echo $row->id; ?>"<?php echo $selected; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $year_id_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $section_id_has_error; ?>">
							<label>Section</label>
							<select class="form-control" name="section_id">
								<option value="">Section</option>
								<?php foreach($section as $row):?>
									<?php $selected='';?>
									<?php if(set_value('section_id')==$row->id):?>
										<?php $selected=' selected';?>
									<?php endif?>
									<option value="<?php echo $row->id?>"<?php echo $selected;?>><?php echo $row->name?></option>
								<?php endforeach?>
							</select>
							<?php echo $section_id_error; ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $user_id_has_error ?>">
							<label>Choose Parent</label>
							<select name="user_id" class="form-control">
								<option value="">Parent</option>
								<?php foreach($parent as $row) { 
									$selected = '';
									if(set_value('user_id') == $row->id) {
										$selected = ' selected="selected"';
									}
								?>
									<option value="<?php echo $row->id; ?>"<?php echo $selected; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $user_id_error ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="form-group<?php echo $subject_has_error ?>">
							<label>Choose Subject(s)</label>
							<select name="subject[]" class="form-control selectpicker" multiple="multiple" title="Subject">
								<?php foreach($subject as $row) { ?>
									<?php $selected = '';  
									if(set_value('subject')) {
										foreach(set_value('subject') as $subject_selected) {
											if($row->id == $subject_selected) {
												$selected = ' selected="selected"';
												break;
											}
										} 
									} 
								?>
									<option value="<?php echo $row->id; ?>"<?php echo $selected; ?>><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
							<?php echo $subject_error ?>
						</div>
						<button type="submit" name="save" class="btn btn-primary">Save</button>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		var bdate_el = $('input[name="bdate"]');
		bdate_el.datepicker();
		$('select[name="year_id"]').change(function() {
			$.ajax({
				type: 'post',
				data: {year_id: $(this).val()},
				url: '<?php echo base_url('student/get_section_by_year_level') ?>',
				dataType: 'json',
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				},
				beforeSend: function() {
					$('select[name="section_id"]').html('<option value="">Loading...</option>');
				},
				success: function(data) {
					var section, option = '';
					if(data.session != true) {
						window.location = '<?php echo base_url(); ?>';
					} else {
						for(var i in data.section) {
							section = data.section[i];
							option += '<option value="'+ section.id +'">'+ section.name +'</option>';
						}
						$('select[name="section_id"]').html(option);
					}
				}
			});
		});
	});
</script>