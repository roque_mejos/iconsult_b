<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('gender_lib', array());
		$this->load->library('user_lib', array());

		$this->load->model('gender_model');
		$this->load->model('user_model');
	}

	public function index() {
		if($this->session->has_userdata('logged_in')) {
			$user_id = $this->session->id;
			$gender = $this->gender_model->read_data();
			$user = $this->user_detail($user_id);
			$gender_set_value = set_value('gender') ? set_value('gender') : $user->gender;
			$data['gender'] = $gender;
			$data['gender_selected'] = $this->gender_selected($gender, $gender_set_value);
			$data['title'] = 'User Profile';
			$data['selected']['dashboard'] = '';
			$data['page'] = 'profile/index';
			$data['content'] = 'users/index';
			$data['message'] = $this->session->flashdata('message');
			$data = array_merge($data, $this->update_form_error($user));
			if($this->session->usertype == ADMIN) {
				$data['side_nav'] = 'users/admin/side-nav';
				$data['selected']['manage'] = '';
			} else if($this->session->usertype == TEACHER) {
				$data['side_nav'] = 'users/teacher/side-nav';
				$data['selected']['announcement'] = '';
				$data['selected']['grade'] = '';
				$data['selected']['student'] = '';
				$data['selected']['report'] = '';
				$data['selected']['subject'] = '';
				$data['selected']['attendance'] = '';
				$data['selected']['year'] = '';
				$data['selected']['calendar_event'] = '';
				$data['selected']['concern'] = '';
			} else {
				$data['side_nav'] = 'users/parent/side-nav';
				$data['selected']['grade'] = '';
				$data['selected']['concern'] = '';
			}
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function update() {
		$image = '';
		$upload_error = false;
		$userdata = array();
		$data = new stdClass();
		if($this->session->has_userdata('logged_in')) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == false) {
				$this->index();
			} else {
				if($_FILES['userfile']['error'] != UPLOAD_ERR_NO_FILE) {
					$config['upload_path'] = './image-profile/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
					$config['max_size'] = 99999;
					$config['max_width'] = 99999;
					$config['max_height'] = 99999;
					$config['encrypt_name'] = true;
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload('userfile')) {
						$this->session->set_flashdata('message', $this->upload->display_errors('<p class="alert alert-danger">', '</span>'));
						$upload_error = true;
						$this->index();
					} else {
						$upload_data = $this->upload->data();
						$image = $upload_data['file_name'];
					}
				}
				if($upload_error != true) {
					$user_id = $this->input->post('user_id');
					$data->fname = $this->input->post('fname');
					$data->lname = $this->input->post('lname');
					$data->mi = $this->input->post('mi');
					$data->bdate = date('Y-m-d', strtotime($this->input->post('bdate')));
					$data->gender = $this->input->post('gender');
					$data->mobile = $this->input->post('mobile');
					$data->address = $this->input->post('address');
					$data->image = $image;
					if($this->user_model->update_data_by_user_id($user_id, $data) != 1) {
						$this->session->set_flashdata('message', '<p class="alert alert-danger">No data updated!</p>');
					} else {
						$this->session->set_flashdata('message', '<p class="alert alert-success">Profile successfully updated!</p>');
						$userdata['fname'] = $data->fname;
						$userdata['lname'] = $data->lname;
						$userdata['gender'] = $data->gender;
						if(strlen($data->image) > 0) {
							$userdata['image'] = $data->image;
						}
						$this->session->set_userdata($userdata);
					}
				}
				redirect('profile/update');
			}
		} else {
			redirect();
		}
	}

	private function update_form_error($user) {
		$data = array();
		// fname
		$data['fname_has_error'] = form_error('fname') ? ' has-error' : '';
		$data['fname_error'] = form_error('fname') ? form_error('fname', '<span class="text-danger">', '</span>') : '';
		$data['fname_set_value'] = set_value('fname') ? set_value('fname') : $user->fname;
		// lname
		$data['lname_has_error'] = form_error('lname') ? ' has-error' : '';
		$data['lname_error'] = form_error('lname') ? form_error('lname', '<span class="text-danger">', '</span>') : '';
		$data['lname_set_value'] = set_value('lname') ? set_value('lname') : $user->lname;
		// mi
		$data['mi_has_error'] = form_error('mi') ? ' has-error' : '';
		$data['mi_error'] = form_error('mi') ? form_error('mi', '<span class="text-danger">', '</span>') : '';
		$data['mi_set_value'] = set_value('mi') ? set_value('mi') : $user->mi;
		// bdate
		$data['bdate_has_error'] = form_error('bdate') ? ' has-error' : '';
		$data['bdate_error'] = form_error('bdate') ? form_error('bdate', '<span class="text-danger">', '</span>') : '';
		$data['bdate_set_value'] = set_value('bdate') ? set_value('bdate') : $user->bdate;
		// gender
		$data['gender_has_error'] = form_error('gender') ? ' has-error' : '';
		$data['gender_error'] = form_error('gender') ? form_error('gender', '<span class="text-danger">', '</span>') : '';
		// mobile
		$data['mobile_has_error'] = form_error('mobile') ? ' has-error' : '';
		$data['mobile_error'] = form_error('mobile') ? form_error('mobile', '<span class="text-danger">', '</span>') : '';
		$data['mobile_set_value'] = set_value('mobile') ? set_value('mobile') : $user->mobile;
		// address
		$data['address_has_error'] = form_error('address') ? ' has-error' : '';
		$data['address_error'] = form_error('address') ? form_error('address', '<span class="text-danger">', '</span>') : '';
		$data['address_set_value'] = set_value('address') ? set_value('address') : $user->address;

		return $data;
	}

	private function gender_selected($gender, $gender_set_value) {
		$selected = array();
		foreach($gender as $row) {
			if($row->id != $gender_set_value) {
				$selected[$row->id] = '';
			} else {
				$selected[$row->id] = 'selected="selected"';
			}
		}
		return $selected;
	}

	private function user_detail($user_id) {
		$user = new stdClass();
		$user->id = $user->fname = $user->lname = $user->mi = $user->gender = $user->mobile = $user->address = $user->bdate = '';
		if($this->user_model->count_data_by_user_id($user_id) > 0) {
			$row = $this->user_model->read_data_by_user_id($user_id);
			$user->id = $row->id;
			$user->fname = $row->fname;
			$user->lname = $row->lname;
			$user->mi = $row->mi;
			$user->gender = $row->gender;
			$user->mobile = $row->mobile;
			$user->address = $row->address;
			$user->bdate = date('m/d/Y', strtotime($row->bdate));
		}
		return $user;
	}

	private function rules() {
		$config = array(
			array(
				'field' => 'user_id',
				'label' => 'User ID',
				'rules' => 'required|numeric|integer'
				),
			array(
				'field' => 'fname',
				'label' => 'Firstname',
				'rules' => 'required'
				),
			array(
				'field' => 'lname',
				'label' => 'Lastname',
				'rules' => 'required'
				),
			array(
				'field' => 'mi',
				'label' => 'Middle Initial',
				'rules' => 'required|exact_length[1]'
				),
			array(
				'field' => 'bdate',
				'label' => 'Birthdate',
				'rules' => 'required|callback_check_bdate'
				),
			array(
				'field' => 'gender',
				'label' => 'Gender',
				'rules' => 'required|in_list['. MALE .','. FEMALE .']'
				),
			array(
				'field' => 'mobile',
				'label' => 'Mobile Number',
				'rules' => 'required|min_length[11]|max_length[20]'
				),
			array(
				'field' => 'address',
				'label' => 'Address',
				'rules' => 'required'
				)
			);
		return $config;
	}

	public function check_bdate($bdate) {
		if(!date_valid_format($bdate)) {
			$this->form_validation->set_message('check_bdate', 'Invalid %s format.');
			return false;
		}
		if(age($bdate) < 1) {
			$this->form_validation->set_message('check_bdate', 'Invalid year.');
			return false;
		}
		return true;
	}
}