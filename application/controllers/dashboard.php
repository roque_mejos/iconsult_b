<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('user_lib', array());
		$this->load->library('announcement_lib', array());

		$this->load->model('user_model');
		$this->load->model('announcement_model');
	}

	public function index() {
		if($this->session->has_userdata('logged_in')) {
			$data = array();
			// landing page
			switch($this->session->usertype) {
				case ADMIN:
					$data['users'] = $this->user_model->read_data_not_by_users_and_admin($this->session->id);
					$data['title'] = 'Manage Users';
					$data['selected']['manage'] = '';
					$data['selected']['dashboard'] = ' class="active-nav"';
					$data['side_nav'] = 'users/admin/side-nav';
					$data['page'] = 'users/admin/manage';
				break;

				case TEACHER:
					$data['panel_class'] = array('success', 'danger', 'info', 'primary', 'default');
					$data['notification'] = $this->announcement_model->read_data();
					$data['title'] = 'Dashboard';
					$data['selected']['student'] = '';
					$data['selected']['grade'] = '';
					$data['selected']['subject'] = '';
					$data['selected']['dashboard'] = ' class="active-nav"';
					$data['selected']['announcement'] = '';
					$data['side_nav'] = 'users/teacher/side-nav';
					$data['page'] = 'users/teacher/index';
					$data['selected']['year'] = '';
					$data['selected']['calendar_event'] = '';
					$data['selected']['attendance'] = '';
					$data['selected']['report'] = '';
					$data['selected']['concern'] = '';
				break;

				case PARENTS:
					$data['panel_class'] = array('success', 'danger', 'info', 'primary', 'default');
					$data['side_nav'] = 'users/parent/side-nav';
					$data['page'] = 'users/parent/index';
					$data['title'] = 'Dashboard';
					$data['selected']['concern'] = '';
					$data['selected']['dashboard'] = ' class="active-nav"';
					$data['selected']['grade'] = '';
					$data['notification'] = $this->announcement_model->read_data();
				break;
			}
			$data['content'] = 'users/index';
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}
}