<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		// lib
		$this->load->library('user_lib', array());
		// model
		$this->load->model('user_model');
	}

	public function index() {
		$usertype_set_value = array();
		$data = array('invalid' => '');
		$data['title'] = 'Login Page';
		$data['content'] = 'login/index';
		$data['email_has_error'] = form_error('username') ? ' has-error' : '';
		$data['password_has_error'] = form_error('password') ? ' has-error' : '';
		if($this->session->has_userdata('invalid')) {
			$data['invalid'] = $this->session->flashdata('invalid');
		}
		if($this->session->has_userdata('logged_in')) {
			redirect('dashboard');
		}
		$this->load->view('index', $data);
	}

	public function verify() {
		$userdata = array();
		$data = new stdClass();
		$data->username = $this->input->post('username');
		$data->password = $this->input->post('password');
		$data->usertype = $this->input->post('usertype');
		$this->form_validation->set_rules($this->rules());
		if($this->form_validation->run() == false) {
			$this->index();
		} else {
			$count = $this->user_model->count_user($data);
			$row = $this->user_model->check_user($data);
			if($count == 1) {
				$userdata['id'] = $row->id;
				$userdata['username'] = $row->username;
				$userdata['usertype'] = $row->usertype;
				$userdata['usertype_name'] = $row->name;
				$userdata['fname'] = $row->fname;
				$userdata['lname'] = $row->lname;
				$userdata['image'] = $row->image;
				$userdata['gender'] = $row->gender;
				$userdata['logged_in'] = true;
				$this->session->set_userdata($userdata);
				redirect('dashboard');
			} else {
				$this->session->set_flashdata('invalid', '<p class="alert alert-danger">Invalid Email Address or Password.</p>');
				redirect();
			}
		}
	}

	public function rules() {
		$config = array(
			array(
				'field' => 'username',
				'label' => 'Email Address',
				'rules' => 'required|valid_email'
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required|min_length[6]|max_length[25]'
			)
		);
		return $config;
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect();
	}
}