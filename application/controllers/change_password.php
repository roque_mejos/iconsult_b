<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Change_password extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('user_lib', array());
		$this->load->model('user_model');
	}

	public function index() {
		if($this->session->has_userdata('logged_in')) {
			$data['success'] = '';
			$data['fail'] = '';
			$data['title'] = 'Change Password';
			$data['page'] = 'users/change-password/index';
			$data['content'] = 'users/index';
			$data['selected']['dashboard'] = '';
			$data['selected']['year'] = '';
			$data = array_merge($data, $this->form_error_response());
			if($this->session->has_userdata('success')) {
				$data['success'] = '<p class="alert alert-success text-left"><strong>' . $this->session->flashdata('success') . '</strong></p>';
			}
			if($this->session->has_userdata('fail')) {
				$data['fail'] = '<p class="alert alert-danger text-left"><strong>' . $this->session->flashdata('fail') . '</strong></p>';
			}
			if($this->session->usertype == ADMIN) {
				$data['side_nav'] = 'users/admin/side-nav';
			} else if($this->session->usertype == TEACHER) {
				$data['side_nav'] = 'users/teacher/side-nav';
				$data['selected']['announcement'] = '';
				$data['selected']['grade'] = '';
				$data['selected']['student'] = '';
				$data['selected']['subject'] = '';
				$data['selected']['attendance'] = '';
				$data['selected']['concern'] = '';
				$data['selected']['year'] = '';
				$data['selected']['calendar_event'] = '';
				$data['selected']['report'] = '';
			} else {
				$data['side_nav'] = 'users/parent/side-nav';
				$data['selected']['grade'] = '';
				$data['selected']['concern'] = '';
			}
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function update() {
		if($this->session->has_userdata('logged_in')) {
			$user_id = $this->session->id;
			$password = md5($this->input->post('password'));
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == false) {
				$this->index();
			} else {
				if($this->user_model->update_password_by_user_id($password, $user_id) != 1) {
					$this->session->set_flashdata('fail', 'Failed to update password.');
				} else {
					$this->session->set_flashdata('success', 'Password changed!');
				}
				redirect('user/change-password/update');
			}
		} else {
			redirect();
		}
	}

	private function rules() {
		$config = array(
			array(
				'field' => 'current_password',
				'label' => 'Current Password',
				'rules' => 'callback_check_current_password'
				),
			array(
				'field' => 'password',
				'label' => 'New Password',
				'rules' => 'required|min_length[6]|max_length[25]'
				),
			array(
				'field' => 'confirm_password',
				'label' => 'Confirm Password',
				'rules' => 'required|min_length[6]|max_length[25]|matches[password]'
				)
			);
		return $config;
	}

	private function form_error_response() {
		$data = array();
		// current_password
		$data['current_password_has_error'] = form_error('current_password') ? ' has-error' : '';
		$data['current_password_error'] = form_error('current_password') ? form_error('current_password', '<span class="text-danger">', '</span>') : '';

		$data['password_has_error'] = form_error('password') ? ' has-error' : '';
		$data['password_error'] = form_error('password') ? form_error('password', '<span class="text-danger">', '</span>') : '';

		$data['confirm_password_has_error'] = form_error('confirm_password') ? ' has-error' : '';
		$data['confirm_password_error'] = form_error('confirm_password') ? form_error('confirm_password', '<span class="text-danger">', '</span>') : '';

		return $data;
	}

	public function check_current_password($current_password) {
		$current_password = md5($current_password);
		$user_id = $this->session->id;
		$password = '';
		if($this->user_model->read_password_by_user_id($user_id)) {
			$password = $this->user_model->read_password_by_user_id($user_id);
		}
		if($current_password != $password) {
			$this->form_validation->set_message('check_current_password', 'Invalid %s.');
			return false;
		}
		return true;
	}
}