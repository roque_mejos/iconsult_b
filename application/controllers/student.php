<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;

class Student extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('subject_lib', array());
		$this->load->library('user_lib', array());
		$this->load->library('gender_lib', array());
		$this->load->library('year_lib', array());
		$this->load->library('student_lib', array());
		$this->load->library('student_record_lib', array());

		$this->load->model('subject_model');
		$this->load->model('user_model');
		$this->load->model('gender_model');
		$this->load->model('year_model');
		$this->load->model('student_model');
		$this->load->model('student_record_model');
		$this->load->model('section_model');
		$this->load->model('grade_model');
		$this->load->model('written_work_model');
		$this->load->model('task_performance_model');
		$this->load->model('quarterly_assessment_model');
		$this->load->model('school_date_model');
		$this->load->model('attendance_model');
	}

	public function index() {
		$data = array('section' => array());
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['year'] = $this->year_model->read_data();
			$data['gender'] = $this->gender_model->read_data();
			$data['parent'] = $this->user_model->read_name_by_parent_usertype();
			$data['subject'] = $this->subject_model->read_data_by_active();
			$data['title'] = 'Student';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['student'] = ' class="active-nav"';
			$data['selected']['announcement'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'users/student/index';
			$data['content'] = 'users/index';
			$data['selected']['year'] = '';
			$data['selected']['report'] = '';
			$data['selected']['calendar_event'] = '';
			$data['selected']['concern'] = '';
			$data['message'] = $this->session->flashdata('message');
			$data = array_merge($data, $this->create_form_error());
			if($this->input->post('section_id')) {
				$data['section'] = $this->section_model->read_data_by_year_id($this->input->post('year_id'));
			}
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	private function create_form_error() {
		$data = array();
		// fname
		$data['fname_has_error'] = form_error('fname') ? ' has-error' : '';
		$data['fname_error'] = form_error('fname') ? form_error('fname', '<span class="text-danger">', '</span>') : '';
		// lname
		$data['lname_has_error'] = form_error('lname') ? ' has-error' : '';
		$data['lname_error'] = form_error('lname') ? form_error('lname', '<span class="text-danger">', '</span>') : '';
		// mi
		$data['mi_has_error'] = form_error('mi') ? ' has-error' : '';
		$data['mi_error'] = form_error('mi') ? form_error('mi', '<span class="text-danger">', '</span>') : '';
		// bdate
		$data['bdate_has_error'] = form_error('bdate') ? ' has-error' : '';
		$data['bdate_error'] = form_error('bdate') ? form_error('bdate', '<span class="text-danger">', '</span>') : '';
		// gender
		$data['gender_has_error'] = form_error('gender') ? ' has-error' : '';
		$data['gender_error'] = form_error('gender') ? form_error('gender', '<span class="text-danger">', '</span>') : '';
		// mobile
		$data['mobile_has_error'] = form_error('mobile') ? ' has-error' : '';
		$data['mobile_error'] = form_error('mobile') ? form_error('mobile', '<span class="text-danger">', '</span>') : '';
		// address
		$data['address_has_error'] = form_error('address') ? ' has-error' : '';
		$data['address_error'] = form_error('address') ? form_error('address', '<span class="text-danger">', '</span>') : '';
		// year
		$data['year_id_has_error'] = form_error('year_id') ? ' has-error' : '';
		$data['year_id_error'] = form_error('year_id') ? form_error('year_id', '<span class="text-danger">', '</span>') : '';
		// parent
		$data['user_id_has_error'] = form_error('user_id') ? ' has-error' : '';
		$data['user_id_error'] = form_error('user_id') ? form_error('user_id', '<span class="text-danger">', '</span>') : '';
		// subject
		$data['subject_has_error'] = form_error('subject[]') ? ' has-error' : '';
		$data['subject_error'] = form_error('subject[]') ? form_error('subject[]', '<span class="text-danger">', '</span>') : '';

		$data['section_id_has_error'] = form_error('section_id') ? ' has-error' : '';
		$data['section_id_error'] = form_error('section_id') ? form_error('section_id', '<span class="text-danger">', '</span>') : '';
		return $data;
	}

	public function insert() {
		$ref_no = time();
		$data = new stdClass();
		$data->fname = $this->input->post('fname');
		$data->lname = $this->input->post('lname');
		$data->mi = $this->input->post('mi');
		$data->subject = $this->input->post('subject');
		$data->bdate = date('Y-m-d', strtotime($this->input->post('bdate')));
		$data->gender = $this->input->post('gender');
		$data->mobile = $this->input->post('mobile');
		$data->address = $this->input->post('address');
		$data->user_id = $this->input->post('user_id');
		$data->year_id = $this->input->post('year_id');
		$data->section_id = $this->input->post('section_id');
		$data->ref_no = $ref_no;
		$status = true;
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run('create') == false) {
				$this->index();
			} else {
				$student_id = $this->student_model->insert_data($data);
				if($student_id > 0) {
					foreach($data->subject as $subject_id) {
						$student_record_id = $this->student_record_model->insert_data($subject_id, $student_id);
						if($student_record_id > 0) {
							foreach(GRADING_TYPE as $grading_type) {
								$grade_affected_rows = $this->grade_model->insert_data($grading_type, $student_record_id);
								if($grade_affected_rows == 1) {
									$status = true;
								}
							}
						}
					}
				}
				if($status) {
					$this->session->set_flashdata('message', '<p class="alert alert-success">Ref. No.: '. $ref_no .'<br>Student successfully created!</p>');
				} else {
					$this->session->set_flashdata('message', '<p class="alert alert-danger">Failed to insert data!</p>');
				}
				redirect('student');
			}
		} else {
			redirect();
		}
	}

	private function rules() {
		$config = array(
			array(
				'field' => 'subject[]',
				'label' => 'Subject',
				'rules' => 'required'
				),
			array(
				'field' => 'fname',
				'label' => 'Firstname',
				'rules' => 'required'
				),
			array(
				'field' => 'lname',
				'label' => 'Lastname',
				'rules' => 'required'
				),
			array(
				'field' => 'user_id',
				'label' => 'Parent',
				'rules' => 'required'
				),
			array(
				'field' => 'mi',
				'label' => 'Middle Initial',
				'rules' => 'required|exact_length[1]'
				),
			array(
				'field' => 'bdate',
				'label' => 'Birthdate',
				'rules' => 'required|callback_check_bdate'
				),
			array(
				'field' => 'gender',
				'label' => 'Gender',
				'rules' => 'required|in_list['. MALE .','. FEMALE .']'
				),
			array(
				'field' => 'mobile',
				'label' => 'Mobile Number',
				'rules' => 'min_length[11]|max_length[20]'
				),
			array(
				'field' => 'address',
				'label' => 'Address',
				'rules' => 'required'
				),
			array(
				'field' => 'year_id',
				'label' => 'Year',
				'rules' => 'required|integer|numeric'
				),
			array(
				'field' => 'section_id',
				'label' => 'Section',
				'rules' => 'required|integer|numeric'
				)
		);
		if($this->input->post()) {

		}
		return $config;
	}

	public function check_bdate($bdate) {
		if(!date_valid_format($bdate)) {
			$this->form_validation->set_message('check_bdate', 'Invalid %s format.');
			return false;
		}
		if(age($bdate) < 1) {
			$this->form_validation->set_message('check_bdate', 'Invalid year.');
			return false;
		}
		return true;
	}

	public function action() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['title'] = 'Student';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['student'] = ' class="active-nav"';
			$data['selected']['announcement'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['selected']['report'] = '';
			$data['page'] = 'users/student/action';
			$data['content'] = 'users/index';
			$data['selected']['year'] = '';
			$data['selected']['calendar_event'] = '';
			$data['selected']['concern'] = '';
			$data['message'] = $this->session->flashdata('message');
			$data = array_merge($data, $this->create_form_error());
			$data['student'] = $this->student_model->read_data_by_teacher($this->session->id);
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function delete() {
		$result = array('message' => '');
		$student_id = $this->input->post('student_id');
		$message = 'Failed to delete student user!';
		$flag = false;
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			
			$student_record = $this->student_record_model->read_student_record_id_by_student_id($student_id);

			foreach($student_record as $row) {
				foreach($this->grade_model->read_data_by_student_record_id($row->id) as $grade_row) {
					$this->written_work_model->delete_data_by_grade_id($grade_row->id);
					$this->task_performance_model->delete_data_by_grade_id($grade_row->id);
					$this->quarterly_assessment_model->delete_data_by_grade_id($grade_row->id);
				}
				if($this->grade_model->delete_data_by_student_record_id($row->id)) {
					if($this->student_record_model->delete_data_by_student_record_id($row->id) == 1) {
						if($this->student_model->delete_data_by_student_id($student_id) == 1) {
							$flag = true;
						}
					}
				}
			}

			if($flag) {
				$message = 'Student successfully deleted!';
			} else {
				$message = 'Failed to delete student!';
			}

			$result['message'] = $message;
		}
		echo json_encode($result);
	}

	public function detail($student_id = 0) {
		$no_of_student = $this->student_model->count_data_by_student_id($student_id);
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER && $student_id != 0 && $no_of_student != 0) {
			$student = $this->read_student_data($student_id);
			$data['section'] = $this->section_model->read_data();
			$data['student_id'] = $student_id;
			$data['year'] = $this->year_model->read_data();
			$data['gender'] = $this->gender_model->read_data();
			$data['parent'] = $this->user_model->read_name_by_parent_usertype();
			$data['subject'] = $this->subject_model->read_data_by_active();
			$data['title'] = 'Student';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['student'] = ' class="active-nav"';
			$data['selected']['announcement'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['selected']['report'] = '';
			$data['page'] = 'users/student/detail';
			$data['content'] = 'users/index';
			$data['selected']['year'] = '';
			$data['selected']['calendar_event'] = '';
			$data['selected']['concern'] = '';
			$data['message'] = $this->session->flashdata('message');
			$data = array_merge($data, $this->update_form_error($student));
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	private function read_student_data($student_id) {
		$data = new stdClass();
		$subject_data = array();
		$subject = $this->student_record_model->read_data_by_student_id($student_id);
		$student = $this->student_model->read_data_by_student_id($student_id);
		foreach($subject as $row) {
			$subject_data[] = $row->subject_id;
		}
		$data->fname = $student->fname;
		$data->lname = $student->lname;
		$data->mi = $student->mi;
		$data->bdate = $student->bdate;
		$data->gender = $student->gender;
		$data->mobile = $student->mobile;
		$data->address = $student->address;
		$data->year_id = $student->year_id;
		$data->user_id = $student->parent;
		$data->subject = $subject_data;
		$data->section = $student->section;
		return $data;
	}

	private function update_form_error($student) {
		$data = array();
		// fname
		$data['fname_has_error'] = form_error('fname') ? ' has-error' : '';
		$data['fname_error'] = form_error('fname') ? form_error('fname', '<span class="text-danger">', '</span>') : '';
		$data['fname_set_value'] = set_value('fname') ? set_value('fname') : $student->fname;
		// lname
		$data['lname_has_error'] = form_error('lname') ? ' has-error' : '';
		$data['lname_error'] = form_error('lname') ? form_error('lname', '<span class="text-danger">', '</span>') : '';
		$data['lname_set_value'] = set_value('lname') ? set_value('lname') : $student->lname;
		// mi
		$data['mi_has_error'] = form_error('mi') ? ' has-error' : '';
		$data['mi_error'] = form_error('mi') ? form_error('mi', '<span class="text-danger">', '</span>') : '';
		$data['mi_set_value'] = set_value('mi') ? set_value('mi') : $student->mi;
		// bdate
		$data['bdate_has_error'] = form_error('bdate') ? ' has-error' : '';
		$data['bdate_error'] = form_error('bdate') ? form_error('bdate', '<span class="text-danger">', '</span>') : '';
		$data['bdate_set_value'] = set_value('bdate') ? set_value('bdate') : date('m/d/Y', strtotime($student->bdate));
		// gender
		$data['gender_has_error'] = form_error('gender') ? ' has-error' : '';
		$data['gender_error'] = form_error('gender') ? form_error('gender', '<span class="text-danger">', '</span>') : '';
		$data['gender_set_value'] = set_value('gender') ? set_value('gender') : $student->gender;
		// mobile
		$data['mobile_has_error'] = form_error('mobile') ? ' has-error' : '';
		$data['mobile_error'] = form_error('mobile') ? form_error('mobile', '<span class="text-danger">', '</span>') : '';
		$data['mobile_set_value'] = set_value('mobile') ? set_value('mobile') : $student->mobile;
		// address
		$data['address_has_error'] = form_error('address') ? ' has-error' : '';
		$data['address_error'] = form_error('address') ? form_error('address', '<span class="text-danger">', '</span>') : '';
		$data['address_set_value'] = set_value('address') ? set_value('address') : $student->address;
		// year
		$data['year_id_has_error'] = form_error('year_id') ? ' has-error' : '';
		$data['year_id_error'] = form_error('year_id') ? form_error('year_id', '<span class="text-danger">', '</span>') : '';
		$data['year_id_set_value'] = set_value('year_id') ? set_value('year_id') : $student->year_id;
		// parent
		$data['user_id_has_error'] = form_error('user_id') ? ' has-error' : '';
		$data['user_id_error'] = form_error('user_id') ? form_error('user_id', '<span class="text-danger">', '</span>') : '';
		$data['user_id_set_value'] = set_value('user_id') ? set_value('user_id') : $student->user_id;
		// subject
		$data['subject_has_error'] = form_error('subject[]') ? ' has-error' : '';
		$data['subject_error'] = form_error('subject[]') ? form_error('subject[]', '<span class="text-danger">', '</span>') : '';
		$data['subject_set_value'] = $student->subject;

		$data['section_has_error'] = form_error('section') ? ' has-error' : '';
		$data['section_error'] = form_error('section') ? form_error('section', '<p class="text-danger">', '</p>') : '';
		$data['section_set_value'] = set_value('section') ? set_value('section') : $student->section;

		return $data;
	}

	public function update() {
		$student_record_id = 0;
		$delete_subject = array();
		$add_subject = array();
		$original_subject = array();
		$flag = false;
		$student_id = $this->input->post('student_id');
		$subject_data = $this->student_record_model->read_data_by_student_id($student_id);
		$data = new stdClass();
		$data->fname = $this->input->post('fname');
		$data->lname = $this->input->post('lname');
		$data->mi = $this->input->post('mi');
		$data->subject = $this->input->post('subject');
		$data->bdate = date('Y-m-d', strtotime($this->input->post('bdate')));
		$data->gender = $this->input->post('gender');
		$data->mobile = $this->input->post('mobile');
		$data->address = $this->input->post('address');
		$data->user_id = $this->input->post('user_id');
		$data->year_id = $this->input->post('year_id');
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == false) {
				$this->detail($student_id);
			} else {
				if($this->student_model->update_data_by_student_id($data, $student_id) == 1) {
					$flag = true;
				}
				foreach($subject_data as $row) {
					$original_subject[] = $row->subject_id;
				}
				$new_subject = $data->subject;
				$delete_subject = array_diff($original_subject, $new_subject);
				$add_subject = array_diff($new_subject, $original_subject);
				if(count($delete_subject) > 0 || count($add_subject) > 0) {
					foreach($delete_subject as $subject_id) {
						$student_record_id = $this->student_record_model->read_student_record_id_by_subject_id_and_student_id($subject_id, $student_id);
						if($student_record_id) {
							foreach($this->grade_model->read_data_by_student_record_id($student_record_id) as $grade_row) {
								$this->written_work_model->delete_data_by_grade_id($grade_row->id);
								$this->task_performance_model->delete_data_by_grade_id($grade_row->id);
								$this->quarterly_assessment_model->delete_data_by_grade_id($grade_row->id);
							}
							if($this->grade_model->delete_data_by_student_record_id($student_record_id)) {
								if($this->student_record_model->delete_data_by_student_record_id($student_record_id) == 1) {
									$flag = true;
								}
							}
						}
					}
					foreach($add_subject as $subject_id) {
						$student_record_id = $this->student_record_model->insert_data($subject_id, $student_id);
						if($student_record_id) {
							foreach(GRADING_TYPE as $grading_type) {
								if($this->grade_model->insert_data($grading_type, $student_record_id)) {
									$flag = true;
								}
							}
						}
					}
				}
				if($flag) {
					$this->session->set_flashdata('message', '<p class="alert alert-success">Student successfully updated!</p>');
				} else {
					$this->session->set_flashdata('message', '<p class="alert alert-danger">No data is updated!</p>');
				}
				redirect('student/detail/' . $student_id);
			}
		} else {
			redirect();
		}
	}

	public function get_section_by_year_level() {
		$result = array('session' => false, 'error' => false, 'success' => false);
		$year_id = $this->input->post('year_id');
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$result['session'] = true;
			$result['section'] = $this->section_model->read_data_by_year_id($year_id);
		}
		echo json_encode($result);
	}

	public function list() {
		$data = array();
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['title'] = 'List of all students';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['student'] = ' class="active-nav"';
			$data['selected']['announcement'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['selected']['report'] = '';
			$data['page'] = 'users/student/list';
			$data['content'] = 'users/index';
			$data['selected']['year'] = '';
			$data['selected']['calendar_event'] = '';
			$data['selected']['concern'] = '';
			$data['student'] = $this->student_model->read_data_by_teacher($this->session->id);
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function report() {
		if($this->session->has_userdata('logged_in') && ($this->session->usertype == TEACHER || $this->session->usertype == PARENTS)) {
			if($this->session->usertype == TEACHER) {
				$data['selected']['dashboard'] = '';
				$data['selected']['grade'] = '';
				$data['selected']['attendance'] = '';
				$data['selected']['subject'] = '';
				$data['selected']['student'] = '';
				$data['selected']['announcement'] = '';
				$data['side_nav'] = 'users/teacher/side-nav';
				$data['selected']['report'] = ' class="active-nav"';
				$data['selected']['year'] = '';
				$data['selected']['calendar_event'] = '';
				$data['selected']['concern'] = '';
				$data['student'] = $this->student_model->read_data_by_teacher($this->session->id);
			} else {
				$data['selected']['calendar_event'] = '';
				$data['selected']['concern'] = '';
				$data['selected']['dashboard'] = '';
				$data['selected']['grade'] = ' class="active-nav"';
				$data['side_nav'] = 'users/parent/side-nav';
				$data['student'] = $this->student_model->read_data_by_parent($this->session->id);
			}
			$data['page'] = 'users/student/report';
			$data['title'] = 'List of all students';
			$data['content'] = 'users/index';
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function reports($student_id = 0) {
		$data = array();
		$subject = array();
		$first_grading_total = array();
		$second_grading_total = array();
		$third_grading_total = array();
		$fourth_grading_total = array();
		$first_ave = array();
		$tot_ave = 0;
		$school_year = 0;
		$sy = 0;

		$num_of_days = array();
		$present_days = array();
		$absent_days = array();

		if($this->student_model->count_school_year_by_student_id($student_id)) {
			$school_year = $this->student_model->read_school_year_by_student_id($student_id);
		}

		for($month = 1; $month <= NUM_OF_MONTH; $month++) {
			if($month >= 1 && $month <= 5) {
				$sy = $school_year + 1;
			}
			$num_of_days[] = $this->school_date_model->count_days_by_month_and_year($student_id, $month, $sy);
			$present_days[] = $this->attendance_model->count_date_by_present_status_and_student_id_and_month_and_year($student_id, $month, $sy);
			$absent_days[] = $this->attendance_model->count_date_by_absent_status_and_student_id_and_month_and_year($student_id, $month, $sy);
		}
		// echo "<pre>", print_r($num_of_days), "</pre>"; exit;
		foreach($this->student_record_model->read_data_by_student_id($student_id) as $row) {
			$subject[] = $row->subject;
			foreach($this->grade_model->read_data_by_student_record_id($row->id) as $grade_detail) {
				switch($grade_detail->grading_type) {
					case '1st':
						$first_grading_total[$row->subject] = number_format($this->compute_written_work_by_grade_id($grade_detail->id) + $this->compute_task_performance_by_grade_id($grade_detail->id) + $this->compute_quarterly_assessment_by_grade_id($grade_detail->id),2);
					break;
					case '2nd':
						$second_grading_total[$row->subject] = number_format($this->compute_written_work_by_grade_id($grade_detail->id) + $this->compute_task_performance_by_grade_id($grade_detail->id) + $this->compute_quarterly_assessment_by_grade_id($grade_detail->id),2);
					break;
					case '3rd':
						$third_grading_total[$row->subject] = number_format($this->compute_written_work_by_grade_id($grade_detail->id) + $this->compute_task_performance_by_grade_id($grade_detail->id) + $this->compute_quarterly_assessment_by_grade_id($grade_detail->id),2);
					break;
					case '4th':
						$fourth_grading_total[$row->subject] = number_format($this->compute_written_work_by_grade_id($grade_detail->id) + $this->compute_task_performance_by_grade_id($grade_detail->id) + $this->compute_quarterly_assessment_by_grade_id($grade_detail->id),2);
					break;
				}
			}
			$first_ave[$row->subject] = number_format(($first_grading_total[$row->subject] + $second_grading_total[$row->subject] + $third_grading_total[$row->subject] + $fourth_grading_total[$row->subject]) / 4,2);
			$tot_ave += $first_ave[$row->subject];
		}
		
		$data['subject'] = $subject;
		$data['first_grading_total'] = $first_grading_total;
		$data['second_grading_total'] = $second_grading_total;
		$data['third_grading_total'] = $third_grading_total;
		$data['fourth_grading_total'] = $fourth_grading_total;
		$data['first_ave'] = $first_ave;
		$data['tot_ave'] = $tot_ave;

		$data['num_of_days'] = $num_of_days;
		$data['present_days'] = $present_days;
		$data['absent_days'] = $absent_days;

		$html = $this->load->view('report/index', $data, true);
		$this->load->file("dompdf/autoload.inc.php");
		$dompdf = new Dompdf();
		$dompdf->loadHtml($html);
		$dompdf->setPaper('A4', 'landscape');
		$dompdf->render();
		$dompdf->stream(time());
	}

	private function compute_written_work_by_grade_id($grade_id) {
		$data = $this->written_work_model->read_data_by_grade_id($grade_id);
		$score = 0;
		$perfect_score = 0;
		$ave = 0;
		foreach($data as $row) {
			$score += $row->score;
			$perfect_score += $row->perfect_score;
		}
		if($score != 0 && $perfect_score != 0) {
			$ave = $score / $perfect_score * WRITTEN_WORK_PERCENTAGE;
		}
		return $ave;
	}

	private function compute_task_performance_by_grade_id($grade_id) {
		$data = $this->task_performance_model->read_data_by_grade_id($grade_id);
		$score = 0;
		$perfect_score = 0;
		$ave = 0;
		foreach($data as $row) {
			$score += $row->score;
			$perfect_score += $row->perfect_score;
		}
		if($score != 0 && $perfect_score != 0) {
			$ave = $score / $perfect_score * TASK_PERFORMANCE_PERCENTAGE;
		}
		return $ave;
	}

	private function compute_quarterly_assessment_by_grade_id($grade_id) {
		$data = $this->quarterly_assessment_model->read_data_by_grade_id($grade_id);
		$score = 0;
		$perfect_score = 0;
		$ave = 0;
		foreach($data as $row) {
			$score += $row->score;
			$perfect_score += $row->perfect_score;
		}
		if($score != 0 && $perfect_score != 0) {
			$ave = $score / $perfect_score * QUARTERLY_PERFORMANCE_PERCENTAGE;
		}
		return $ave;
	}
}