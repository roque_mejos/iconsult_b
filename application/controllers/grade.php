<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Dompdf\Dompdf;
class Grade extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('student_lib', array());
		$this->load->library('student_record_lib', array());
		$this->load->library('user_lib', array());
		$this->load->library('subject_lib', array());

		$this->load->model('user_model');
		$this->load->model('student_model');
		$this->load->model('student_record_model');
		$this->load->model('grade_model');
		$this->load->model('written_work_model');
		$this->load->model('task_performance_model');
		$this->load->model('quarterly_assessment_model');
		$this->load->model('grade_model');
		$this->load->model('subject_model');
	}

	public function index() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['student'] = $this->student_model->read_data_by_teacher($this->session->id);
			$data['title'] = 'Student';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = ' class="active-nav"';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'grade/index';
			$data['selected']['year'] = '';
			$data['selected']['concern'] = '';
			$data['selected']['calendar_event'] = '';
			$data['content'] = 'users/index';
			$data['selected']['report'] = '';
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function detail($student_id = 0) {
		$subject = array();
		$subject_id = array();
		$tot_ave = 0;
		$ave = 0;
		$first_grading_total = array();
		$second_grading_total = array();
		$third_grading_total = array();
		$fourth_grading_total = array();
		$first_ave = array();
		$counter = 0;
		$grading_total = array('1st' => 0, '2nd' => 0, '3rd' => 0, '4th' => 0);
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER && $student_id != 0 && $this->student_record_model->count_data_by_student_id($student_id) != 0) {
			$grade = array();
			$name = $this->student_model->read_data_by_student_id($student_id)->name;
			$data['student_id'] = $student_id;
			$data['name'] = $name;
			$data['student_record'] = $this->student_record_model->read_data_by_student_id($student_id);
			$data['title'] = 'Grades';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = ' class="active-nav"';
			$data['selected']['student'] = '';
			$data['selected']['announcement'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['report'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['year'] = '';
			$data['selected']['concern'] = '';
			$data['selected']['calendar_event'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'grade/detail';
			$data['content'] = 'users/index';
			foreach($this->student_record_model->read_data_by_student_id($student_id) as $row) {
				$subject[] = $row->subject;
				$subject_id[] = $row->subject_id;
				$counter++;
				foreach($this->grade_model->read_data_by_student_record_id($row->id) as $grade_detail) {
					switch($grade_detail->grading_type) {
						case '1st':
							$first_grading_total[$row->subject] = number_format($this->compute_written_work_by_grade_id($grade_detail->id) + $this->compute_task_performance_by_grade_id($grade_detail->id) + $this->compute_quarterly_assessment_by_grade_id($grade_detail->id),2);
						break;
						case '2nd':
							$second_grading_total[$row->subject] = number_format($this->compute_written_work_by_grade_id($grade_detail->id) + $this->compute_task_performance_by_grade_id($grade_detail->id) + $this->compute_quarterly_assessment_by_grade_id($grade_detail->id),2);
						break;
						case '3rd':
							$third_grading_total[$row->subject] = number_format($this->compute_written_work_by_grade_id($grade_detail->id) + $this->compute_task_performance_by_grade_id($grade_detail->id) + $this->compute_quarterly_assessment_by_grade_id($grade_detail->id),2);
						break;
						case '4th':
							$fourth_grading_total[$row->subject] = number_format($this->compute_written_work_by_grade_id($grade_detail->id) + $this->compute_task_performance_by_grade_id($grade_detail->id) + $this->compute_quarterly_assessment_by_grade_id($grade_detail->id),2);
						break;
					}
				}
				$first_ave[$row->subject] = number_format(($first_grading_total[$row->subject] + $second_grading_total[$row->subject] + $third_grading_total[$row->subject] + $fourth_grading_total[$row->subject]) / 4,2);
				$tot_ave += $first_ave[$row->subject];
			}
			$ave = $tot_ave / $counter;
			$data['ave'] = $ave;
			$data['first_ave'] = $first_ave;
			$data['first_grading_total'] = $first_grading_total;
			$data['second_grading_total'] = $second_grading_total;
			$data['third_grading_total'] = $third_grading_total;
			$data['fourth_grading_total'] = $fourth_grading_total;
			$data['subject'] = $subject;
			$data['subject_id'] = $subject_id;
			$data['grade'] = $grade;
			$data['grading_total'] = $grading_total;
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	private function compute_written_work_by_grade_id($grade_id) {
		$data = $this->written_work_model->read_data_by_grade_id($grade_id);
		$score = 0;
		$perfect_score = 0;
		$ave = 0;
		foreach($data as $row) {
			$score += $row->score;
			$perfect_score += $row->perfect_score;
		}
		if($score != 0 && $perfect_score != 0) {
			$ave = $score / $perfect_score * WRITTEN_WORK_PERCENTAGE;
		}
		return $ave;
	}

	private function compute_task_performance_by_grade_id($grade_id) {
		$data = $this->task_performance_model->read_data_by_grade_id($grade_id);
		$score = 0;
		$perfect_score = 0;
		$ave = 0;
		foreach($data as $row) {
			$score += $row->score;
			$perfect_score += $row->perfect_score;
		}
		if($score != 0 && $perfect_score != 0) {
			$ave = $score / $perfect_score * TASK_PERFORMANCE_PERCENTAGE;
		}
		return $ave;
	}

	private function compute_quarterly_assessment_by_grade_id($grade_id) {
		$data = $this->quarterly_assessment_model->read_data_by_grade_id($grade_id);
		$score = 0;
		$perfect_score = 0;
		$ave = 0;
		foreach($data as $row) {
			$score += $row->score;
			$perfect_score += $row->perfect_score;
		}
		if($score != 0 && $perfect_score != 0) {
			$ave = $score / $perfect_score * QUARTERLY_PERFORMANCE_PERCENTAGE;
		}
		return $ave;
	}

	public function insert() {
		$data = new stdClass();
		$data->student_record_id = $this->input->post('student_record_id');
		$data->grade = $this->input->post('grade');
		$result = array('session' => false, 'error' => false, 'message' => '', 'success' => false);
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$result['session'] = $this->session->logged_in;
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == false) {
				$result['error'] = true;
				$result['message'] = validation_errors();
			} else {
				if($this->student_record_model->update_grade_by_student_record_id($data) == 1) {
					$result['success'] = true;
					$result['message'] = 'Grade successfully saved!';
				} else {
					$result['success'] = true;
					$result['message'] = 'No changes of grade!';
				}
			}
		}
		echo json_encode($result);
	}

	private function rules() {
		$config = array(
			array(
				'field' => 'grade',
				'label' => 'Grade',
				'rules' => 'required|numeric|greater_than_equal_to[60]|less_than_equal_to[100]'
				),
			array(
				'field' => 'student_record_id',
				'label' => 'Student record ID',
				'rules' => 'required|numeric|integer'
				)
			);
		return $config;
	}

	public function report($student_id = 0) {
		$student_record_id = 0;
		$grade = $data = array();
		$total_per_subject = 0;
		$html = $this->report_header($student_id);
		$first_grading_total = 0;
		$second_grading_total = 0;
		$third_grading_total = 0;
		$fourth_grading_total = 0;
		$counter = 0;
		$grading_total = array('1st' => 0, '2nd' => 0, '3rd' => 0, '4th' => 0);
		if($this->session->has_userdata('logged_in') && ($this->session->usertype == TEACHER || $this->session->usertype == PARENTS) && $student_id != 0 && $this->student_record_model->count_data_by_student_id($student_id) != 0) {
			foreach($this->student_record_model->read_student_record_id_by_student_id($student_id) as $row) {
				++$counter;
				$student_record_id = $row->id;
				foreach($this->grade_model->read_data_by_student_record_id($student_record_id) as $grade_data) {
					$grade[$grade_data->grading_type][$row->id]['subject'] = $row->name;
					$total_per_subject = number_format($this->compute_written_work_by_grade_id($grade_data->id) + $this->compute_task_performance_by_grade_id($grade_data->id) + $this->compute_quarterly_assessment_by_grade_id($grade_data->id), 2);
					$grade[$grade_data->grading_type][$row->id]['total_per_subject'] = $total_per_subject;
					switch($grade_data->grading_type) {
						case '1st':
							$first_grading_total += $total_per_subject;
						break;
						case '2nd':
							$second_grading_total += $total_per_subject;
						break;
						case '3rd':
							$third_grading_total += $total_per_subject;
						break;
						case '4th':
							$fourth_grading_total += $total_per_subject;
						break;
					}
				}
			}
			$grading_total['1st'] = number_format($first_grading_total / $counter, 2);
			$grading_total['2nd'] = number_format($second_grading_total / $counter, 2);
			$grading_total['3rd'] = number_format($third_grading_total / $counter, 2);
			$grading_total['4th'] = number_format($fourth_grading_total / $counter, 2);
			$gen_ave = ($grading_total['1st'] + $grading_total['2nd'] + $grading_total['3rd'] + $grading_total['4th']) / GRADING_COUNT;
			$data['grade'] = $grade;
			$data['grading_total'] = $grading_total;
			$html .= '<p style="font-weight:bold; text-align:center; font-size: 24px;">Report Card</p>';
			foreach(GRADING_TYPE as $grading_type) {
				$data['type'] = $grading_type;
				$html .= $this->load->view('report/index', $data, true);
			}
			$html .= '<span style="font-weight: bold; float: right;">General Average: ' . number_format($gen_ave,2) . '</span>';
			$html .= '<div style="clear:both;"></div>';
			$this->load->file("dompdf/autoload.inc.php");
			$dompdf = new Dompdf();
			$dompdf->loadHtml($html);
			$dompdf->setPaper('A4', 'landscape');
			$dompdf->render();
			$dompdf->stream(time());
		} else {
			redirect();
		}
	}

	private function report_header($student_id) {
		$data = $this->student_model->read_data_by_student_id($student_id);
		$html = '<p style="text-align: left;"><strong>Name: ' . $data->fname . ' ' . $data->mi . '. ' . $data->lname . '</strong><br>';
		$html .= '<strong>Level: ' . $data->year . '</strong><br>';
		$html .= '<strong>Teacher: ' . $this->user_model->read_name_by_user_id($this->session->id) . '</strong></p>';
		return $html;
	}

	public function parent_report($student_id = 0, $submit = false) {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == PARENTS) {
			$counter = 0;
			$data['count_student'] = $this->student_model->count_school_year_by_student_id($student_id);
			$data['student'] = array();
			$data['title'] = 'Student';
			$data['contents'] = '';
			$data['student_id'] = $student_id;
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = ' class="active-nav"';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['selected']['report'] = '';
			$data['selected']['year'] = '';
			$data['selected']['calendar_event'] = '';
			$data['selected']['concern'] = '';
			$data['side_nav'] = 'users/parent/side-nav';
			$data['page'] = 'grade/report';
			$data['content'] = 'users/index';
			if($submit) {
				if(!$student_id) {
					$data['contents'] = '<div class="row"><div class="col-lg-3 col-lg-offset-5"><strong class="text-danger" style="text-align:center; font-size: 18px">Student not found!</strong></div></div>';
				} else {
					if($this->student_model->count_school_year_by_student_id($student_id) > 0) {
						$data['student'] = $this->student_model->read_data_by_student_id($student_id);
					}
				}
			}
			
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function search() {
		$student_id = 0;
		$submit = true;
		if($this->session->has_userdata('logged_in') && $this->session->usertype == PARENTS) {
			if($this->student_model->count_data_by_ref_no_and_parent() == 1) {
				$student_id = $this->student_model->read_student_id_by_ref_no();
				$this->parent_report($student_id, $submit);
			} else {
				$this->parent_report($student_id, $submit);
			}
		} else {
			redirect();
		}
	}
}