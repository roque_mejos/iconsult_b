<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('usertype_lib', array());
		$this->load->library('user_lib', array());
		$this->load->library('gender_lib', array());

		$this->load->model('usertype_model');
		$this->load->model('user_model');
		$this->load->model('gender_model');
	}

	public function delete_user() {
		$result = array('message' => '');
		$user_id = $this->input->post('user_id');
		if($this->session->has_userdata('logged_in') && $this->session->usertype == ADMIN) {
			if($this->user_model->delete_data_by_user_id($user_id) > 0) {
				$result['message'] = 'Data successfully deleted.';
			} else {
				$result['message'] = 'Failed to delete user.';
			}
		} else {
			$result['message'] = 'Session is already been expired.';
		}
		echo json_encode($result);
	}

	public function detail($user_id = 0) {
		if($this->session->has_userdata('logged_in') && $user_id != 0 && $this->session->usertype == ADMIN) {
			$gender = $this->gender_model->read_data();
			$user = $this->user_detail($user_id);
			$gender_set_value = set_value('gender') ? set_value('gender') : $user->gender;
			$data['success'] = '';
			$data['fail'] = '';
			$data['title'] = 'User Details';
			$data['selected']['manage'] = ' class="active-nav"';
			$data['selected']['dashboard'] = '';
			$data['selected']['attendance'] = '';
			$data['side_nav'] = 'users/admin/side-nav';
			$data['page'] = 'users/admin/user-detail';
			$data['gender'] = $gender;
			$data['gender_selected'] = $this->gender_selected($gender, $gender_set_value);
			$data['user'] = $user;
			$data = array_merge($data, $this->update_form_error($user));
			if($this->session->has_userdata('success')) {
				$data['success'] = '<p class="alert alert-success text-left"><strong>' . $this->session->flashdata('success') . '</strong></p>';
			}
			if($this->session->has_userdata('fail')) {
				$data['fail'] = '<p class="alert alert-danger text-left"><strong>' . $this->session->flashdata('fail') . '</strong></p>';
			}
			$data['content'] = 'users/index';
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	private function gender_selected($gender, $gender_set_value) {
		$selected = array();
		foreach($gender as $row) {
			if($row->id != $gender_set_value) {
				$selected[$row->id] = '';
			} else {
				$selected[$row->id] = 'selected="selected"';
			}
		}
		return $selected;
	}

	private function usertype_selected($usertype, $usertype_set_value) {
		$selected = array();
		foreach($usertype as $row) {
			if($row->id != $usertype_set_value) {
				$selected[$row->id] = '';
			} else {
				$selected[$row->id] = 'selected="selected"';
			}
		}
		return $selected;
	}

	private function user_detail($user_id) {
		$user = new stdClass();
		$user->id = $user->fname = $user->lname = $user->mi = $user->username = $user->gender = $user->mobile = $user->address = $user->bdate = $user->usertype_name = '';
		if($this->user_model->count_data_by_user_id($user_id) > 0) {
			$row = $this->user_model->read_data_by_user_id($user_id);
			$user->id = $row->id;
			$user->fname = $row->fname;
			$user->lname = $row->lname;
			$user->mi = $row->mi;
			$user->username = $row->username;
			$user->gender = $row->gender;
			$user->mobile = $row->mobile;
			$user->address = $row->address;
			$user->usertype_name = $row->usertype_name;
			$user->bdate = date('m/d/Y', strtotime($row->bdate));
		}
		return $user;
	}

	private function update_form_error($user) {
		$data = array();
		// fname
		$data['fname_has_error'] = form_error('fname') ? ' has-error' : '';
		$data['fname_error'] = form_error('fname') ? form_error('fname', '<span class="text-danger">', '</span>') : '';
		$data['fname_set_value'] = set_value('fname') ? set_value('fname') : $user->fname;
		// lname
		$data['lname_has_error'] = form_error('lname') ? ' has-error' : '';
		$data['lname_error'] = form_error('lname') ? form_error('lname', '<span class="text-danger">', '</span>') : '';
		$data['lname_set_value'] = set_value('lname') ? set_value('lname') : $user->lname;
		// mi
		$data['mi_has_error'] = form_error('mi') ? ' has-error' : '';
		$data['mi_error'] = form_error('mi') ? form_error('mi', '<span class="text-danger">', '</span>') : '';
		$data['mi_set_value'] = set_value('mi') ? set_value('mi') : $user->mi;
		// username
		$data['username_has_error'] = form_error('username') ? ' has-error' : '';
		$data['username_error'] = form_error('username') ? form_error('username', '<span class="text-danger">', '</span>') : '';
		$data['username_set_value'] = set_value('username') ? set_value('username') : $user->username;
		// bdate
		$data['bdate_has_error'] = form_error('bdate') ? ' has-error' : '';
		$data['bdate_error'] = form_error('bdate') ? form_error('bdate', '<span class="text-danger">', '</span>') : '';
		$data['bdate_set_value'] = set_value('bdate') ? set_value('bdate') : $user->bdate;
		// gender
		$data['gender_has_error'] = form_error('gender') ? ' has-error' : '';
		$data['gender_error'] = form_error('gender') ? form_error('gender', '<span class="text-danger">', '</span>') : '';
		// mobile
		$data['mobile_has_error'] = form_error('mobile') ? ' has-error' : '';
		$data['mobile_error'] = form_error('mobile') ? form_error('mobile', '<span class="text-danger">', '</span>') : '';
		$data['mobile_set_value'] = set_value('mobile') ? set_value('mobile') : $user->mobile;
		// address
		$data['address_has_error'] = form_error('address') ? ' has-error' : '';
		$data['address_error'] = form_error('address') ? form_error('address', '<span class="text-danger">', '</span>') : '';
		$data['address_set_value'] = set_value('address') ? set_value('address') : $user->address;

		return $data;
	}

	public function update() {
		$data = new stdClass();
		if($this->session->has_userdata('logged_in') && $this->session->usertype == ADMIN) {
			$user_id = $this->input->post('user_id');
			$data->fname = $this->input->post('fname');
			$data->lname = $this->input->post('lname');
			$data->mi = $this->input->post('mi');
			$data->username = $this->input->post('username');
			$data->bdate = date('Y-m-d', strtotime($this->input->post('bdate')));
			$data->gender = $this->input->post('gender');
			$data->mobile = $this->input->post('mobile');
			$data->address = $this->input->post('address');
			if(!$user_id) {
				redirect();
			} else {
				$this->form_validation->set_rules($this->rules());
				if($this->form_validation->run() == false) {
					$this->detail($user_id);
				} else {
					if($this->user_model->update_data_by_user_id($user_id, $data) != 1) {
						$this->session->set_flashdata('fail', 'No changes of data!');
					} else {
						$this->session->set_flashdata('success', 'Successfully updated!');
					}
					redirect('user/detail/' . $user_id);
				}
			}
		} else {
			redirect();
		}
	}

	private function rules() {
		$config = array(
			array(
				'field' => 'fname',
				'label' => 'Firstname',
				'rules' => 'required'
				),
			array(
				'field' => 'lname',
				'label' => 'Lastname',
				'rules' => 'required'
				),
			array(
				'field' => 'mi',
				'label' => 'Middle Initial',
				'rules' => 'required|exact_length[1]'
				),
			array(
				'field' => 'bdate',
				'label' => 'Birthdate',
				'rules' => 'required|callback_check_bdate'
				),
			array(
				'field' => 'gender',
				'label' => 'Gender',
				'rules' => 'required|in_list['. MALE .','. FEMALE .']'
				),
			array(
				'field' => 'mobile',
				'label' => 'Mobile Number',
				'rules' => 'required|min_length[11]|max_length[20]'
				),
			array(
				'field' => 'address',
				'label' => 'Address',
				'rules' => 'required'
				)
			);

			if($this->input->post('user_id')) {
				$config[] = array(
					'field' => 'username',
					'label' => 'Username',
					'rules' => 'required|valid_email|callback_check_username'
					);
			} else {
				$config[] = array(
					'field' => 'username',
					'label' => 'Username',
					'rules' => 'required|valid_email|is_unique['. USERS .'.username]'
					);
				$config[] = array(
					'field' => 'usertype',
					'label' => 'Usertype',
					'rules' => 'required|in_list['. ADMIN .','. TEACHER .','. PARENTS .']'
					);
			}

		return $config;
	}

	public function check_username($username) {
		$user_id = $this->input->post('user_id');
		if($this->user_model->count_data_by_username_and_not_user_id($username, $user_id) > 0) {
			$this->form_validation->set_message('check_username', '%s is already exist.');
			return false;
		}
		return true;
	}

	public function check_bdate($bdate) {
		if(!date_valid_format($bdate)) {
			$this->form_validation->set_message('check_bdate', 'Invalid %s format.');
			return false;
		}
		if(age($bdate) < 1) {
			$this->form_validation->set_message('check_bdate', 'Invalid year.');
			return false;
		}
		return true;
	}

	public function create() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == ADMIN) {
			$usertype = $this->usertype_model->read_data();
			$gender = $this->gender_model->read_data();
			$gender_set_value = set_value('gender') ? set_value('gender') : '';
			$usertype_set_value = set_value('usertype') ? set_value('usertype') : '';
			$data['usertype'] = $usertype;
			$data['gender'] = $gender;
			$data['gender_selected'] = $this->gender_selected($gender, $gender_set_value);
			$data['usertype_selected'] = $this->usertype_selected($usertype, $usertype_set_value);
			$data['success'] = '';
			$data['fail'] = '';
			$data['title'] = 'Create';
			$data['selected']['manage'] = ' class="active-nav"';
			$data['selected']['dashboard'] = '';
			$data['side_nav'] = 'users/admin/side-nav';
			$data['page'] = 'users/admin/create';
			$data['content'] = 'users/index';
			$data = array_merge($data, $this->create_form_error());
			if($this->session->has_userdata('success')) {
				$data['success'] = '<p class="alert alert-success text-left"><strong>' . $this->session->flashdata('success') . '</strong></p>';
			}
			if($this->session->has_userdata('fail')) {
				$data['fail'] = '<p class="alert alert-danger text-left"><strong>' . $this->session->flashdata('fail') . '</strong></p>';
			}
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function create_form_error() {
		$data = array();
		// fname
		$data['fname_has_error'] = form_error('fname') ? ' has-error' : '';
		$data['fname_error'] = form_error('fname') ? form_error('fname', '<span class="text-danger">', '</span>') : '';
		// lname
		$data['lname_has_error'] = form_error('lname') ? ' has-error' : '';
		$data['lname_error'] = form_error('lname') ? form_error('lname', '<span class="text-danger">', '</span>') : '';
		// mi
		$data['mi_has_error'] = form_error('mi') ? ' has-error' : '';
		$data['mi_error'] = form_error('mi') ? form_error('mi', '<span class="text-danger">', '</span>') : '';
		// username
		$data['usertype_has_error'] = form_error('usertype') ? ' has-error' : '';
		$data['usertype_error'] = form_error('usertype') ? form_error('usertype', '<span class="text-danger">', '</span>') : '';
		// usertype
		$data['username_has_error'] = form_error('username') ? ' has-error' : '';
		$data['username_error'] = form_error('username') ? form_error('username', '<span class="text-danger">', '</span>') : '';
		// bdate
		$data['bdate_has_error'] = form_error('bdate') ? ' has-error' : '';
		$data['bdate_error'] = form_error('bdate') ? form_error('bdate', '<span class="text-danger">', '</span>') : '';
		// gender
		$data['gender_has_error'] = form_error('gender') ? ' has-error' : '';
		$data['gender_error'] = form_error('gender') ? form_error('gender', '<span class="text-danger">', '</span>') : '';
		// mobile
		$data['mobile_has_error'] = form_error('mobile') ? ' has-error' : '';
		$data['mobile_error'] = form_error('mobile') ? form_error('mobile', '<span class="text-danger">', '</span>') : '';
		// address
		$data['address_has_error'] = form_error('address') ? ' has-error' : '';
		$data['address_error'] = form_error('address') ? form_error('address', '<span class="text-danger">', '</span>') : '';

		return $data;
	}

	public function insert() {
		$data = new stdClass();
		if($this->session->has_userdata('logged_in') && $this->session->usertype == ADMIN) {
			$data->fname = $this->input->post('fname');
			$data->lname = $this->input->post('lname');
			$data->mi = $this->input->post('mi');
			$data->username = $this->input->post('username');
			$data->usertype = $this->input->post('usertype');
			$data->bdate = date('Y-m-d', strtotime($this->input->post('bdate')));
			$data->gender = $this->input->post('gender');
			$data->mobile = $this->input->post('mobile');
			$data->address = $this->input->post('address');
			$data->password = md5(DEFAULT_PASSWORD);
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == false) {
				$this->create();
			} else {
				if($this->user_model->insert_data($data) != 1) {
					$this->session->set_flashdata('fail', 'Failed to saved data!');
				} else {
					$this->session->set_flashdata('success', 'Successfully saved!');
				}
				redirect('user/create');
			}
		} else {
			redirect();
		}
	}
}