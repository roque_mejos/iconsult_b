<?php
class Concern extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('student_lib', array());
		$this->load->library('user_lib', array());
		$this->load->model('student_model');
		$this->load->model('user_model');
		$this->load->model('chat_model');
	}

	public function index() {
		if($this->session->has_userdata('logged_in') && ($this->session->usertype == PARENTS || $this->session->usertype == TEACHER)) {
			$data['page'] = 'concern/index';
			$data['title'] = 'Concern';
			if($this->session->usertype != TEACHER) {
				$data['selected']['concern'] = ' class="active-nav"';
				$data['selected']['dashboard'] = '';
				$data['selected']['grade'] = '';
				$data['side_nav'] = 'users/parent/side-nav';
				$data['teacher'] = $this->student_model->read_teacher_by_parent($this->session->id);
			} else {
				$data['selected']['student'] = '';
				$data['selected']['grade'] = '';
				$data['selected']['dashboard'] = '';
				$data['selected']['subject'] = '';
				$data['selected']['attendance'] = '';
				$data['selected']['report'] = '';
				$data['selected']['concern'] = ' class="active-nav"';
				$data['selected']['year'] = '';
				$data['selected']['calendar_event'] = '';
				$data['selected']['announcement'] = '';
				$data['side_nav'] = 'users/teacher/side-nav';
				$data['teacher'] = $this->student_model->read_parent_by_teacher($this->session->id);
			}
			$data['content'] = 'users/index';
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}


	public function get_message() {
		$result = array('session' => false, 'current_user' => 0, 'chat' => array(), 'time_history' => array());
		if($this->session->has_userdata('logged_in') && ($this->session->usertype == PARENTS || $this->session->usertype == TEACHER)) {
			$time_history = array();
			$current_user = $this->session->id;
			$user_id = $this->input->post('user_id');
			$result['session'] = true;
			$result['current_user'] = $current_user;
			$result['user_id'] = $user_id;
			$result['chat'] = $this->chat_model->read_data_by_teacher_and_parent($current_user, $user_id);
			foreach($result['chat'] as $row) {
				$time_history[] = date('M d, Y h:i:s A', strtotime($row->created_at));
			}
			$result['time_history'] = $time_history;
		}
		echo json_encode($result);
	}

	public function save() {
		$result = array('session' => false, 'error' => false, 'chat' => array(), 'time_history' => array());
		if($this->session->has_userdata('logged_in') && ($this->session->usertype == PARENTS || $this->session->usertype == TEACHER)) {
			$result['session'] = true;
			$this->form_validation->set_rules('receiver', 'Receiver', 'required|integer');
			$this->form_validation->set_rules('message', 'Message', 'required|trim');
			$current_user = $this->session->id;
			$result['current_user'] = $current_user;
			if($this->form_validation->run() == false) {
				$result['error'] = true;
				$result['message'] = validation_errors();
			} else {
				if($this->chat_model->insert_data() == 1) {
					$result['chat'] = $this->chat_model->read_data_by_teacher_and_parent($current_user, $this->input->post('receiver'));
					foreach($result['chat'] as $row) {
						$time_history[] = date('M d, Y h:i:s A', strtotime($row->created_at));
					}
					$result['time_history'] = $time_history;
				} else {
					$result['error'] = true;
					$result['message'] = 'Message Failed.';
				}
			}
		}
		echo json_encode($result);
	}

	public function check_message() {
		$result = array('session' => false, 'count' => 0, 'chat' => array(), 'time_history' => array());
		if($this->session->has_userdata('logged_in') && ($this->session->usertype == PARENTS || $this->session->usertype == TEACHER)) {
			$time_history = array();
			$result['session'] = true;
			$receiver = $this->input->post('receiver');
			$sender = $this->session->id;
			$result['current_user'] = $sender;
			$result['chat'] = $this->chat_model->read_data_by_teacher_and_parent($sender, $receiver);
			$result['count'] = count($this->chat_model->read_data_by_teacher_and_parent($sender, $receiver));
			foreach($result['chat'] as $row) {
				$time_history[] = date('M d, Y h:i:s A', strtotime($row->created_at));
			}
			$result['time_history'] = $time_history;
		}
		echo json_encode($result);
	}
}