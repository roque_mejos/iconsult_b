<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('announcement_lib', array());

		$this->load->model('announcement_model');
	}

	public function create() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['title'] = 'Announcement';
			$data['selected']['dashboard'] = '';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['report'] = '';
			$data['selected']['year'] = '';
			$data['selected']['concern'] = '';
			$data['selected']['calendar_event'] = '';
			$data['selected']['announcement'] = ' class="active-nav"';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'announcement/create';
			$data['content'] = 'users/index';
			$data['message'] = $this->session->flashdata('message');
			$data = array_merge($data, $this->create_form_error());
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function insert() {
		$data = new stdClass();
		$data->title = $this->input->post('title');
		$data->message = $this->input->post('message');
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == false) {
				$this->create();
			} else {
				if($this->announcement_model->insert_data($data) == 1) {
					$this->session->set_flashdata('message', '<p class="alert alert-success">Announcement successfully created.</p>');
				} else {
					$this->session->set_flashdata('message', '<p class="alert alert-danger">Failed to create announcement.</p>');
				}
				redirect('announcement/insert');
			}
		} else {
			redirect();
		}
	}

	public function create_form_error() {
		$data['title_has_error'] = form_error('title') ? ' has-error' : '';
		$data['title_error'] = form_error('title') ? form_error('title', '<p class="text-danger">', '</p>') : '';
		$data['message_has_error'] = form_error('message') ? ' has-error' : '';
		$data['message_error'] = form_error('message') ? form_error('message', '<p class="text-danger">', '</p>') : '';
		return $data;
	}

	private function rules() {
		$config = array(
			array(
				'field' => 'title',
				'label' => 'Title',
				'rules' => 'required'
				),
			array(
				'field' => 'message',
				'label' => 'Message',
				'rules' => 'required'
				)
			);
		if($this->input->post('announcement_id')) {
			$config[] = array(
				'field' => 'announcement_id',
				'label' => 'Announcement ID',
				'rules' => 'required|integer|numeric'
				);
		}
		return $config;
	}

	public function action() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['announcement'] = $this->announcement_model->read_data();
			$data['title'] = 'Announcement';
			$data['selected']['student'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['dashboard'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['report'] = '';
			$data['selected']['announcement'] = ' class="active-nav"';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'announcement/action';
			$data['selected']['year'] = '';
			$data['selected']['calendar_event'] = '';
			$data['selected']['concern'] = '';
			$data['content'] = 'users/index';
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function delete() {
		$result = array('message' => '');
		$announcement_id = $this->input->post('announcement_id');
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			if($this->announcement_model->delete_data_by_announcement_id($announcement_id) == 1) {
				$result['message'] = 'Data successfully deleted.';
			} else {
				$result['message'] = 'Failed to delete announcement.';
			}
		} else {
			$result['message'] = 'Session is already been expired.';
		}
		echo json_encode($result);
	}

	public function detail($announcement_id = 0) {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER && $announcement_id != 0 && $this->announcement_model->count_data_by_announcement_id($announcement_id)) {
			$announcement = $this->announcement_model->read_data_by_announcement_id($announcement_id);
			$data['announcement_id'] = $announcement_id;
			$data['title'] = 'Detail';
			$data['selected']['student'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['dashboard'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['report'] = '';
			$data['selected']['concern'] = '';
			$data['selected']['year'] = '';
			$data['selected']['calendar_event'] = '';
			$data['selected']['announcement'] = ' class="active-nav"';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'announcement/detail';
			$data['content'] = 'users/index';
			$data['message'] = $this->session->flashdata('message');
			$data = array_merge($data, $this->update_form_error($announcement));
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function update_form_error($announcement) {
		// title
		$data['title_has_error'] = form_error('title') ? ' has-error' : '';
		$data['title_error'] = form_error('title') ? form_error('title', '<p class="text-danger">', '</p>') : '';
		$data['title_set_value'] = set_value('title') ? set_value('title') : $announcement->title;
		// message
		$data['message_has_error'] = form_error('message') ? ' has-error' : '';
		$data['message_error'] = form_error('message') ? form_error('message', '<p class="text-danger">', '</p>') : '';
		$data['message_set_value'] = set_value('message') ? set_value('message') : $announcement->message;

		return $data;
	}

	public function update() {
		$data = new stdClass();
		$data->title = $this->input->post('title');
		$data->message = $this->input->post('message');
		$announcement_id = $this->input->post('announcement_id');
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == false) {
				$this->detail($announcement_id);
			} else {
				if($this->announcement_model->update_data_by_announcement_id($data, $announcement_id) == 1) {
					$this->session->set_flashdata('message', '<p class="alert alert-success">Announcement successfully updated!</p>');
				} else {
					$this->session->set_flashdata('message', '<p class="alert alert-danger">No changes of data!</p>');
				}
				redirect('announcement/detail/' . $announcement_id);
			}
		} else {
			redirect();
		}
	}
}