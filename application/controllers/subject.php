<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Subject extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('subject_lib', array());
		$this->load->model('subject_model');
	}

	public function view() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['subject'] = $this->subject_model->read_data();
			$data['title'] = 'Subject';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['subject'] = ' class="active-nav"';
			$data['selected']['student'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['selected']['report'] = '';
			$data['page'] = 'subject/view';
			$data['selected']['year'] = '';
			$data['selected']['calendar_event'] = '';
			$data['content'] = 'users/index';
			$data['selected']['concern'] = '';
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function deactivate_subject() {
		$result = array();
		$subject_id = $this->input->post('subject_id');
		$result['session'] = false;
		$result['success'] = false;
		$result['message'] = '';
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$result['session'] = true;
			$this->form_validation->set_rules('subject_id', 'Subject ID', 'required|integer|numeric');
			if($this->form_validation->run() == false) {
				$result['message'] = form_error('subject_id');
			} else {
				if($this->subject_model->deactivate_status_by_subject_id($subject_id) == 1) {
					$result['message'] = 'Subject successfully deactivated!';
					$result['success'] = true;
				} else {
					$result['message'] = 'Failed to deactivate subject!';
				}
			}
		}
		echo json_encode($result);
	}

	public function activate_subject() {
		$result = array();
		$subject_id = $this->input->post('subject_id');
		$result['session'] = false;
		$result['success'] = false;
		$result['message'] = '';
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$result['session'] = true;
			$this->form_validation->set_rules('subject_id', 'Subject ID', 'required|integer|numeric');
			if($this->form_validation->run() == false) {
				$result['message'] = form_error('subject_id');
			} else {
				if($this->subject_model->activate_status_by_subject_id($subject_id) == 1) {
					$result['message'] = 'Subject successfully activated!';
					$result['success'] = true;
				} else {
					$result['message'] = 'Failed to activate subject!';
				}
			}
		}
		echo json_encode($result);
	}

	public function create() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['subject'] = $this->subject_model->read_data();
			$data['title'] = 'Subject';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['subject'] = ' class="active-nav"';
			$data['selected']['student'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'subject/create';
			$data['selected']['report'] = '';
			$data['content'] = 'users/index';
			$data['selected']['year'] = '';
			$data['selected']['calendar_event'] = '';
			$data['selected']['concern'] = '';
			$data['subject_has_error'] = form_error('subject') ? ' has-error' : '';
			$data['subject_error'] = form_error('subject') ? form_error('subject', '<p class="text-danger">', '</p>') : '';
			$data['message'] = $this->session->flashdata('message');
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function insert() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$this->form_validation->set_rules('subject', 'Subject', 'required');
			if($this->form_validation->run() == false) {
				$this->create();
			} else {
				if($this->subject_model->insert_data($this->input->post('subject'))) {
					$this->session->set_flashdata('message', '<p class="alert alert-success">Subject successfully created!</p>');
				} else {
					$this->session->set_flashdata('message', '<p class="alert alert-danger">Failed to create subject!</p>');
				}
				redirect('subject/create');
			}
		} else {
			redirect();
		}
	}
}