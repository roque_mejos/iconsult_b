<?php
class Task_performance extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('student_record_lib', array());

		$this->load->model('task_performance_model');
		$this->load->model('student_record_model');
		$this->load->model('grade_model');
		$this->load->model('subject_model');
	}

	public function index() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$student_id = $this->input->get('student_id');
			$subject_id = $this->input->get('subject_id');
			$student_record_id = $this->student_record_model->read_student_record_id_by_subject_id_and_student_id($subject_id, $student_id);
			$grade_id = $this->grade_model->read_grade_id_by_student_record_id_and_grading_type($student_record_id);
			$data['grade_id'] = $grade_id;
			$data['student_id'] = $student_id;
			$data['subject_id'] = $subject_id;
			$data['activity'] = $this->task_performance_model->read_data_by_grade_id($grade_id);
			$data['subject'] = $this->subject_model->read_name_by_subject_id($subject_id);
			$data['grading_type'] = $this->input->get('grading_type');
			$data['title'] = 'Task Performance';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = ' class="active-nav"';
			$data['selected']['student'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['announcement'] = '';
			$data['selected']['report'] = '';
			$data['selected']['year'] = '';
			$data['selected']['calendar_event'] = '';
			$data['selected']['concern'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'activity/task';
			$data['content'] = 'users/index';
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function insert() {
		$result = array();
		$result['message'] = '';
		$result['error'] = false;
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$subject_id = $this->input->post('subject_id');
			$student_id = $this->input->post('student_id');
			$grading_type = $this->input->post('grading_type');
			$this->form_validation->set_rules('title','Title','required');
			$this->form_validation->set_rules('score','Score','required|numeric|callback_check_score');
			$this->form_validation->set_rules('perfect_score','Perfect Score','required|numeric');
			$this->form_validation->set_rules('date','Date','required|callback_check_date');
			$this->form_validation->set_rules('grading_type','Grading Type','required|in_list[1st,2nd,3rd,4th]');
			if($this->form_validation->run() == false) {
				$result['message'] = validation_errors();
				$result['error'] = true;
			} else {
				if($this->task_performance_model->insert_data()) {
					$student_record_id = $this->student_record_model->read_student_record_id_by_subject_id_and_student_id($subject_id, $student_id);
					$grade_id = $this->grade_model->read_grade_id_by_student_record_id_and_grading_type($student_record_id, $grading_type);
					$result['grade_id'] = $grade_id;
					$result['data'] = $this->task_performance_model->read_data_by_grade_id($grade_id);
					$result['message'] = 'Written works successfully created!';
				} else {
					$result['error'] = true;
					$result['message'] = 'Failed to insert data';
				}
			}
		}
		echo json_encode($result);
	}

	public function check_score($score) {
		$perfect_score = $this->input->post('perfect_score');
		if($score > $perfect_score) {
			$this->form_validation->set_message('check_score','The %s should not greater than perfect score.');
			return false;
		}
		return true;
	}

	public function check_date($date) {
		if(!date_valid_format($date)) {
			$this->form_validation->set_message('check_date', 'Invalid %s format.');
			return false;
		}
		return true;
	}

	public function detail(){
		$result = array();
		$result['session'] = false;
		$result['error'] = false;
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$result['session'] = true;
			if($this->task_performance_model->count_data_by_written_work_id()) {
				$data = $this->task_performance_model->read_data_by_written_work_id();
				$result['title'] = $data->title;
				$result['score'] = $data->score;
				$result['perfect_score'] = $data->perfect_score;
				$result['id'] = $data->id;
				$result['date'] = date('m/d/Y',strtotime($data->date));
			}
		}
		echo json_encode($result);
	}

	public function update() {
		$result = array();
		$result['message'] = '';
		$result['error'] = false;
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$subject_id = $this->input->post('subject_id');
			$student_id = $this->input->post('student_id');
			$grading_type = $this->input->post('grading_type');
			$this->form_validation->set_rules('id','ID','required|numeric');
			$this->form_validation->set_rules('title','Title','required');
			$this->form_validation->set_rules('score','Score','required|numeric|callback_check_score');
			$this->form_validation->set_rules('perfect_score','Perfect Score','required|numeric');
			$this->form_validation->set_rules('date','Date','required|callback_check_date');
			$this->form_validation->set_rules('grading_type','Grading Type','required|in_list[1st,2nd,3rd,4th]');
			if($this->form_validation->run() == false) {
				$result['message'] = validation_errors();
				$result['error'] = true;
			} else {
				if($this->task_performance_model->update_data_by_written_work_id()) {
					$student_record_id = $this->student_record_model->read_student_record_id_by_subject_id_and_student_id($subject_id, $student_id);
					$grade_id = $this->grade_model->read_grade_id_by_student_record_id_and_grading_type($student_record_id, $grading_type);
					$result['grade_id'] = $grade_id;
					$result['data'] = $this->task_performance_model->read_data_by_grade_id($grade_id);
					$result['message'] = 'Task performance successfully updated!';
				} else {
					$result['error'] = true;
					$result['message'] = 'No data is updated!';
				}
			}
		}
		echo json_encode($result);
	}

	public function delete() {
		$result = array();
		$result['message'] = '';
		$result['error'] = false;
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$subject_id = $this->input->post('subject_id');
			$student_id = $this->input->post('student_id');
			$grading_type = $this->input->post('grading_type');
			if($this->task_performance_model->delete_data_by_written_work_id()) {
				$student_record_id = $this->student_record_model->read_student_record_id_by_subject_id_and_student_id($subject_id, $student_id);
				$grade_id = $this->grade_model->read_grade_id_by_student_record_id_and_grading_type($student_record_id, $grading_type);
				$result['grade_id'] = $grade_id;
				$result['data'] = $this->task_performance_model->read_data_by_grade_id($grade_id);
				$result['message'] = 'Data successfully deleted!';
			} else {
				$result['message'] = 'Failed to delete the data!';
				$result['error'] = false;
			}
		}
		echo json_encode($result);
	}

	public function search_grading_type() {
		$result = array('message' => '', 'session' => false, 'data' => array(), 'grade_id' => 0);
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$result['session'] = true;
			$subject_id = $this->input->post('subject_id');
			$student_id = $this->input->post('student_id');
			$grading_type = $this->input->post('grading_type');
			$student_record_id = $this->student_record_model->read_student_record_id_by_subject_id_and_student_id($subject_id, $student_id);
			$grade_id = $this->grade_model->read_grade_id_by_student_record_id_and_grading_type($student_record_id, $grading_type);
			$data = $this->task_performance_model->read_data_by_grade_id($grade_id);
			$result['data'] = $data;
			$result['grade_id'] = $grade_id;
		}
		echo json_encode($result);
	}
}