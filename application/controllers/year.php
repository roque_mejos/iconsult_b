<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Year extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('year_lib', array());
		$this->load->model('year_model');
		$this->load->model('section_model');
	}

	public function index() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['title'] = 'Year';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['selected']['report'] = '';
			$data['selected']['year'] = ' class="active-nav"';
			$data['selected']['calendar_event'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'year/index';
			$data['selected']['concern'] = '';
			$data['content'] = 'users/index';
			$data['message'] = $this->session->flashdata('message');
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function insert() {
		$insert_id = 0;
		$result = array('session' => false, 'success' => false, 'error' => false, 'message' => '');
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$result['session'] = true;
			$this->form_validation->set_rules('year','Grade Level','required|is_unique[years.name]');
			$this->form_validation->set_rules('section[]','Section','required|is_unique[sections.name]|callback_check_section');
			if($this->form_validation->run() == false) {
				$result['error'] = true;
				$result['message'] = validation_errors();
			} else {
				$insert_id = $this->year_model->insert_data();
				if($insert_id > 0) {
					if($this->section_model->insert_data($insert_id) > 0) {
						$result['success'] = true;
						$result['message'] = 'Year Successfully created!';
					} else {
						$result['error'] = true;
						$result['message'] = 'failed to created section';
					}
				} else {
					$result['error'] = true;
					$result['message'] = 'failed to create year';
				}
			}
		}
		echo json_encode($result);
	}

	public function action() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['title'] = 'Year';
			$data['year'] = $this->year_model->read_data();
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['selected']['year'] = ' class="active-nav"';
			$data['selected']['calendar_event'] = '';
			$data['selected']['report'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'year/action';
			$data['content'] = 'users/index';
			$data['selected']['concern'] = '';
			$data['message'] = $this->session->flashdata('message');
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function detail($year_id = 0) {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER && $this->year_model->count_data_by_year_id($year_id) > 0) {
			$year = $this->year_model->read_data_by_year_id($year_id);
			$data['year_id'] = $year_id;
			$data['year'] = $year->name;
			$data['section'] = $this->section_model->read_data_by_year_id($year_id);
			$data['title'] = 'Year';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['selected']['report'] = '';
			$data['selected']['year'] = ' class="active-nav"';
			$data['selected']['calendar_event'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'year/detail';
			$data['content'] = 'users/index';
			$data['selected']['concern'] = '';
			$data['message'] = $this->session->flashdata('message');
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function update() {
		$flag = false;
		$insert_id = 0;
		$result = array('session' => false, 'success' => false, 'error' => false, 'message' => '');
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$result['session'] = true;
			$this->form_validation->set_rules('year','Grade Level','required|callback_check_year');
			$this->form_validation->set_rules('section[]','Section','required|callback_check_section');
			if($this->form_validation->run() == false) {
				$result['error'] = true;
				$result['message'] = validation_errors();
			} else {
				$this->section_model->delete_data_year_id();
				$insert_id = $this->section_model->insert_data($this->input->post('year_id'));
				if($insert_id > 0) {
					$flag = true;
				} else {
					$result['error'] = true;
					$result['message'] = 'No data changes!';
				}

				if($this->year_model->update_data_by_year_id() > 0) {
					$flag = true;
				}

				if($flag) {
					$result['success'] = true;
					$result['message'] = 'Data Successfully Updated';
				}
				
			}
		}
		echo json_encode($result);
	}

	function array_has_dupes($array) {
	   return count($array) !== count(array_unique($array));
	}

	public function check_section() {
		$section = $this->input->post('section');
		$year_id = $this->input->post('year_id');
		$flag = false;
		foreach($section as $row) {
			$count = $this->section_model->read_data_not_by_year_id_and_by_name($row, $year_id);
			if($count == 1) {
				$flag = true;
				break;
			}
		}
		if($this->array_has_dupes($section)) {
			$this->form_validation->set_message('check_section','The %s must contain a unique value.');
			return false;
		}

		if($flag) {
			$this->form_validation->set_message('check_section','The %s is already exist!');
			return false;
		}

		return true;
	}

	public function check_year($year) {
		$year_id = $this->input->post('year_id');
		$count = $this->year_model->read_data_by_year_id_and_not_by_name($year_id);
		if($count > 0) {
			$this->form_validation->set_message('check_year', 'The %s is already exist!');
			return false;
		}
		return true;
	}

	public function delete($year_id = 0) {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER && $this->year_model->count_data_by_year_id($year_id) > 0) {
			$this->section_model->delete_data_year_id($year_id);
			$this->year_model->delete_data_by_year_id($year_id);
			redirect('year/action');
		} else {
			redirect();
		}
	}
}