<?php
use Dompdf\Dompdf;
class Attendance extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('student_record_lib', array());
		$this->load->library('student_lib', array());
		$this->load->library('user_lib', array());
		$this->load->library('year_lib', array());

		$this->load->model('student_record_model');
		$this->load->model('student_model');
		$this->load->model('attendance_model');
		$this->load->model('user_model');
		$this->load->model('year_model');
		$this->load->model('section_model');
		$this->load->model('school_date_model');
	}

	public function index() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['year'] = $this->year_model->read_data();
			$data['section'] = $this->section_model->read_data_by_year_id();
			$data['title'] = 'Attendance';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = ' class="active-nav"';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['report'] = '';
			$data['selected']['concern'] = '';
			$data['selected']['announcement'] = '';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'attendance/index';
			$data['content'] = 'users/index';
			$data['selected']['year'] = '';
			$data['selected']['calendar_event'] = '';

			// validation message
			$data['year_id_has_error'] = form_error('year_id') ? ' has-error' : '';
			$data['section_id_has_error'] = form_error('section_id') ? ' has-error' : '';
			$data['school_year_has_error'] = form_error('school_year') ? ' has-error' : '';
			$data['year_id_error'] = form_error('year_id') ? form_error('year_id', '<p class="text-danger">', '</p>') : '';
			$data['section_id_error'] = form_error('section_id') ? form_error('section_id', '<p class="text-danger">', '</p>') : '';
			$data['school_year_error'] = form_error('school_year') ? form_error('school_year', '<p class="text-danger">', '</p>') : '';

			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function insert() {
		$result = array('error' => false, 'message' => '');
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$this->form_validation->set_rules('date','Date','required|callback_check_date');
			$this->form_validation->set_rules('status','Status','required|in_list[present,absent]');
			$this->form_validation->set_rules('student_id','Student ID','required|numeric');
			if($this->form_validation->run() == false) {
				$result['message'] = validation_errors();
				$result['error'] = true;
			} else {
				if($this->attendance_model->insert_data()) {
					$result['message'] = 'Attendance successfully Created!';
				} else {
					$result['message'] = 'Failed to insert data';
					$result['error'] = true;
				}
			}
		}
		echo json_encode($result);
	}

	public function check_date($date) {
		if(!date_valid_format($date)) {
			$this->form_validation->set_message('check_date', 'Invalid %s format.');
			return false;
		}
		return true;
	}

	public function delete() {
		$result = array('error' => false, 'message' => '');
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			if($this->attendance_model->delete_data_by_attendance_id()) {
				$result['message'] = 'Data successfully Deleted!';
			} else {
				$result['message'] = 'Failed to delete data';
			}
		}
		echo json_encode($result);
	}

	public function report($student_id = 0) {
		$html = '';
		$data = array();
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER && $student_id != 0 && $this->student_record_model->count_data_by_student_id($student_id) != 0) {
			$data['attendance'] = $this->attendance_model->read_data_by_student_id($student_id);
			$html .= $this->report_header($student_id);
			$html .= '<p style="font-weight:bold; text-align:center; font-size: 24px;">Attendance</p>';
			$html .= $this->load->view('attendance/report', $data, true);
			$this->load->file("dompdf/autoload.inc.php");
			$dompdf = new Dompdf();
			$dompdf->loadHtml($html);
			$dompdf->setPaper('A4', 'landscape');
			$dompdf->render();
			$dompdf->stream(time());
		}
	}

	private function report_header($student_id) {
		$data = $this->student_model->read_data_by_student_id($student_id);
		$html = '<p style="text-align: left;"><strong>Name: ' . $data->fname . ' ' . $data->mi . '. ' . $data->lname . '</strong><br>';
		$html .= '<strong>Level: ' . $data->year . '</strong><br>';
		$html .= '<strong>Teacher: ' . $this->user_model->read_name_by_user_id($this->session->id) . '</strong></p>';
		return $html;
	}

	public function get_section() {
		$result = array();
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$year_id = $this->input->post('year_id');
			$result['section'] = $this->section_model->read_data_by_year_id($year_id);
		}
		echo json_encode($result);
	}

	public function filter() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$this->form_validation->set_rules('year_id', 'Year Level', 'required|numeric|integer');
			$this->form_validation->set_rules('section_id', 'Section', 'required|numeric|integer');
			$this->form_validation->set_rules('school_year', 'School Year', 'required|numeric|integer');
			if($this->form_validation->run() == false) {
				$this->index();
			} else {
				$year_id = $this->input->post('year_id');
				$section_id = $this->input->post('section_id');
				$school_year = $this->input->post('school_year');
				$this->view($year_id, $section_id, $school_year);
			}
		} else {
			redirect();
		}
	}

	private function view($year_id, $section_id, $school_year) {
		$status = array();
		$data['year_level'] = $this->year_model->read_name_by_year_id($year_id);
		$data['section'] = $this->section_model->read_name_by_section_id($section_id);
		$data['school_year'] = $school_year;
		$student = $this->student_model->read_data_by_year_id_and_section_id_and_school_year($year_id, $section_id, $school_year);
		$data['year_id'] = $year_id;
		$data['section_id'] = $section_id;
		$data['student'] = $student;
		$data['title'] = 'Attendance';
		$data['selected']['dashboard'] = '';
		$data['selected']['grade'] = '';
		$data['selected']['student'] = '';
		$data['selected']['subject'] = '';
		$data['selected']['attendance'] = '';
		$data['selected']['report'] = '';
		$data['selected']['concern'] = '';
		$data['selected']['announcement'] = '';
		$data['side_nav'] = 'users/teacher/side-nav';
		$data['page'] = 'attendance/view';
		$data['content'] = 'users/index';
		$data['selected']['year'] = '';
		$data['selected']['calendar_event'] = '';
		$school_date_id = $this->school_date_model->read_school_date_id_by_date(date('Y-m-d'));
		foreach($student as $row) {
			$status[$row->id] = $this->attendance_model->read_status_by_date_and_student_id($school_date_id, $row->id);
		}
		$data['count_school_date_id'] = $this->school_date_model->count_school_date_id_by_date(date('Y-m-d'));
		$data['status'] = $status;
		$this->load->view('index', $data);
	}

	public function check_attendance_date() {
		// init result
		$result = array('count_school_date_id' => 0);
		$status = array();

		// input
		$date = date('Y-m-d', strtotime($this->input->post('date')));
		$year_id = $this->input->post('year_id');
		$school_year = $this->input->post('school_year');
		$section_id = $this->input->post('section_id');
		$search = $this->input->post('search');

		// count
		$count_school_date_id = $this->school_date_model->count_school_date_id_by_date($date);
		$result['count_school_date_id'] = $count_school_date_id;

		$student = $this->student_model->read_data_by_year_id_and_section_id_and_school_year($year_id, $section_id, $school_year, $search);
		$result['student'] = $student;

		$school_date_id = $this->school_date_model->read_school_date_id_by_date($date);

		foreach($student as $row) {
			$status[$row->id] = $this->attendance_model->read_status_by_date_and_student_id($school_date_id, $row->id);
		}
		$result['status'] = $status;

		// output
		echo json_encode($result);
	}

	public function save() {
		$result = array('error' => false, 'message' => '', 'school_date' => 0);
		$school_date = 0;
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$date = date('Y-m-d', strtotime($this->input->post('date')));
			$this->form_validation->set_rules('date', 'Date', 'required|callback_check_date');
			if($this->form_validation->run() == false) {
				$result['error'] = true;
				$result['message'] = validation_errors();
			} else {
				$school_date = $this->school_date_model->insert_data($date);
				if(!$school_date) {
					$result['message'] = 'Failed to insert the school date';
					$result['error'] = true;
				} else {
					$result['school_date'] = $school_date;
				}
			}
		}
		echo json_encode($result);
	}

	public function insert_attendance() {
		$this->attendance_model->insert_data();
	}

	public function get_school_date_id() {
		$result = array('session' => false, 'school_date_id' => 0);
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$result['session'] = true;
			$date = date('Y-m-d', strtotime($this->input->post('date')));
			if($this->school_date_model->read_school_date_id_by_date($date) > 0) {
				$result['school_date_id'] = $this->school_date_model->read_school_date_id_by_date($date);
			}
		}
		echo json_encode($result);
	}

	public function update_attendance() {
		$school_date = $this->input->post('school_date');
		$student_id = $this->input->post('student_id');
		
		if($this->attendance_model->count_data_by_date_and_student_id($school_date, $student_id)) {
			// update
			$this->attendance_model->update_data();
		} else {
			// insert
			$this->attendance_model->insert_data();
		}
	}

	public function search() {
		$result = array('session' => false);
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$result['session'] = true;
			// input
			$date = date('Y-m-d', strtotime($this->input->post('date')));
			$year_id = $this->input->post('year_id');
			$school_year = $this->input->post('school_year');
			$section_id = $this->input->post('section_id');
			$search = $this->input->post('search');


		}
		echo json_encode($result);
	}
}