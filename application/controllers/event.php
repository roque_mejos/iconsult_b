<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('event_lib', array());
		$this->load->model('event_model');
	}

	public function index() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['title'] = 'Calendar Event';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['selected']['year'] = '';
			$data['selected']['concern'] = '';
			$data['selected']['calendar_event'] = ' class="active-nav"';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'calendar-event/index';
			$data['content'] = 'users/index';
			$data['selected']['report'] = '';
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function create() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['title'] = 'Create Event';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['selected']['report'] = '';
			$data['selected']['year'] = '';
			$data['selected']['concern'] = '';
			$data['selected']['calendar_event'] = ' class="active-nav"';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'calendar-event/create';
			$data['content'] = 'users/index';
			$data = array_merge($data, $this->create_form_error());
			if($this->session->has_userdata('success')) {
				$data['success'] = '<p class="alert alert-success text-left"><strong>' . $this->session->flashdata('success') . '</strong></p>';
			}
			if($this->session->has_userdata('fail')) {
				$data['fail'] = '<p class="alert alert-danger text-left"><strong>' . $this->session->flashdata('fail') . '</strong></p>';
			}
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	private function create_form_error() {
		$data = array();
		$data['success'] = '';
		$data['fail'] = '';
		$data['title_has_error'] = form_error('title') ? ' has-error' : '';
		$data['title_error'] = form_error('title') ? form_error('title', '<p class="text-danger">', '</p>') : '';
		$data['date_has_error'] = form_error('date') ? ' has-error' : '';
		$data['date_error'] = form_error('date') ? form_error('date', '<p class="text-danger">', '</p>') : '';
		return $data;
	}

	private function update_form_error($event) {
		$data = array();
		$data['success'] = '';
		$data['fail'] = '';
		// 
		$data['title_has_error'] = form_error('title') ? ' has-error' : '';
		$data['title_error'] = form_error('title') ? form_error('title', '<p class="text-danger">', '</p>') : '';
		$data['title_set_value'] = set_value('title') ? set_value('title') : $event->title;
		// 
		$data['date_has_error'] = form_error('date') ? ' has-error' : '';
		$data['date_error'] = form_error('date') ? form_error('date', '<p class="text-danger">', '</p>') : '';
		$data['date_set_value'] = set_value('date') ? set_value('date') : $event->date;

		return $data;
	}

	public function insert() {
		$data = new stdClass();
		$data->title = $this->input->post('title');
		$data->date = date('Y-m-d', strtotime($this->input->post('date')));
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == false) {
				$this->create();
			} else {
				if($this->event_model->insert_data($data) != 1) {
					$this->session->set_flashdata('fail', 'Failed to create event!');
				} else {
					$this->session->set_flashdata('success', 'Event successfully created!');
				}
				redirect('event/create');
			}
		} else {
			redirect();
		}
	}

	private function rules() {
		$config = array(
			array(
				'field' => 'title',
				'label' => 'Title',
				'rules' => 'required|callback_check_title'
				),
			array(
				'field' => 'date',
				'label' => 'Event Date',
				'rules' => 'required|callback_check_date'
				)
			);
		if($this->input->post('event_id')) {
			$config[] = array(
				'field' => 'event_id',
				'label' => 'Event ID',
				'rules' => 'required|integer|numeric'
				);
		}
		return $config;
	}

	public function check_date($date) {
		if(!date_valid_format($date)) {
			$this->form_validation->set_message('check_date', 'Invalid %s format.');
			return false;
		}
		if(get_number_of_day_by_date($date) > 0) {
			$this->form_validation->set_message('check_date', 'You can\'t create or update event in previous date.');
			return false;
		}
		return true;
	}

	public function check_title($title) {
		$date = date('Y-m-d', strtotime($this->input->post('date')));
		if($this->event_model->count_data_by_title_and_date($title, $date) > 0) {
			$this->form_validation->set_message('check_title', 'Event is already exist.');
			return false;
		}
		return true;
	}

	public function action() {
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$data['event'] = $this->event_model->read_data();
			$data['title'] = 'Action Event';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['selected']['report'] = '';
			$data['selected']['year'] = '';
			$data['selected']['concern'] = '';
			$data['selected']['calendar_event'] = ' class="active-nav"';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'calendar-event/action';
			$data['content'] = 'users/index';
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	public function delete() {
		$result = array('message' => '');
		$event_id = $this->input->post('event_id');
		if($this->session->has_userdata('logged_in')) {
			if($this->event_model->delete_data_by_event_id($event_id) > 0) {
				$result['message'] = 'Event successfully deleted.';
			} else {
				$result['message'] = 'Failed to delete user.';
			}
		} else {
			$result['message'] = 'Session is already been expired.';
		}
		echo json_encode($result);
	}

	public function detail($event_id = 0) {
		if($this->session->has_userdata('logged_in') && $event_id != 0 && $this->session->usertype == TEACHER) {
			$event = $this->event_detail($event_id);
			$data['event'] = $event;
			$data['title'] = 'Event Detail';
			$data['selected']['dashboard'] = '';
			$data['selected']['grade'] = '';
			$data['selected']['student'] = '';
			$data['selected']['subject'] = '';
			$data['selected']['attendance'] = '';
			$data['selected']['announcement'] = '';
			$data['selected']['report'] = '';
			$data['selected']['year'] = '';
			$data['selected']['concern'] = '';
			$data['selected']['calendar_event'] = ' class="active-nav"';
			$data['side_nav'] = 'users/teacher/side-nav';
			$data['page'] = 'calendar-event/detail';
			$data['content'] = 'users/index';
			$data = array_merge($data, $this->update_form_error($event));
			if($this->session->has_userdata('success')) {
				$data['success'] = '<p class="alert alert-success text-left"><strong>' . $this->session->flashdata('success') . '</strong></p>';
			}
			if($this->session->has_userdata('fail')) {
				$data['fail'] = '<p class="alert alert-danger text-left"><strong>' . $this->session->flashdata('fail') . '</strong></p>';
			}
			$this->load->view('index', $data);
		} else {
			redirect();
		}
	}

	private function event_detail($event_id) {
		$event = new stdClass();
		$event->id = $event->title = $event->date = '';
		if($this->event_model->count_data_by_event_id($event_id) > 0) {
			$row = $this->event_model->read_data_by_event_id($event_id);
			$event->id = $row->id;
			$event->title = $row->title;
			$event->date = date('m/d/Y', strtotime($row->date));
		}
		return $event;
	}

	public function update() {
		$data = new stdClass();
		$event_id = $this->input->post('event_id');
		$data->title = $this->input->post('title');
		$data->date = date('Y-m-d', strtotime($this->input->post('date')));
		if($this->session->has_userdata('logged_in') && $this->session->usertype == TEACHER) {
			$this->form_validation->set_rules($this->rules());
			if($this->form_validation->run() == false) {
				$this->detail($event_id);
			} else {
				if($this->event_model->update_data_by_event_id($data, $event_id) != 1) {
					$this->session->set_flashdata('fail', 'No data changes!');
				} else {
					$this->session->set_flashdata('success', 'Event successfully updated!');
				}
				redirect('event/detail/' . $event_id);
			}
		} else {
			redirect();
		}
	}
}