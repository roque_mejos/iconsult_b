<?php
class Register extends CI_Controller {

	public function __construct() {
		parent:: __construct();
		$this->load->library('user_lib', array());
		$this->load->library('gender_lib', array());
		$this->load->library('usertype_lib', array());
		$this->load->model('user_model');
		$this->load->model('gender_model');
		$this->load->model('usertype_model');
	}

	public function index() {
		$data['usertype_error'] = form_error('usertype') ? form_error('usertype', '<p class="text-danger">', '</p>') : ''; 
		$data['username_error'] = form_error('username') ? form_error('username', '<p class="text-danger">', '</p>') : ''; 
		$data['fname_error'] = form_error('fname') ? form_error('fname', '<p class="text-danger">', '</p>') : ''; 
		$data['lname_error'] = form_error('lname') ? form_error('lname', '<p class="text-danger">', '</p>') : ''; 
		$data['mi_error'] = form_error('mi') ? form_error('mi', '<p class="text-danger">', '</p>') : ''; 
		$data['bdate_error'] = form_error('bdate') ? form_error('bdate', '<p class="text-danger">', '</p>') : ''; 
		$data['gender_error'] = form_error('gender') ? form_error('gender', '<p class="text-danger">', '</p>') : ''; 
		$data['address_error'] = form_error('address') ? form_error('address', '<p class="text-danger">', '</p>') : ''; 
		$data['mobile_error'] = form_error('mobile') ? form_error('mobile', '<p class="text-danger">', '</p>') : ''; 
		$data['password_error'] = form_error('password') ? form_error('password', '<p class="text-danger">', '</p>') : ''; 
		$data['confirm_password_error'] = form_error('confirm_password') ? form_error('confirm_password', '<p class="text-danger">', '</p>') : '';
		$data['message'] = $this->session->flashdata('message');
		$data['gender'] = $this->gender_model->read_data();
		$data['usertype'] = $this->usertype_model->read_teacher_and_parent();
		$this->load->view('register/index', $data);
	}

	public function insert() {
		$this->form_validation->set_rules('usertype', 'Usertype', 'required|numeric|integer');
		$this->form_validation->set_rules('username', 'Email Address', 'required|valid_email|is_unique[users.username]');
		$this->form_validation->set_rules('fname', 'Firstname', 'required');
		$this->form_validation->set_rules('lname', 'Lastname', 'required');
		$this->form_validation->set_rules('mi', 'Middle Initial', 'required|exact_length[1]');
		$this->form_validation->set_rules('bdate', 'Birthdate', 'required|callback_check_bdate');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required|max_length[20]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[25]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]|min_length[6]|max_length[25]');
		if($this->form_validation->run() == false) {
			$this->index();
		} else {
			if($this->user_model->insert_parent() == 1) {
				$this->session->set_flashdata('message', '<p class="alert alert-success">Successfully created!</p>');
			} else {
				$this->session->set_flashdata('message', '<p class="alert alert-danger">Failed to insert the user!</p>');
			}
			redirect('register');
		}
	}

	public function check_bdate($bdate) {
		if(!date_valid_format($bdate)) {
			$this->form_validation->set_message('check_bdate', 'Invalid %s format.');
			return false;
		}
		if(age($bdate) < 1) {
			$this->form_validation->set_message('check_bdate', 'Invalid year.');
			return false;
		}
		return true;
	}
}