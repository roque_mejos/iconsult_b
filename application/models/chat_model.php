<?php
class Chat_model extends CI_Model {
	
	public function read_data_by_teacher_and_parent($sender, $receiver) {
		$sql = "
			SELECT
				`chat`.`created_at`,
				`chat`.`sender`,
				`chat`.`receiver`,
				`chat`.`message`,
				CONCAT(`r`.`fname`, ' ', `r`.`mi`, '. ', `r`.`lname`) as `receiver_name`,
				CONCAT(`s`.`fname`, ' ', `s`.`mi`, '. ', `s`.`lname`) as `sender_name`
			FROM
				`chat`
			LEFT JOIN
				`users` AS `s`
			ON
				`chat`.`sender` = `s`.`id`
			LEFT JOIN
				`users` AS `r`
			ON
				`chat`.`receiver` = `r`.`id`
			WHERE
				(`chat`.`sender` = ". $this->db->escape($sender) ." AND `chat`.`receiver` = ". $this->db->escape($receiver) .")
			OR
				(`chat`.`sender` = ". $this->db->escape($receiver) ." AND `chat`.`receiver` = ". $this->db->escape($sender) .")
			ORDER BY
				`chat`.`created_at`
			ASC
		";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function insert_data() {
		$sql = "
			INSERT INTO
				`chat`(`chat`.`message`, `chat`.`receiver`, `chat`.`sender`)
			VALUES
				(". $this->db->escape($this->input->post('message')) .", ". $this->db->escape($this->input->post('receiver')) .", ". $this->db->escape($this->session->id) .")
		";
		$this->db->query($sql);
		return $this->db->affected_rows();
	}
}