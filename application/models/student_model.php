<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends CI_Model {

	public function insert_data($data) {
		$sql = $this->student_lib->insert_data_sql($data);
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	public function read_data_by_teacher($teacher) {
		$sql = $this->student_lib->read_data_by_teacher_sql($teacher);
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_data_by_parent($parent) {
		$sql = $this->student_lib->read_data_by_parent_sql($parent);
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_data_by_student_id($student_id) {
		$sql = $this->student_lib->read_data_by_student_id_sql($student_id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	public function delete_data_by_student_id($student_id) {
		$sql = $this->student_lib->delete_data_by_student_id_sql($student_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function count_data_by_student_id($student_id) {
		$sql = $this->student_lib->count_data_by_student_id_sql($student_id);
		$query = $this->db->query($sql);
		return $query->row()->count_data;
	}

	public function update_data_by_student_id($data, $student_id) {
		$sql = $this->student_lib->update_data_by_student_id_sql($data, $student_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function read_name_by_student_id($student_id) {
		$sql = "
			SELECT
				CONCAT(`students`.`fname`, ' ', `students`.`mi`, '. ', `students`.`lname`) as `fullname`
			FROM
				`students`
			WHERE
				`students`.`id` = ". $this->db->escape($student_id) ."
		";
		$query = $this->db->query($sql);
		return $query->row()->fullname;
	}

	public function count_data_by_ref_no_and_parent() {
		$query = $this->db->where('ref_no', $this->input->post('ref_no'))->where('parent', $this->session->id)->get('students');
		return $query->num_rows();
	}

	public function read_student_id_by_ref_no() {
		$query = $this->db->where('ref_no', $this->input->post('ref_no'))->get('students');
		return $query->row()->id;
	}

	public function read_data_by_year_id_and_section_id_and_school_year($year_id, $section_id, $school_year, $search = '') {

		if(empty($search) != true || strlen($search) > 0) {
			$_search = " AND `students`.`ref_no` LIKE '" . $this->db->escape_like_str($search) . "%'";
		} else {
			$_search = '';
		}

		$sql = "
			SELECT
				`students`.*
			FROM
				`students`
			WHERE
				`students`.`year_id` = ". $this->db->escape($year_id) ."
			AND
				`students`.`section_id` = ". $this->db->escape($section_id) ."
			AND
				`students`.`school_year` = ". $this->db->escape($school_year) ."
			". $_search ."
			ORDER BY
				`students`.`lname`
			ASC
		";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_school_year_by_student_id($student_id = 0) {
		$query = $this->db->select('school_year')->where('id', $student_id)->get('students');
		return $query->row()->school_year;
	}

	public function count_school_year_by_student_id($student_id = 0) {
		$query = $this->db->where('id', $student_id)->get('students');
		return $query->num_rows();
	}

	public function read_teacher_by_parent($parent) {
		$sql = "
			SELECT
				DISTINCT `users`.`id`,
				CONCAT(`users`.`fname`, ' ', `users`.`lname`) AS `fullname`
			FROM
				`students`
			LEFT JOIN
				`users`
			ON
				`students`.`teacher` = `users`.`id`
			WHERE
				`students`.`parent` = ". $this->db->escape($parent) ."
		";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_parent_by_teacher($teacher) {
		$sql = "
			SELECT
				DISTINCT `users`.`id`,
				CONCAT(`users`.`fname`, ' ', `users`.`lname`) AS `fullname`
			FROM
				`students`
			LEFT JOIN
				`users`
			ON
				`students`.`parent` = `users`.`id`
			WHERE
				`students`.`teacher` = ". $this->db->escape($teacher) ."
		";
		$query = $this->db->query($sql);
		return $query->result();
	}
}