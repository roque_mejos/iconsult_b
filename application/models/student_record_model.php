<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Student_record_model extends CI_Model {

	public function insert_data($subject_id, $student_id) {
		$sql = $this->student_record_lib->insert_data_sql($subject_id, $student_id);
		$this->db->query($sql);
		return $this->db->insert_id();
	}

	public function read_data_by_student_id($student_id) {
		$sql = $this->student_record_lib->read_data_by_student_id_sql($student_id);
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function count_data_by_student_id($student_id) {
		$sql = $this->student_record_lib->count_data_by_student_id_sql($student_id);
		$query = $this->db->query($sql);
		return $query->row()->count_data;
	}

	public function update_grade_by_student_record_id($data) {
		$sql = $this->student_record_lib->update_grade_by_student_record_id_sql($data);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function delete_data_by_student_id($student_id) {
		$sql = $this->student_record_lib->delete_data_by_student_id_sql($student_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function read_student_record_id_by_subject_id_and_student_id($subject_id, $student_id) {
		$query = $this->db->select('id')->get_where('student_records', array('subject_id' => $subject_id, 'student_id' => $student_id));
		return $query->row()->id;
	}

	public function delete_data_by_subject_id_and_student_id($subject_id, $student_id) {
		$this->db->where('subject_id', $subject_id)->where('student_id', $student_id)->delete('student_records');
		return $this->db->affected_rows();
	}

	public function read_grade_id_by_student_id($student_id) {
		$query = $this->db->select('grade_id')->where('student_id', $student_id)->get('student_records');
		return $query->result();
	}

	public function delete_data_by_student_record_id($student_record_id) {
		$this->db->where('id', $student_record_id)->delete('student_records');
		return $this->db->affected_rows();
	}

	public function read_student_record_id_by_student_id($student_id) {
		$query = $this->db->select('student_records.id')->select('subjects.name')->where('student_id', $student_id)->join('subjects', 'subjects.id = student_records.subject_id', 'left')->get('student_records');
		return $query->result();
	}
}