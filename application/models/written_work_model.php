<?php
class Written_work_model extends CI_Model {

	public function read_data_by_grade_id($grade_id) {
		$sql = "
			SELECT
				`written_works`.`id`,
				`written_works`.`title`,
				`written_works`.`score`,
				`written_works`.`perfect_score`,
				DATE_FORMAT(`written_works`.`date`, '%m/%d/%Y') `date`
			FROM
				`written_works`
			LEFT JOIN
				`grades`
			ON
				`written_works`.`grade_id` = `grades`.`id`
			WHERE
				`written_works`.`grade_id` = ". $this->db->escape($grade_id) ."
			ORDER BY
				DATE(`written_works`.`date`)
			DESC
		";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function insert_data() {
		$data = array(
			'title' => $this->input->post('title'),
			'score' => $this->input->post('score'),
			'perfect_score' => $this->input->post('perfect_score'),
			'grade_id' => $this->input->post('grade_id'),
			'date' => date('Y-m-d',strtotime($this->input->post('date')))
			);
		$this->db->insert('written_works', $data);
		return $this->db->affected_rows();
	}

	public function count_data_by_written_work_id() {
		$query = $this->db->where('id', $this->input->post('id'))->get('written_works');
		return $query->num_rows();
	}

	public function read_data_by_written_work_id() {
		$query = $this->db->where('id', $this->input->post('id'))->get('written_works');
		return $query->row();
	}

	public function update_data_by_written_work_id() {
		$data = array(
			'title' => $this->input->post('title'),
			'score' => $this->input->post('score'),
			'perfect_score' => $this->input->post('perfect_score'),
			'date' => date('Y-m-d',strtotime($this->input->post('date')))
			);
		$this->db->where('id', $this->input->post('id'))->update('written_works', $data);
		return $this->db->affected_rows();
	}

	public function delete_data_by_written_work_id() {
		$this->db->where('id', $this->input->post('id'))->delete('written_works');
		return $this->db->affected_rows();
	}

	public function delete_data_by_grade_id($grade_id) {
		$this->db->where('grade_id', $grade_id)->delete('written_works');
	}
}