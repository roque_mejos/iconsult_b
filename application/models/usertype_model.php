<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Usertype_model extends CI_Model {

	public function read_data() {
		$sql = $this->usertype_lib->read_data_sql();
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_teacher_and_parent() {
		$sql = "
			SELECT
				`usertypes`.`id`,
				`usertypes`.`name`
			FROM
				`usertypes`
			WHERE
				`usertypes`.`id` IN(". TEACHER .", ". PARENTS .")
			ORDER BY
				`usertypes`.`name`
		";

		$query = $this->db->query($sql);
		return $query->result();
	}
}