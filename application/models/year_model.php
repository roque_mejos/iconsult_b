<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Year_model extends CI_Model {

	public function read_data() {
		$sql = $this->year_lib->read_data_sql();
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function insert_data() {
		$data['name'] = $this->input->post('year');
		$this->db->insert('years', $data);
		return $this->db->insert_id();
	}

	public function read_data_by_year_id($year_id) {
		$query = $this->db->where('id', $year_id)->get('years');
		return $query->row();
	}
	
	public function count_data_by_year_id($year_id) {
		$query = $this->db->where('id', $year_id)->get('years');
		return $query->num_rows();
	}

	public function read_data_by_year_id_and_not_by_name($year_id) {
		$sql = "
			SELECT
				COUNT(*) as `count_data`
			FROM
				`years`
			WHERE
				`years`.`id` != ". $this->db->escape($year_id) ."
			AND
				`years`.`name` = ". $this->db->escape($this->input->post('year')) ."
		";
		$query = $this->db->query($sql);
		return $query->row()->count_data;
	}

	public function update_data_by_year_id() {
		$data = array('name' => $this->input->post('year'));
		$this->db->where('id', $this->input->post('year_id'))->update('years', $data);
		return $this->db->affected_rows();
	}

	public function delete_data_by_year_id($year_id = 0) {
		if($year_id > 0) {
			$this->db->where('id', $year_id)->delete('years');
		} else {
			$this->db->where('id', $this->input->post('year_id'))->delete('years');
		}
		return $this->db->affected_rows();
	}

	public function read_name_by_year_id($year_id = 0) {
		$query = $this->db->select('name')->where('id', $year_id)->get('years');
		return $query->row()->name;
	}
}