<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Section_model extends CI_Model {

	public function insert_data($year_id) {
		$data=array();
		$section = $this->input->post('section');
		foreach($section as $row) {
			$data[] = array('name' => $row, 'year_id' => $year_id);
		}
		$this->db->insert_batch('sections', $data);
		return $this->db->affected_rows();
	}

	public function read_data_by_year_id($year_id = 1) {
		$query = $this->db->where('year_id', $year_id)->order_by('name', 'asc')->get('sections');
		return $query->result();
	}

	public function read_data_not_by_year_id_and_by_name($section, $year_id) {
		$sql = "
			SELECT
				COUNT(*) as `count_data`
			FROM
				`sections`
			WHERE
				`sections`.`year_id` != ". $this->db->escape($year_id) ."
			AND
				`sections`.`name` = ". $this->db->escape($section) ."
		";
		$query = $this->db->query($sql);
		return $query->row()->count_data;
	}

	public function delete_data_year_id($year_id = 0) {
		if($year_id > 0) {
			$this->db->where('year_id', $year_id)->delete('sections');
		} else {
			$this->db->where('year_id', $this->input->post('year_id'))->delete('sections');
		}
		return $this->db->affected_rows();
	}

	public function read_data() {
		$query = $this->db->get('sections');
		return $query->result();
	}

	public function read_name_by_section_id($section_id = 0) {
		$query = $this->db->select('name')->where('id', $section_id)->get('sections');
		return $query->row()->name;
	}
}