<?php
class Task_performance_model extends CI_Model {

	public function read_data_by_grade_id($grade_id) {
		$sql = "
			SELECT
				`task_performances`.`id`,
				`task_performances`.`title`,
				`task_performances`.`score`,
				`task_performances`.`perfect_score`,
				DATE_FORMAT(`task_performances`.`date`, '%m/%d/%Y') `date`
			FROM
				`task_performances`
			LEFT JOIN
				`grades`
			ON
				`task_performances`.`grade_id` = `grades`.`id`
			WHERE
				`task_performances`.`grade_id` = ". $this->db->escape($grade_id) ."
			ORDER BY
				DATE(`task_performances`.`date`)
			DESC
		";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function insert_data() {
		$data = array(
			'title' => $this->input->post('title'),
			'score' => $this->input->post('score'),
			'perfect_score' => $this->input->post('perfect_score'),
			'grade_id' => $this->input->post('grade_id'),
			'date' => date('Y-m-d',strtotime($this->input->post('date')))
			);
		$this->db->insert('task_performances', $data);
		return $this->db->affected_rows();
	}

	public function count_data_by_written_work_id() {
		$query = $this->db->where('id', $this->input->post('id'))->get('task_performances');
		return $query->num_rows();
	}

	public function read_data_by_written_work_id() {
		$query = $this->db->where('id', $this->input->post('id'))->get('task_performances');
		return $query->row();
	}

	public function update_data_by_written_work_id() {
		$data = array(
			'title' => $this->input->post('title'),
			'score' => $this->input->post('score'),
			'perfect_score' => $this->input->post('perfect_score'),
			'date' => date('Y-m-d',strtotime($this->input->post('date')))
			);
		$this->db->where('id', $this->input->post('id'))->update('task_performances', $data);
		return $this->db->affected_rows();
	}

	public function delete_data_by_written_work_id() {
		$this->db->where('id', $this->input->post('id'))->delete('task_performances');
		return $this->db->affected_rows();
	}

	public function delete_data_by_grade_id($grade_id) {
		$this->db->where('grade_id', $grade_id)->delete('task_performances');
	}
}