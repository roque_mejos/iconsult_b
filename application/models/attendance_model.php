<?php 
class Attendance_model extends CI_Model {

	public function insert_data() {
		$data = array();
		$data['school_date_id'] = $this->input->post('school_date');
		$data['status'] = $this->input->post('status');
		$data['student_id'] = $this->input->post('student_id');
		$this->db->insert('attendance', $data);
		return $this->db->affected_rows();
	}

	public function update_data() {
		$data = array();
		$school_date_id = $this->input->post('school_date');
		$student_id = $this->input->post('student_id');
		$data['status'] = $this->input->post('status');
		$this->db->where('school_date_id', $school_date_id)->where('student_id', $student_id)->update('attendance', $data);
	}

	public function read_data_by_student_id($student_id) {
		$query = $this->db->where('student_id', $student_id)->order_by('date', 'desc')->get('attendance');
		return $query->result();
	}

	public function delete_data_by_attendance_id() {
		$this->db->where('id', $this->input->post('id'))->delete('attendance');
		return $this->db->affected_rows();
	}

	public function read_status_by_date_and_student_id($school_date_id, $student_id) {
		$query = $this->db->select('status')->where('school_date_id', $school_date_id)->where('student_id', $student_id)->get('attendance');
		if($query->num_rows()) {
			return $query->row()->status;
		} else {
			return false;
		}
	}

	public function count_data_by_date_and_student_id($school_date_id, $student_id) {
		$query = $this->db->select('status')->where('school_date_id', $school_date_id)->where('student_id', $student_id)->get('attendance');
		if($query->num_rows()) {
			return true;
		}
		return false;
	}

	public function count_date_by_present_status_and_student_id_and_month_and_year($student_id, $month, $year) {
		$sql = "
			SELECT
				COUNT(*) AS `count_data`
			FROM
				`attendance`
			LEFT JOIN
				`school_dates`
			ON
				`attendance`.`school_date_id` = `school_dates`.`id`
			WHERE
				`attendance`.`status` = true
			AND
				EXTRACT(MONTH FROM `school_dates`.`date`) = ". $this->db->escape($month) ."
			AND
				EXTRACT(YEAR FROM `school_dates`.`date`) = ". $this->db->escape($year) ."
			AND
				`attendance`.student_id = ". $this->db->escape($student_id) ."
		";

		$query = $this->db->query($sql);

		return $query->row()->count_data;
	}

	public function count_date_by_absent_status_and_student_id_and_month_and_year($student_id, $month, $year) {
		$sql = "
			SELECT
				COUNT(*) AS `count_data`
			FROM
				`attendance`
			LEFT JOIN
				`school_dates`
			ON
				`attendance`.`school_date_id` = `school_dates`.`id`
			WHERE
				`attendance`.`status` = false
			AND
				EXTRACT(MONTH FROM `school_dates`.`date`) = ". $this->db->escape($month) ."
			AND
				EXTRACT(YEAR FROM `school_dates`.`date`) = ". $this->db->escape($year) ."
			AND
				`attendance`.student_id = ". $this->db->escape($student_id) ."
		";

		$query = $this->db->query($sql);

		return $query->row()->count_data;
	}
}