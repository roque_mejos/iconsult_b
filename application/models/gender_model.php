<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gender_model extends CI_Model {

	public function read_data() {
		$sql = $this->gender_lib->read_data_sql();
		$query = $this->db->query($sql);
		return $query->result();
	}
}