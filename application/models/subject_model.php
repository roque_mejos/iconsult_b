<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Subject_model extends CI_Model {

	public function read_data() {
		$sql = $this->subject_lib->read_data_sql();
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_data_by_active() {
		$sql = $this->subject_lib->read_data_by_active_sql();
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function deactivate_status_by_subject_id($subject_id) {
		$sql = $this->subject_lib->deactivate_status_by_subject_id_sql($subject_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function activate_status_by_subject_id($subject_id) {
		$sql = $this->subject_lib->activate_status_by_subject_id_sql($subject_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function insert_data($subject) {
		$sql = $this->subject_lib->insert_data_sql($subject);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function read_name_by_subject_id($subject_id) {
		$query = $this->db->select('name')->where('id', $subject_id)->get('subjects');
		return $query->row()->name;
	}
}