<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Event_model extends CI_Model {

	public function insert_data($data) {
		$sql = $this->event_lib->insert_data_sql($data);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function count_data_by_title_and_date($title, $date) {
		$sql = $this->event_lib->count_data_by_title_and_date_sql($title, $date);
		$query = $this->db->query($sql);
		return $query->row()->count_data;
	}

	public function read_data() {
		$sql = $this->event_lib->read_data_sql();
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function delete_data_by_event_id($event_id) {
		$sql = $this->event_lib->delete_data_by_event_id_sql($event_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function count_data_by_event_id($event_id) {
		$sql = $this->event_lib->count_data_by_event_id_sql($event_id);
		$query = $this->db->query($sql);
		return $query->row()->count_data;
	}

	public function read_data_by_event_id($event_id) {
		$sql = $this->event_lib->read_data_by_event_id_sql($event_id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	public function update_data_by_event_id($data, $event_id) {
		$sql = $this->event_lib->update_data_by_event_id_sql($data, $event_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}
}