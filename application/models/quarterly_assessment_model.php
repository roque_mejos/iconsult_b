<?php
class Quarterly_assessment_model extends CI_Model {

	public function read_data_by_grade_id($grade_id) {
		$sql = "
			SELECT
				`quarterly_assessments`.`id`,
				`quarterly_assessments`.`title`,
				`quarterly_assessments`.`score`,
				`quarterly_assessments`.`perfect_score`,
				DATE_FORMAT(`quarterly_assessments`.`date`, '%m/%d/%Y') `date`
			FROM
				`quarterly_assessments`
			LEFT JOIN
				`grades`
			ON
				`quarterly_assessments`.`grade_id` = `grades`.`id`
			WHERE
				`quarterly_assessments`.`grade_id` = ". $this->db->escape($grade_id) ."
			ORDER BY
				DATE(`quarterly_assessments`.`date`)
			DESC
		";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function insert_data() {
		$data = array(
			'title' => $this->input->post('title'),
			'score' => $this->input->post('score'),
			'perfect_score' => $this->input->post('perfect_score'),
			'grade_id' => $this->input->post('grade_id'),
			'date' => date('Y-m-d',strtotime($this->input->post('date')))
			);
		$this->db->insert('quarterly_assessments', $data);
		return $this->db->affected_rows();
	}

	public function count_data_by_written_work_id() {
		$query = $this->db->where('id', $this->input->post('id'))->get('quarterly_assessments');
		return $query->num_rows();
	}

	public function read_data_by_written_work_id() {
		$query = $this->db->where('id', $this->input->post('id'))->get('quarterly_assessments');
		return $query->row();
	}

	public function update_data_by_written_work_id() {
		$data = array(
			'title' => $this->input->post('title'),
			'score' => $this->input->post('score'),
			'perfect_score' => $this->input->post('perfect_score'),
			'date' => date('Y-m-d',strtotime($this->input->post('date')))
			);
		$this->db->where('id', $this->input->post('id'))->update('quarterly_assessments', $data);
		return $this->db->affected_rows();
	}

	public function delete_data_by_written_work_id() {
		$this->db->where('id', $this->input->post('id'))->delete('quarterly_assessments');
		return $this->db->affected_rows();
	}

	public function delete_data_by_grade_id($grade_id) {
		$this->db->where('grade_id', $grade_id)->delete('quarterly_assessments');
	}
}