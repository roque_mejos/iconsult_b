<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Grade_model extends CI_Model {

	public function insert_data($grading_type, $student_record_id) {
		$data = array(
			'grading_type' => $grading_type,
			'student_record_id' => $student_record_id
			);
		$this->db->insert('grades', $data);
		return $this->db->affected_rows();
	}

	public function delete_data_by_grade_id($grade_id) {
		$this->db->where_in('id', $grade_id)->delete('grades');
		return $this->db->affected_rows();
	}

	public function delete_data_by_student_record_id($student_record_id) {
		$this->db->where('student_record_id', $student_record_id)->delete('grades');
		return $this->db->affected_rows();
	}

	public function read_data_by_student_record_id($student_record_id) {
		$query = $this->db->where('student_record_id', $student_record_id)->get('grades');
		return $query->result();
	}

	public function read_grade_id_by_student_record_id_and_grading_type($student_record_id = 0, $grading_type = '1st') {
		$query = $this->db->select('id')->where('student_record_id', $student_record_id)->where('grading_type', $grading_type)->get('grades');
		return $query->row()->id;
	}
}