<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement_model extends CI_Model {

	public function read_data() {
		$sql = $this->announcement_lib->read_data_sql();
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function insert_data($data) {
		$sql = $this->announcement_lib->insert_data_sql($data);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function delete_data_by_announcement_id($announcement_id) {
		$sql = $this->announcement_lib->delete_data_by_announcement_id_sql($announcement_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function read_data_by_announcement_id($announcement_id) {
		$sql = $this->announcement_lib->read_data_by_announcement_id_sql($announcement_id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	public function count_data_by_announcement_id($announcement_id) {
		$sql = $this->announcement_lib->count_data_by_announcement_id_sql($announcement_id);
		$query = $this->db->query($sql);
		return $query->row()->count_data;
	}

	public function update_data_by_announcement_id($data, $announcement_id) {
		$sql = $this->announcement_lib->update_data_by_announcement_id_sql($data, $announcement_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}
}