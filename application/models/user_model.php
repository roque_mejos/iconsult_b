<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function check_user($data) {
		$sql = $this->user_lib->check_user_sql($data);
		$query = $this->db->query($sql);
		return $query->row();
	}

	public function count_user($data) {
		$sql = $this->user_lib->count_user_sql($data);
		$query = $this->db->query($sql);
		return $query->row()->count_data;
	}

	public function read_data_not_by_users_and_admin($user_id) {
		$sql = $this->user_lib->read_data_not_by_users_and_admin_sql($user_id);
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function delete_data_by_user_id($user_id) {
		$sql = $this->user_lib->delete_data_by_user_id_sql($user_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function read_data_by_user_id($user_id) {
		$sql = $this->user_lib->read_data_by_user_id_sql($user_id);
		$query = $this->db->query($sql);
		return $query->row();
	}

	public function count_data_by_user_id($user_id) {
		$sql = $this->user_lib->count_data_by_user_id_sql($user_id);
		$query = $this->db->query($sql);
		return $query->row()->count_data;
	}

	public function count_data_by_username_and_not_user_id($username, $user_id) {
		$sql = $this->user_lib->count_data_by_username_and_not_user_id_sql($username, $user_id);
		$query = $this->db->query($sql);
		return $query->row()->count_data;
	}

	public function update_data_by_user_id($user_id, $data) {
		$sql = $this->user_lib->update_data_by_user_id_sql($user_id, $data);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function insert_data($data) {
		$sql = $this->user_lib->insert_data_sql($data);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function read_password_by_user_id($user_id) {
		$sql = $this->user_lib->read_password_by_user_id_sql($user_id);
		$query = $this->db->query($sql);
		return $query->row()->password;
	}

	public function update_password_by_user_id($password, $user_id) {
		$sql = $this->user_lib->update_password_by_user_id_sql($password, $user_id);
		$this->db->query($sql);
		return $this->db->affected_rows();
	}

	public function read_name_by_parent_usertype() {
		$sql = $this->user_lib->read_name_by_parent_usertype_sql();
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_name_by_user_id($user_id) {
		$sql = "
			SELECT
				CONCAT(`users`.`fname`, ' ', `users`.`mi`, '. ', `users`.`lname`) as `fullname`
			FROM
				`users`
			WHERE
				`users`.id = ". $this->db->escape($user_id) ."
		";
		$query = $this->db->query($sql);
		return $query->row()->fullname;
	}

	public function insert_parent() {
		$data = array(
			'username' => $this->input->post('username'),
			'fname' => $this->input->post('fname'),
			'lname' => $this->input->post('lname'),
			'mi' => $this->input->post('mi'),
			'bdate' => date('Y-m-d', strtotime($this->input->post('bdate'))),
			'gender' => $this->input->post('gender'),
			'address' => $this->input->post('address'),
			'mobile' => $this->input->post('mobile'),
			'password' => md5($this->input->post('password')),
			'usertype' => $this->input->post('usertype')
			);
		$this->db->insert('users', $data);
		return $this->db->affected_rows();
	}
}