<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Grade_category_model extends CI_Model {

	public function read_data_by_user_id() {
		$sql = "SELECT
					`grade_categories`.*
				FROM
					`grade_categories`
				WHERE
					`grade_categories`.`user_id` = ". $this->db->escape($this->session->id) ."
		";
		$query = $this->db->query($sql);
		return $query->row();
	}
}