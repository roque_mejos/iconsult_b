<?php

class School_date_model extends CI_Model {

	public function read_school_date_id_by_date($date) {
		$query = $this->db->select('id')->where('DATE(date)', $date)->get('school_dates');
		if($query->num_rows()) {
			return $query->row()->id;
		} else {
			return 0;
		}
	}

	public function count_school_date_id_by_date($date) {
		$query = $this->db->select('id')->where('DATE(date)', $date)->get('school_dates');
		return $query->num_rows();
	}

	public function insert_data($date) {
		$this->db->insert('school_dates', ['date' => $date]);
		return $this->db->insert_id();
	}

	public function count_days_by_month_and_year($student_id, $month, $year) {

		$sql = "
			SELECT
				COUNT(*) AS `count_data`
			FROM
				`school_dates`
			right join
				`attendance`
			ON
				`school_dates`.`id` = `attendance`.`school_date_id`
			WHERE
				EXTRACT(MONTH FROM `school_dates`.`date`) = ". $this->db->escape($month) ."
			AND
				EXTRACT(YEAR FROM `school_dates`.`date`) = ". $this->db->escape($year) ."
			AND
				`attendance`.student_id = ". $this->db->escape($student_id) ."
		";

		$query = $this->db->query($sql);

		return $query->row()->count_data;
	}

}