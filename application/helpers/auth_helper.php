<?php defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('ci')) {
	function ci() {
		$ci =& get_instance();
		return $ci;
	}
}

if(!function_exists('load_nav')) {
	function load_nav() {
		if(ci()->session->has_userdata('logged_in')) {
			ci()->load->view('template/nav');
		}
	}
}

if(!function_exists('load_js')) {
	function load_js() {
		if(ci()->session->has_userdata('logged_in')) {
			echo '<script src="'. base_url('assets/plugins/bootstrap/bootstrap.min.js') .'"></script>';
			echo '<script src="'. base_url('assets/plugins/metisMenu/jquery.metisMenu.js') .'"></script>';
			echo '<script src="'. base_url('assets/plugins/pace/pace.js') .'"></script>';
			echo '<script src="'. base_url('assets/scripts/siminta.js') .'"></script>';
			echo '<script src="'. base_url('assets/scripts/bootbox.min.js') .'"></script>';
			echo '<script src="'. base_url('assets/scripts/bootstrap-datepicker.js') .'"></script>';
			echo '<script src="'. base_url('assets/bootstrap-select/dist/js/bootstrap-select.min.js') .'"></script>';
		}
	}
}

if(!function_exists('age')) {
	function age($birthDate) {
		$age = 0;
		$birthDate = explode("/", $birthDate);
		$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
		return $age;
	}
}

if(!function_exists('date_valid_format')) {
	function date_valid_format($bdate) {
		$format = 'm/d/Y';
		$date = DateTime::createFromFormat($format, $bdate);
		return $date && $date->format($format) == $bdate;
	}
}

if(!function_exists('get_number_of_day_by_date')) {
	function get_number_of_day_by_date($date) {
		$now = time(); // or your date as well
		$your_date = strtotime($date);
		$datediff = $now - $your_date;

		return floor($datediff / (60 * 60 * 24));
	}
}

if(!function_exists('profile_image')) {
	function profile_image() {
		$image = '';
		if(strlen(ci()->session->image) > 0 && file_exists('image-profile/' . ci()->session->image) !== false) {
			$image = base_url('image-profile/' . ci()->session->image);
		} else {
			if(ci()->session->gender == MALE) {
				$image = base_url('assets/img/user.jpg');
			} else {
				$image = base_url('assets/img/user-female.png');
			}
		}
		return $image;
	}
}